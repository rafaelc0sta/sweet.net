//
//  EditProfileViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 20/05/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADNKit.h"
#import "RCKit.h"

@interface EditProfileViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIBarPositioningDelegate> {
    IBOutlet UITextField *textField_name;
    
    IBOutlet UILabel *label_username;
    IBOutlet UITextView *textView_description;
    IBOutlet UIButton *button_editDescription;
    
    IBOutlet RCMaskedImageView *imageView_userImage;
    
    BOOL updatePhaseOneComplete;
    BOOL updatePhaseTwoComplete;
}

- (void)updateUserProperties;

@property (nonatomic) BOOL hasChangedProfileImage;
@property (nonatomic) BOOL hasChangedProfileBio;
@property (nonatomic) ANKUser *user;


- (IBAction)changeUserImage:(id)sender;
- (IBAction)changeUserBio:(id)sender;

- (IBAction)textFieldDone:(id)sender;

- (void)modifyBioToBeSent:(NSString *)newBio;

- (IBAction)saveUserProfile:(id)sender;
- (IBAction)cancelChanges:(id)sender;
@end
