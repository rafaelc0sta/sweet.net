//
//  SearchViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 30/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "SearchViewController.h"
#import "SVPullToRefresh.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import "UserDetailViewController.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[[self.navigationController navigationBar] topItem] setTitle:@"Search"];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    searchPriority = 0;
    
    //[searchBar setBarTintColor:[UIColor whiteColor]];
    [searchBar setDelegate:self];
    self.title = @"Search";
    
    array_users = [[NSMutableArray alloc]init];
    
    [self.navigationController setDelegate:self];
    
    //[searchBar setBarTintColor:UIColor.whiteColor];
    
    // Do any additional setup after loading the view.
}


- (void)reloadUsersWithSearchPriority:(NSUInteger)searchP {
    NSUInteger searchPri = searchP;
    [[ANKClient sharedClient] searchForUsersWithQuery:searchBar.text completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
        if (error == nil) {
            if (searchPriority == searchPri) {
                NSArray *users = responseObject;
                array_users = [users mutableCopy];
                [tableView_results reloadData];
            }
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [searchBar becomeFirstResponder];
    for (RCComposeViewController *VC in [self childViewControllers]) {
        [VC.view removeFromSuperview];
        [VC removeFromParentViewController];
    }
    for (UIView *view in [self.view subviews]) {
        if (view.backgroundColor == [UIColor blackColor] && view.alpha == .7f) [view removeFromSuperview];
    }
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    
    // Dispose of any resources that can be recreated.
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [array_users count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"CellUser";
    UITableViewCell *cell = [tableView_results dequeueReusableCellWithIdentifier:identifier];
    
    ANKUser *currentUser = (ANKUser *)[array_users objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [@"@" stringByAppendingString:currentUser.username];
    cell.imageView.image = [UIImage imageWithCIImage:[[self imageWithImage:[UIImage imageNamed:@"placeholder"] convertToSize:CGSizeMake(48, 48)] CIImage] scale:2.0 orientation:UIImageOrientationUp];
    
    [cell.imageView.layer setMasksToBounds:YES];
    [cell.imageView.layer setCornerRadius:48.f/2.0];
    
    NSString *finalURLString = [currentUser.avatarImage.URL.absoluteString stringByAppendingString:@"?h=120&w=120"];
    NSURL *userURL = [NSURL URLWithString:finalURLString];
    
    [cell.imageView setImageWithURL:userURL placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        cell.imageView.image = [self imageWithImage:cell.imageView.image convertToSize:CGSizeMake(48, 48)];
    }];
    [cell layoutSubviews];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 65.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UserDetailViewController *userDetailViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"UserDetailViewController"];
    [userDetailViewController setUser:[array_users objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:userDetailViewController animated:YES];
}


#pragma mark - Search Bar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    searchPriority = searchPriority++;
    [self reloadUsersWithSearchPriority:searchPriority];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark NavBarDelegate


- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (viewController == self) {
        [self.navigationController.navigationBar.topItem setTitle:@"Search"];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    } else {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}

@end
