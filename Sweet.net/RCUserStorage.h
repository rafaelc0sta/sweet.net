//
//  RCTokenStorage.h
//  Sweet.net
//
//  Created by Rafael Costa on 12/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADNKit.h"
#import <Accounts/Accounts.h>
@interface RCUserStorage : NSObject
+ (instancetype)sharedUserStorage;
@property (nonatomic) NSString *user;
@property (nonatomic) ANKUser *userObject;
@property (nonatomic) ACAccountStore *accountStore;
@end
