//
//  RCShowdownImageView.h
//  Sweet.net
//
//  Created by Rafael Costa on 1/3/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import <AFNetworking/AFNetworking.h>

@interface RCShowdownImageView : UIView <UIScrollViewDelegate> {
    UIScrollView *scrollView;
	MBBarProgressView *progressView;
    UIImageView *imageView;
}
- (void)showViewToView:(UIView *)superview;
- (void)hideView;

- (void)setImageWithURL:(NSURL *)url;
- (void)setImage:(UIImage *)image;
@end
