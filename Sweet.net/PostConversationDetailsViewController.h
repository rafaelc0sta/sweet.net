//
//  PostConversationDetailsViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 23/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADNKit.h"
#import "JLActionSheet.h"
#import "RCKit.h"

@interface PostConversationDetailsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, RCPostCellDelegate> {
    JLActionSheet *actionSheet_repost;
    
    UIView *maskView;
    
    UIView *maskView_navController;

    IBOutlet UINavigationBar *navBar;
    IBOutlet UITableView *tableView_posts;
    NSMutableArray *array_posts;
    
    IBOutlet UIRefreshControl *refreshControl;
}
@property (nonatomic) ANKPost *post;
@end
