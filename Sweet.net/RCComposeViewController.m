//
//  RCComposeViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 12/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "RCComposeViewController.h"
#import "TimelineViewController.h"
#import "RCUserStorage.h"
#import "AutoCompleteViewController.h"
#import "ADNKit.h"
#import <MediaPlayer/MediaPlayer.h>
#import <CoreLocation/CoreLocation.h>
#import "MBProgressHUD.h"
#import <Social/Social.h>


@interface RCComposeViewController ()
@end

@implementation RCComposeViewController

- (IBAction)hideKeyboard:(id)sender {
    [textView_post resignFirstResponder];
}

- (void)addTextToPost:(NSString *)text {
    if ([textView_post.text hasSuffix:@" "]) {
        textView_post.text = [textView_post.text stringByAppendingString:text];
        textView_post.text = [textView_post.text stringByAppendingString:@" "];
    } else if (![textView_post.text hasSuffix:@" "] && textView_post.text.length == 0) {
        textView_post.text = [textView_post.text stringByAppendingString:text];
        textView_post.text = [textView_post.text stringByAppendingString:@" "];
    } else if (![textView_post.text hasSuffix:@" "] && textView_post.text.length > 0) {
        textView_post.text = [textView_post.text stringByAppendingString:[NSString stringWithFormat:@" %@ ", text]];
    }
    [self updateCounter];
}

- (IBAction)addNowPlaying:(id)sender {
    
    MPMediaItem * song = [[MPMusicPlayerController iPodMusicPlayer] nowPlayingItem];
    
    textView_post.text = [textView_post.text stringByAppendingFormat:@" #np %@ - %@",[song valueForKey:MPMediaItemPropertyTitle],[song valueForKey:MPMediaItemPropertyArtist]];
    
    [self updateCounter];
}

- (IBAction)openAutoComplete:(id)sender {
    
    AutoCompleteViewController *ac = [[AutoCompleteViewController alloc] initWithNibName:@"AutoCompleteViewController" bundle:nil];
    [ac setDelegate:self];
    [self presentViewController:ac animated:YES completion:NO];
}

- (IBAction)getPicture:(id)sender {
    //UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Camera",nil), NSLocalizedString(@"Photo Albums", nil),nil];
    
    JLActionSheet *actionSheet = [[JLActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@[NSLocalizedString(@"Camera", nil), NSLocalizedString(@"Photo Albums", nil)]];
    [actionSheet setStyle:JLSTYLE_SUPERCLEAN];
    [textView_post resignFirstResponder];
    [actionSheet setClickedButtonBlock:^(JLActionSheet *actionSheet, NSInteger buttonIndex) {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        [imagePickerController setDelegate:self];
        [imagePickerController setAllowsEditing:NO];
        if (buttonIndex == 0) {
            //Do nothing...
        } else if (buttonIndex == 1) {
            //Photos Album
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            [self presentViewController:imagePickerController animated:YES completion:nil];
        } else if (buttonIndex == 2) {
            //Camera
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
        
        [textView_post becomeFirstResponder];
    }];
    
    
    [actionSheet showInView:self.view];
}

- (IBAction)addLocation:(id)sender {
    if (hasAddedLocation == FALSE) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
        [locationManager stopUpdatingLocation];
        location  = [locationManager location];
        
        while (location.coordinate.longitude == 0.0) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:60.f]];
        }
        
        
        float longitude = location.coordinate.longitude;
        float latitude = location.coordinate.latitude;
        
        if (latitude != 0.f && longitude != 0.0) {
            ANKGeolocation *geoLocation = [[ANKGeolocation alloc] init];
            [geoLocation setLatitude:latitude];
            [geoLocation setLongitude:longitude];

            ANKAnnotation *annotation = [ANKAnnotation geolocationAnnotationForGeolocation:geoLocation];
            
            NSMutableArray *mutableAnnotations = currentPost.annotations.mutableCopy;
            [mutableAnnotations addObject:annotation];
            [currentPost setAnnotations:mutableAnnotations.copy];
            
            [button_addLocation setImage:[button_addLocation.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            hasAddedLocation = TRUE;
        }
    } else {
        
        NSMutableArray *mutableAnnotations = currentPost.annotations.mutableCopy;
        ANKAnnotation *annotationToRemove = nil;
        
        for (ANKAnnotation *annon in mutableAnnotations) {
            if ([annon.type isEqualToString:kANKCoreAnnotationGeolocation]) annotationToRemove = annon;
        }
        [mutableAnnotations removeObject:annotationToRemove];
        [currentPost setAnnotations:mutableAnnotations.copy];
        
        [button_addLocation setImage:[button_addLocation.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        //[button_addLocation setImage:[UIImage imageNamed:@"pxm_location"] forState:UIControlStateNormal];
        hasAddedLocation = FALSE;
    }
    
    
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (actionSheet.title == nil) {
        
    }
}

- (IBAction)addTwitterSharing:(id)sender {
    if (twitterAccount == nil) {
        NSMutableArray *array_usernames = NSMutableArray.new;
        for (ACAccount *account in accountArray) {
            //This makes it add usernames in the same order as they're on the accountArray.
            //Smart trick, huh?
            [array_usernames addObject:[NSString stringWithFormat:@"@%@", account.username]];
        }
        
        JLActionSheet *actionSheet_account = [[JLActionSheet alloc] initWithTitle:@"Pick a Twitter account:" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:array_usernames];
        [actionSheet_account setStyle:JLSTYLE_SUPERCLEAN];
        [textView_post resignFirstResponder];
        [actionSheet_account setClickedButtonBlock:^(JLActionSheet *actionSheet, NSInteger buttonIndex) {
            NSLog(@"Button index: %i", buttonIndex);
            if (buttonIndex != 0) {
                buttonIndex = buttonIndex - 1;
                
                NSArray *temp = [[accountArray reverseObjectEnumerator] allObjects];
                
                twitterAccount = [temp objectAtIndex:buttonIndex];
                [button_addTwitterSharing setImage:[button_addTwitterSharing.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
            [textView_post becomeFirstResponder];
        }];
        [actionSheet_account showOnViewController:self];
    } else {
        twitterAccount = nil;
        [button_addTwitterSharing setImage:[button_addTwitterSharing.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    }
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    self.imageAttached = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    NSString *imageFileName = [[info objectForKey:@"UIImagePickerControllerReferenceURL"] absoluteString];
    if (imageFileName.length == 0) {
        //Camera image. Assign a time-based filename.
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd-HH:mm"];
        
        imageFileName = [formatter stringFromDate:[NSDate date]];
    }
    [button_getPicture setImage:[button_getPicture.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    button_getPicture.userInteractionEnabled = false;
    button_getPicture.hidden = TRUE;
    
    barButtonItem_post.enabled = FALSE;
    
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activityIndicatorView startAnimating];
    activityIndicatorView.center = button_getPicture.center;
    [self.view addSubview:activityIndicatorView];

    NSData *finalImageData =  [self compressImage:self.imageAttached toTargetSize:2097152];
    
    ANKFile *file = [[ANKFile alloc] init];
    NSString *fileName = [[imageFileName stringByReplacingOccurrencesOfString:@"assets-library://asset/asset.JPG?id=" withString:@""] stringByReplacingOccurrencesOfString:@"&ext=JPG" withString:@""];
    [file setName:[fileName stringByAppendingString:@".jpg"]];
    [file setMimeType:@"image/jpeg"];
    [file setType:@"com.rafaelcosta.sweet.net.file"];
    [[ANKClient sharedClient] createFile:file withData:finalImageData completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
        if (error == nil) {

            ANKAnnotation *annotation = [ANKAnnotation oembedAnnotationForFile:responseObject];
            
            NSMutableArray *mutableAnnotations = currentPost.annotations.mutableCopy;
            
            [mutableAnnotations addObject:annotation];
            
            [currentPost setAnnotations:mutableAnnotations.copy];

            self.charLimit = self.charLimit - @" photos.app.net/{post_id}/1".length;
            [self textViewDidChange:textView_post];
            hasAddedFile = TRUE;
        } else {
            [button_getPicture setImage:[UIImage imageNamed:@"picture_off"] forState:UIControlStateNormal];
            button_getPicture.userInteractionEnabled = true;
            self.imageAttached = nil;
        }
        [activityIndicatorView removeFromSuperview];
        barButtonItem_post.enabled = TRUE;
        button_getPicture.hidden = FALSE;
    }];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)post:(id)sender {
    barButtonItem_cancel.enabled = false;
    barButtonItem_post.enabled = false;
    [textView_post resignFirstResponder];
    [textView_post setUserInteractionEnabled:NO];
    if (currentPost.text == nil) currentPost.text = @"";
    currentPost.text = textView_post.text;
    
    if (hasAddedFile == TRUE) {
        if ([[currentPost.text substringFromIndex:currentPost.text.length - 1] isEqualToString:@" "]) {
            currentPost.text = [currentPost.text stringByAppendingString:@"photos.app.net/{post_id}/1"];
        } else {
            currentPost.text = [currentPost.text stringByAppendingString:@" photos.app.net/{post_id}/1"];
        }
    }
    
    if (self.postToReply != nil) [currentPost setRepliedToPostID:self.postToReply.postID];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
    [hud setMode:MBProgressHUDModeText];
    [hud setLabelText:@"Posting to App.net..."];
    [[ANKClient sharedClient] createPost:currentPost completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
        if (error == nil) {
            ANKPost *responsePost = responseObject;
            
            if (twitterAccount != nil) {
                [hud setLabelText:@"Posting to Twitter..."];
                NSString *twitterPost = [responsePost text];
                if (twitterPost.length > 140) {
                    twitterPost = [twitterPost substringToIndex:(140 - 28)];
                    
                    twitterPost = [twitterPost stringByAppendingString:@"... "];
                    
                    NSString *appendingString = [NSString stringWithFormat:@"http://alpha.app.net/%@/post/%@", responsePost.user.username, responsePost.postID];
                    
                    twitterPost = [twitterPost stringByAppendingString:appendingString];
                    //URL lenght doesn't matter, t.co is there for that.
                    //t.co urls lenght is 23. 23.
                    //Just go on with posting.
                }
                [self postTweetToTwitterWithText:twitterPost];
            } else {
                [hud hide:YES];
                [self dismiss:nil];
            }
            
            
        } else {
            UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alertError show];
            [hud hide:YES];
            [self dismiss:nil];
        }
    }];
}

- (void)postTweetToTwitterWithText:(NSString *)text {
    SLRequest *requestPostToTwitter = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:[NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"] parameters:@{@"status": text}];
    [requestPostToTwitter setAccount:twitterAccount];
    [requestPostToTwitter performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (error == nil) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self dismiss:nil];
            });
        } else {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self dismiss:nil];
            });
        }
    }];
     
}

- (IBAction)dismiss:(id)sender {
    [textView_post resignFirstResponder];
    if (!self.presentingViewController) {
        UIViewController *vC = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateInitialViewController];
        vC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:vC animated:YES completion:nil];
    }
    
    if ([self.presentingViewController isKindOfClass:[UINavigationController class]]){
        [[[(UINavigationController *)self.presentingViewController viewControllers]objectAtIndex:0] viewWillAppear:YES];
    } 
    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"Dismissed");
    }];
}

- (NSData *)compressImage:(UIImage *)image toTargetSize:(NSUInteger)targetSize {
    NSData *data = UIImageJPEGRepresentation(image, 1.f);
    
    float compression = [data length]/targetSize;
    
    data = UIImageJPEGRepresentation(image, compression);
//    
//    
//    if ([data length] > targetSize) {
//        data = UIImageJPEGRepresentation(image, 0.55);
//        if ([data length] > targetSize) {
//            data = UIImageJPEGRepresentation(image, 0.5);
//            if ([data length] > targetSize) {
//                data = UIImageJPEGRepresentation(image, 0.45);
//                if ([data length] > targetSize) {
//                    data = UIImageJPEGRepresentation(image, 0.4);
//                    if ([data length] > targetSize) {
//                        data = UIImageJPEGRepresentation(image, 0.35);
//                        if ([data length] > targetSize) {
//                            data = UIImageJPEGRepresentation(image, 0.3);
//                            if ([data length] > targetSize) {
//                                data = UIImageJPEGRepresentation(image, 0.25);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
    return data;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)updateCounter {
    //When more than 88% is filled:
    if (textView_post.text.length > (self.charLimit * .885)) {
        float x = textView_post.text.length - (int)(self.charLimit * .88);
        float y = (19*x)/3;
        label_counter.textColor = [UIColor colorWithRed:y/255 green:0 blue:0 alpha:1.0f];
    } else {
        label_counter.textColor = [UIColor blackColor];
    }
    label_counter.text = [NSString stringWithFormat:@"%i", (self.charLimit - textView_post.text.length)];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    sharedAccountStore = [[RCUserStorage sharedUserStorage] accountStore];
    accountType_twitter = [sharedAccountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    [sharedAccountStore requestAccessToAccountsWithType:accountType_twitter options:nil completion:^(BOOL granted, NSError *error) {
        if (granted) {
            accountArray = [sharedAccountStore accountsWithAccountType:accountType_twitter];
            if (accountArray.count == 0) button_addTwitterSharing.enabled = false;
        } else {
            MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
            [hud setMode:MBProgressHUDModeText];
            hud.labelText = @"Can't access Twitter accounts.";
            [hud showAnimated:YES whileExecutingBlock:^{
                button_addTwitterSharing.hidden = true;
            }];
        }
    }];
    twitterAccount = nil;
    
    [navigationBar setDelegate:self];
    
    [textView_post setDelegate:self];
    
    hasAddedLocation = FALSE;
    if (self.navTitle == nil) {
        self.navTitle = @"Compose";
    }
    navigationBar.topItem.title = self.navTitle;
    [textView_post becomeFirstResponder];
    currentPost = [[ANKPost alloc] init];
    currentPost.text = @"";
    currentPost.annotations = @[];
    
    if (self.postToReply != nil) {
        if (self.initialText.length == 0) {
            for (ANKMentionEntity *entity in self.postToReply.entities.mentions) {
                currentPost.text = [currentPost.text stringByAppendingFormat:@"@%@ ", entity.username];
            }
            if ([currentPost.text rangeOfString:self.postToReply.user.username].location == NSNotFound) {
                currentPost.text = [NSString stringWithFormat:@"@%@ %@", self.postToReply.user.username, currentPost.text];
            }

        } else {
            currentPost.text = self.initialText;
        }
    } else {
        if (self.initialText.length == 0) {
            currentPost.text = @"";
        } else {
            currentPost.text = self.initialText;
        }
    }
    
    self.charLimit = 256;
    
    NSString *usernameText = [@"@" stringByAppendingString:[[RCUserStorage sharedUserStorage] user]];
    textView_post.text = [currentPost.text stringByReplacingOccurrencesOfString:usernameText withString:@""];
    if (self.postToReply != nil) {
        if ([textView_post.text characterAtIndex:textView_post.text.length - 1] == ' ' && [textView_post.text characterAtIndex:textView_post.text.length - 2] == ' ') {
            textView_post.text = [textView_post.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
        }
    }
    
    
    currentPost.text = textView_post.text;
    
    [textView_post setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16.f]];
    
    
    autoCompleteView = [[RCAutoCompleteView alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 216 - 30 - 50, [UIScreen mainScreen].bounds.size.width, 30)];
    [autoCompleteView setDelegate:self];
    [self.view addSubview:autoCompleteView];
    
    
    [self updateCounter];
    if (self.initialText.length > 0 && self.postToReply != nil) [textView_post setSelectedRange:NSMakeRange(0, 0)];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [textView_post setKeyboardType:UIKeyboardTypeTwitter];
    [textView_post becomeFirstResponder];
    [button_hideKeyboard setHidden:true];
    label_replyingTo.text = @"";
    if (self.postToReply != nil) {
        button_hideKeyboard.hidden = false;
        label_replyingTo.text = [NSString stringWithFormat:@"Replying to @%@: %@", self.postToReply.user.username, self.postToReply.text];
    }
    
    
    MPMediaItem * song = [[MPMusicPlayerController iPodMusicPlayer] nowPlayingItem];
    
    if ([[song valueForKey:MPMediaItemPropertyTitle] length] > 0 && [[MPMusicPlayerController iPodMusicPlayer] playbackState] == MPMusicPlaybackStatePlaying) {
        button_nowPlaying.enabled = TRUE;
    } else {
        button_nowPlaying.enabled = FALSE;
    }

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TextViewDelegate 

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if (self.postToReply != nil) button_hideKeyboard.hidden = false;
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    button_hideKeyboard.hidden = true;
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    if (textView.text.length > self.charLimit) {
        textView.text = [textView.text substringToIndex:self.charLimit];
    }
    [self updateCounter];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    
    NSString *futureTextViewText = [textView.text stringByAppendingString:text];
    
    
    if ([text isEqualToString:@" "]) {
        //RESET PROPERTY.
        autoCompleteView.isTypingAutocomplete = false;
    }
    
    if (autoCompleteView.isTypingAutocomplete == TRUE) {
        
        NSString *typedUntilWord = [futureTextViewText substringToIndex:range.location + 1];
        
        NSUInteger locationOfLastBlankSpace = [typedUntilWord rangeOfString:@" " options:NSBackwardsSearch].location;
        
        
        if (locationOfLastBlankSpace == NSNotFound) locationOfLastBlankSpace = 0;
        else locationOfLastBlankSpace = locationOfLastBlankSpace + 1;
        
        NSRange wordRange = NSMakeRange(locationOfLastBlankSpace, typedUntilWord.length - locationOfLastBlankSpace);
        
        NSString *str = [typedUntilWord substringWithRange:wordRange];
        
        
        if (![str hasPrefix:@"@"]) {
            autoCompleteView.isTypingAutocomplete = false;
            [autoCompleteView setTypedString:@""];
            return YES;
        }
        
        [autoCompleteView setTypedString:str withRange:wordRange];
    }
    
    
    if ([text isEqualToString:@"@"] && [textView_post.text hasSuffix:@" "]) {
        autoCompleteView.isTypingAutocomplete = TRUE;
    } else if ([text isEqualToString:@"@"] && ![textView_post.text hasSuffix:@" "] && [textView_post.text length] == 0) {
        autoCompleteView.isTypingAutocomplete = TRUE;
    }
    
    return YES;
    
}

#pragma mark - UIToolbarDelegate

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

#pragma mark - AutoCompleteDelegate

- (void)returnedCompletion:(NSString *)completion withRange:(NSRange)range {
//    NSLog(@"Returned completion: %@", completion);
//    NSRange range = [textView_post.text rangeOfString:@" " options:NSBackwardsSearch];
//    NSUInteger loc = 0;
//    if (range.location != NSNotFound) {
//        loc = range.location;
//    }
//    
//    NSString *str = [textView_post.text substringWithRange:NSMakeRange(loc, textView_post.text.length - loc)];
//    
//    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
//    
//    NSLog(@"String %@ will be replaced by %@", str, completion);
//    if (loc != 0) completion = [NSString stringWithFormat:@" %@", completion];
//    
//    textView_post.text = [textView_post.text stringByReplacingCharactersInRange:NSMakeRange(loc, textView_post.text.length - loc) withString:completion];
//    textView_post.text = [textView_post.text stringByAppendingString:@" "];
    
    
    completion = [NSString stringWithFormat:@"%@ ", completion];
    
    textView_post.text = [textView_post.text stringByReplacingCharactersInRange:range withString:completion];
    
    autoCompleteView.isTypingAutocomplete = false;
    
    [autoCompleteView setTypedString:@"" withRange:NSMakeRange(0, 0)];
}

- (void)autoComplete:(AutoCompleteViewController *)autoComplete didReturnWithUsername:(NSString *)username andUserID:(NSString *)userID {
    if ([textView_post.text hasSuffix:@" "]) {
        textView_post.text = [textView_post.text stringByAppendingString:username];
        textView_post.text = [textView_post.text stringByAppendingString:@" "];
    } else if (![textView_post.text hasSuffix:@" "] && textView_post.text.length == 0) {
        textView_post.text = [textView_post.text stringByAppendingString:username];
        textView_post.text = [textView_post.text stringByAppendingString:@" "];
    } else if (![textView_post.text hasSuffix:@" "] && textView_post.text.length > 0) {
        textView_post.text = [textView_post.text stringByAppendingString:[NSString stringWithFormat:@" %@ ", username]];
    }
    [self updateCounter];
}



@end
