//
//  RCNavigationController.h
//  Sweet.net
//
//  Created by Rafael Costa on 17/04/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCNavigationController : UINavigationController

@end
