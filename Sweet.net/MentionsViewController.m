//
//  MentionsViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 31/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "MentionsViewController.h"


#import <QuartzCore/QuartzCore.h>
#import "UIGestureRecognizer+Blocks.h"
#import "PostDetailViewController.h"
#import "UIImageView+WebCache.h"
#import "ADNKit.h"
#import "SVPullToRefresh.h"
#import "SearchViewController.h"
#import "PostConversationDetailsViewController.h"

@interface MentionsViewController ()

@end

@implementation MentionsViewController

- (IBAction)openSearch:(id)sender {
    
    SearchViewController *searchViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"SearchViewController"];
    UINavigationController *navigationControllerSearch = [[UINavigationController alloc] initWithRootViewController:searchViewController];
    [self presentViewController:navigationControllerSearch animated:YES completion:nil];
}

- (void)repostWithQuote:(ANKPost *)post {
    RCComposeViewController *cpVC = [[RCComposeViewController alloc]initWithNibName:@"RCComposeViewController" bundle:[NSBundle mainBundle]];
    
    if (post != nil) cpVC.navTitle = @"Quote";
    
    [cpVC setInitialText:[NSString stringWithFormat:@" >> @%@: %@", post.user.username, post.text]];
    [cpVC setPostToReply:post];
    
    [self presentViewController:cpVC animated:YES completion:^{
        
    }];
}

- (void)replyToPost:(ANKPost *)post {
    RCComposeViewController *cpVC = [[RCComposeViewController alloc]initWithNibName:@"RCComposeViewController" bundle:[NSBundle mainBundle]];
    
    [cpVC setPostToReply:post];
    
    if (post != nil) cpVC.navTitle = @"Reply";
    
    [self presentViewController:cpVC animated:YES completion:^(void){
        
    }];
}

- (void)starPost:(ANKPost *)post1 {
    if (post1.isStarredByCurrentUser == TRUE) {
        //[WTStatusBar setStatusText:@"Unstarring post..." animated:YES];
        [[ANKClient sharedClient] unstarPostWithID:post1.postID completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                //[WTStatusBar setStatusText:@"Unstarred!" timeout:1.f animated:YES];
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Unstarred!";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                int i;
                for (i = 0; i < [array_posts count]; i++) {
                    RCPostCell *postCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    if (postCell.post.postID == post1.postID) {
                        [postCell setStarredButtonOn:FALSE];
                        postCell.post.isStarredByCurrentUser = !postCell.post.isStarredByCurrentUser;
                    }
                }
            }
        }];
    } else {
        //[WTStatusBar setStatusText:@"Starring post..." animated:YES];
        [[ANKClient sharedClient] starPostWithID:post1.postID completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                //[WTStatusBar setStatusText:@"Starred!" timeout:1.f animated:YES];
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Starred!";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                int i;
                for (i = 0; i < [array_posts count]; i++) {
                    RCPostCell *postCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    if (postCell.post.postID == post1.postID) {
                        [postCell setStarredButtonOn:TRUE];
                        postCell.post.isStarredByCurrentUser = !postCell.post.isStarredByCurrentUser;
                    }
                }
            }
        }];
    }
}

- (void)threadForPost:(ANKPost *)post1 {
    if (post1.repliesCount > 0 || post1.repliedToPostID != nil) {
        PostConversationDetailsViewController *pcdvc = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"PostConversationDetailsViewController"];
        [pcdvc setPost:post1];
        [[self navigationController] pushViewController:pcdvc animated:YES];
    } else {
        MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
        HUD.mode = MBProgressHUDModeText;
        [HUD setLabelText:@"Thread is empty"];
        [HUD hide:YES afterDelay:1.5];
    }
}

- (void)repostPost:(ANKPost *)post1 {
    if (post1.isRepostedByCurrentUser == TRUE) {
        //[WTStatusBar setStatusText:@"Unreposting post..." animated:YES];
        [[ANKClient sharedClient] unrepostPostWithID:post1.postID completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                //[WTStatusBar setStatusText:@"Unreposted!" timeout:1.f animated:YES];
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Unreposted!";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                int i;
                for (i = 0; i <= [array_posts count]; i++) {
                    RCPostCell *postCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    if (postCell.post.postID == post1.postID) {
                        [postCell setRepostedButtonOn:FALSE];
                        postCell.post.isRepostedByCurrentUser = !postCell.post.isRepostedByCurrentUser;
                    }
                }
            }
        }];
    } else {
        //[WTStatusBar setStatusText:@"Reposting post..." animated:YES];
        [[ANKClient sharedClient] repostPostWithID:post1.postID completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                //[WTStatusBar setStatusText:@"Reposted!" timeout:1.f animated:YES];
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Reposted";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                
        
                int i;
                for (i = 0; i <= [array_posts count]; i++) {
                    RCPostCell *postCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    if (postCell.post.postID == post1.postID) {
                        [postCell setRepostedButtonOn:TRUE];
                        postCell.post.isRepostedByCurrentUser = !postCell.post.isRepostedByCurrentUser;
                    }
                }
            }
        }];
    }
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)compose:(id)sender {
    if (sender != self) 
    [self replyToPost:nil];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    lastPostID = @"0";
    
    [self.navigationController setDelegate:self];
    [self.navigationController.navigationBar.topItem setTitle:self.title];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBarTexture"] forBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setTitleTextAttributes:@{UITextAttributeTextColor : [UIColor colorWithRed:113.f/255.f green:120.f/255.f blue:128.f/255.f alpha:1.0f], UITextAttributeTextShadowColor : [UIColor colorWithRed:238.f/255.f green:239.f/255.f blue:242.f/255.f alpha:1.0f], UITextAttributeTextShadowOffset : [NSValue valueWithCGSize:CGSizeMake(0, 1)]}];
//    [self.navigationController.navigationBar setClipsToBounds:YES];
//    
//    [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:135.f/255.f green:142.f/255.f blue:150.f/255.f alpha:1.f]];
    
    UIBarButtonItem *barButtonItemCompose = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(compose:)];
    self.navigationItem.rightBarButtonItem = barButtonItemCompose;
    
    UIBarButtonItem *barButtonItemSearch = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(openSearch:)];
    self.navigationItem.leftBarButtonItem = barButtonItemSearch;
    
    [tableView_posts registerNib:[UINib nibWithNibName:@"RCPostCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    array_posts = [[NSMutableArray alloc]init];
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self reloadPosts];
    });
    
    
    [tableView_posts addInfiniteScrollingWithActionHandler:^{
        [self reloadMorePosts];
    }];
    
//    self.navigationController.navigationBar.layer.shadowColor = [[UIColor blackColor] CGColor];
//    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.0, 3);
//    self.navigationController.navigationBar.layer.shadowOpacity = 0.25;
//    self.navigationController.navigationBar.layer.masksToBounds = NO;
//    self.navigationController.navigationBar.layer.shouldRasterize = YES;
//    
    tableView_posts.showsInfiniteScrolling = NO;
    refreshControl = [[UIRefreshControl alloc] init];
    
    [refreshControl addTarget:self action:@selector(reloadPosts) forControlEvents:UIControlEventValueChanged];
    [tableView_posts addSubview:refreshControl];
    
    // Do any additional setup after loading the view.
}

- (void)reloadPosts {
    [refreshControl beginRefreshing];
    ANKPaginationSettings *paginationSettings = [ANKPaginationSettings settingsWithCount:100];
    
    //ANKPaginationSettings *paginationSettings = [[ANKPaginationSettings alloc] init];
    [[[ANKClient sharedClient] clientWithPagination:paginationSettings] fetchPostsMentioningUser:[[RCUserStorage sharedUserStorage] userObject] completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
        if (error == nil) {
            NSMutableArray *posts = responseObject;
            
            NSMutableArray *nilPosts = @[].mutableCopy;
            for (ANKPost *post in posts) {
                if (post == nil || post.text == nil || post.user == nil) {
                    [nilPosts addObject:post];
                }
            }
            [posts removeObjectsInArray:nilPosts.copy];
            
            NSMutableArray *mutableArrayNewItems = [[NSMutableArray alloc] init];
            if (array_posts.count > 0 && array_posts != nil) {
                for (ANKPost *post in posts) {
                    if (post.postID.integerValue > [(ANKPost *)[array_posts objectAtIndex:0] postID].integerValue) [mutableArrayNewItems addObject:post];
                }
            } else {
                mutableArrayNewItems = [posts mutableCopy];
            }
            
            
            
            tableView_posts.showsInfiniteScrolling = YES;
            BOOL shouldScroll;
            if (array_posts.count > 0 && array_posts != nil) {
                shouldScroll = TRUE;
                array_posts = [[mutableArrayNewItems arrayByAddingObjectsFromArray:array_posts] mutableCopy];
            }
            else {
                shouldScroll = FALSE;
                array_posts = [mutableArrayNewItems mutableCopy];
            }
            
            if (!meta.moreDataAvailable) tableView_posts.showsInfiniteScrolling = NO;
            
            [tableView_posts.infiniteScrollingView stopAnimating];
            lastPostID = meta.minID;
            
            //Saving the offset. MAJIK TRYK!
            CGPoint offset = [tableView_posts contentOffset];
            //Reload the data, this way, the offset is reset.
            [tableView_posts reloadData];
            //Set a default value for the row size. Don't worry, will be fixed in the l00p.
            CGFloat newRowsSize = 0.f;
            //That's the l00p I was talking 'bout.
            int index = 0;
            for (index = 0; index < mutableArrayNewItems.count; index++) {
                //See the MAJIK?
                newRowsSize += [self tableView:tableView_posts heightForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            }
            //And the finale!
            offset.y += newRowsSize;
            if (offset.y > [tableView_posts contentSize].height) {
                offset.y = 0;
            }
            //Guess what? We only scroll if necessary!
            if (shouldScroll == TRUE) [tableView_posts setContentOffset:offset];
            //[WTStatusBar setStatusText:@"Reloaded!" timeout:1.5f animated:YES textColor:[UIColor colorWithRed:95.f/255.f green:180.f/255.f blue:35.f/255.f alpha:1.f]];
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
            [appWindow addSubview:HUD];
            HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
            HUD.mode = MBProgressHUDModeCustomView;
            HUD.labelText = @"Reloaded";
            [HUD show:YES];
            [HUD hide:YES afterDelay:1.5];
            [refreshControl endRefreshing];
        }
        else {
            //[WTStatusBar setStatusText:@"Error loading newer posts!" timeout:1.5f animated:YES textColor:[UIColor redColor]];
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
            [appWindow addSubview:HUD];
            HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
            HUD.mode = MBProgressHUDModeCustomView;
            HUD.labelText = @"Error loading posts";
            [HUD show:YES];
            [HUD hide:YES afterDelay:1.5];
            [refreshControl endRefreshing];
            [tableView_posts.infiniteScrollingView stopAnimating];
        }
    }];    
}

- (void)reloadMorePosts {
    ANKPaginationSettings *paginationSettings = [ANKPaginationSettings settingsWithCount:100];
    [paginationSettings setBeforeID:lastPostID];
    [paginationSettings setSinceID:@"0"];
    
    [[[ANKClient sharedClient] clientWithPagination:paginationSettings] fetchPostsMentioningUser:[[RCUserStorage sharedUserStorage] userObject] completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
        if (error == nil) {
            NSMutableArray *posts = responseObject;
            
            NSMutableArray *nilPosts = @[].mutableCopy;
            for (ANKPost *post in posts) {
                if (post == nil || post.text == nil || post.user == nil) {
                    [nilPosts addObject:post];
                }
            }
            [posts removeObjectsInArray:nilPosts.copy];
            
            
            if (array_posts.count > 0) array_posts = [[array_posts arrayByAddingObjectsFromArray:posts] mutableCopy];
            else array_posts = [posts mutableCopy];
            if (!meta.moreDataAvailable) tableView_posts.showsInfiniteScrolling = NO;
            [tableView_posts.infiniteScrollingView stopAnimating];
            lastPostID = meta.maxID;
            
            [tableView_posts reloadData];
            [refreshControl endRefreshing];
        } else {
            //[WTStatusBar setStatusText:@"Error loading older posts!" timeout:1.5f animated:YES textColor:[UIColor redColor]];
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
            [appWindow addSubview:HUD];
            HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
            HUD.mode = MBProgressHUDModeCustomView;
            HUD.labelText = @"Error loading posts";
            [HUD show:YES];
            [HUD hide:YES afterDelay:1.5];
            [refreshControl endRefreshing];
            [tableView_posts.infiniteScrollingView stopAnimating];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [refreshControl endRefreshing];
    
    for (RCComposeViewController *VC in [self childViewControllers]) {
        [VC.view removeFromSuperview];
        [VC removeFromParentViewController];
    }
    for (UIView *view in [self.view subviews]) {
        if (view.backgroundColor == [UIColor blackColor] && view.alpha == .7f) [view removeFromSuperview];
    }
    for (UIView *view in [self.navigationController.view subviews]) {
        if (view.backgroundColor == [UIColor blackColor] && view.alpha == .7f) [view removeFromSuperview];
    }
    
    if (array_posts.count > 0) {
        CGPoint offset = tableView_posts.contentOffset;
        NSMutableArray *array_itemsToRemove = @[].mutableCopy;
        for (ANKPost *post in array_posts) {
            if (post.isDeleted) [array_itemsToRemove addObject:post];
        }
        [array_posts removeObjectsInArray:array_itemsToRemove.copy];
        [tableView_posts reloadData];
        [tableView_posts setContentOffset:offset];
    }
    
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    int nowIndex = 0;
    for (nowIndex = 0; nowIndex <= array_posts.count; nowIndex = nowIndex + 1) {
        RCPostCell *currentCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:nowIndex inSection:0]];
        [currentCell hideMiniOptions];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

- (void)cell:(RCPostCell *)cell wantsTo:(RCPostCellAction)action {
    if (action == RCPostCellActionReply) {
        [self replyToPost:cell.post];
    } else if (action == RCPostCellActionRepost) {
        NSString *stringRepostButton = @"Repost";
        if ([cell.post isRepostedByCurrentUser]) stringRepostButton = @"Unrepost";
        //actionSheet_repost = [[RCActionSheet alloc] initWithTitle:@"Repost options:" post:cell.post delegate:self otherButtonTitles:@[stringRepostButton, @"Repost with comment", @"Cancel"]];
//        actionSheet_repost = [[RCActionSheet alloc] initWithTitle:@"Repost options:"  delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:stringRepostButton, @"Repost with comment", nil];
        actionSheet_repost = [[JLActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@[stringRepostButton, @"Repost with comment"]];
        [actionSheet_repost setStyle:JLSTYLE_SUPERCLEAN];
        [actionSheet_repost setClickedButtonBlock:^(JLActionSheet *actionSheet, NSInteger clickedButtonIndex) {
            if (clickedButtonIndex == 2) {
                //Repost
                [self repostPost:cell.post];
            } else if (clickedButtonIndex == 1) {
                [self repostWithQuote:cell.post];
            }
        }];
        [actionSheet_repost showInView:self.view];
    } else if (action == RCPostCellActionStar) {
        [self starPost:cell.post];
    } else if (action == RCPostCellActionThread) {
        [self threadForPost:cell.post];
    }
}

//- (void)didDisplayMiniOptionsForCell:(RCPostCell *)cell {
//    int nowIndex = 0;
//    for (nowIndex = 0; nowIndex <= array_posts.count; nowIndex = nowIndex + 1) {
//        RCPostCell *currentCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:nowIndex inSection:0]];
//        if (currentCell != cell) {
//            if (currentCell.isShowingMiniOptions) [currentCell hideMiniOptions];
//        }
//    }
//}

- (void)cell:(RCPostCell *)cell didMakeSwipeMotionWithDirection:(RCDirection)direction {
    if (direction == RCDirectionLeftToRight) {
        int nowIndex = 0;
        for (nowIndex = 0; nowIndex <= array_posts.count; nowIndex = nowIndex + 1) {
            RCPostCell *currentCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:nowIndex inSection:0]];
            if (currentCell != cell) {
                if (currentCell.isShowingMiniOptions) [currentCell hideMiniOptions];
            }
        }
    }
    
}



#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [array_posts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"Cell";
    RCPostCell *cell = (RCPostCell *)[tableView_posts dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) cell = [[RCPostCell alloc] init];
    cell.indexPath = indexPath;
    cell.delegate = self;
    
    ANKPost *currentPost = (ANKPost *)[array_posts objectAtIndex:indexPath.row];
    ANKPost *originalPost = currentPost;
    if ([currentPost repostedPost] != nil) currentPost = [currentPost repostedPost];
    [[RCUserListStorage sharedInstance] saveUser:currentPost.user.username];
    
    
    cell.label_username.text = [NSString stringWithFormat:@"@%@", currentPost.user.username];
    
#pragma mark - Start of colored stuff
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:currentPost.text attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Light" size:15.f], NSForegroundColorAttributeName : [UIColor blackColor]}];
    
    
    
    for (id object in currentPost.entities.mentions) {
        [string addAttributes:@{NSForegroundColorAttributeName : defaultBlue} range:[currentPost.text rangeOfString:[NSString stringWithFormat:@"@%@", [object username]]]];
    }
    
    //Disable for links = improved reading...
    //    for (id object in currentPost.entities.links) {
    //        [string addAttributes:@{NSForegroundColorAttributeName : defaultBlue} range:[object range]];
    //    }
    
    [cell.label_post setAttributedText:string];
    
    
    
    
    
    if (currentPost == [originalPost repostedPost]) {
        NSMutableAttributedString *attr1 = cell.label_post.attributedText.mutableCopy;
        NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n\nReposted by @%@", originalPost.user.username]];
        [attr1 appendAttributedString:attr2];
        cell.label_post.attributedText = attr1;
    }
#pragma mark - End of colored stuff.
    
//    cell.label_post.text = currentPost.text;
//    if (currentPost == [originalPost repostedPost]) {
//        cell.label_post.text = [cell.label_post.text stringByAppendingString:[NSString stringWithFormat:@"\n\nReposted by @%@", originalPost.user.username]];
//    }
    [cell setContentViewBackgorundColor:[UIColor whiteColor]];
    cell.imageView_userImage.image = [UIImage imageNamed:@"placeholder"];
    
    NSDate *now = [NSDate date];
    
    NSTimeInterval timeInterval = [now timeIntervalSinceDate:currentPost.createdAt];
    NSUInteger minutes = timeInterval/60;
    NSUInteger seconds = timeInterval/1;
    NSUInteger hours = timeInterval/3600;
    NSString *finalTime = [NSString stringWithFormat:@"%i min(s) ", minutes];
    if (timeInterval <= 60) {
        finalTime = [NSString stringWithFormat:@"%i sec(s) ", seconds];
    }else if (timeInterval >= 3600) {
        finalTime = [NSString stringWithFormat:@"%i hour(s) ", hours];
    }
    
    cell.label_longAgo.text = finalTime;
    
    cell.post = currentPost;
    
    NSString *finalURLString = [currentPost.user.avatarImage.URL.absoluteString stringByAppendingString:@"?h=120&w=120"];
    NSURL *userURL = [NSURL URLWithString:finalURLString];
    
    [cell.imageView_userImage setImageWithURL:userURL placeholderImage:[UIImage imageNamed:@"placeholder"]];
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    for (UITableViewCell *cell in tableView_posts.visibleCells) {
        if ([cell.class isSubclassOfClass:[UITableViewCell class]]) {
            [(RCPostCell *)cell hideMiniOptions];
        }
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    ANKPost *currentPost = (ANKPost *)[array_posts objectAtIndex:indexPath.row];
    ANKPost *orignialPost = currentPost;
    if ([currentPost repostedPost] != nil) currentPost = [currentPost repostedPost];
    NSString *cellText =  [currentPost text];
    if (orignialPost != currentPost) cellText = [cellText stringByAppendingString:[NSString stringWithFormat:@"\n\nReposted by @%@", orignialPost.user.username]];
    UIFont *cellFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.f];
    CGSize constraintSize = CGSizeMake(231.f, 5000.f);
    CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    
    return labelSize.height + 55.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PostDetailViewController *postDetailViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"PostDetailViewController"];
    
    [postDetailViewController setPost:[array_posts objectAtIndex:indexPath.row]];
    if ([[array_posts objectAtIndex:indexPath.row] repostedPost]) {
        [postDetailViewController setPost:[[array_posts objectAtIndex:indexPath.row] repostedPost]];
    }
    [postDetailViewController setIndexPath:indexPath];
    [[self navigationController] pushViewController:postDetailViewController animated:YES];
}

#pragma mark NavBarDelegate


- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (viewController == self) {
        [self.navigationController.navigationBar.topItem setTitle:self.title];
        if ([self.navigationController.parentViewController isKindOfClass:[SNTabBarController class]]) {
            [(SNTabBarController *)[self.navigationController parentViewController] showBottomBar];
        }
    } else {
        if ([self.navigationController.parentViewController isKindOfClass:[SNTabBarController class]]) {
            [(SNTabBarController *)[self.navigationController parentViewController] hideBottomBar];
        }
    }
}

#pragma mark - Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        //Repost
        [self repostPost:[(RCActionSheet *)actionSheet post]];
    } else if (buttonIndex == 1) {
        [self repostWithQuote:[(RCActionSheet *)actionSheet post]];
    }
}

@end
