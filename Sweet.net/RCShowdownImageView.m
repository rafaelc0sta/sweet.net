//
//  RCShowdownImageView.m
//  Sweet.net
//
//  Created by Rafael Costa on 1/3/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import "RCShowdownImageView.h"
#import "UIGestureRecognizer+Blocks.h"

@implementation RCShowdownImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        CGRect viewFrame = self.frame;
        viewFrame.size.height -= 60;
        viewFrame.origin.y += 30;
        scrollView = [[UIScrollView alloc] initWithFrame:viewFrame];
        [scrollView setShowsHorizontalScrollIndicator:FALSE];
        [scrollView setShowsVerticalScrollIndicator:FALSE];
        [scrollView setDelegate:self];
        [self addSubview:scrollView];
        
        progressView = [[MBBarProgressView alloc] initWithFrame:CGRectMake(frame.size.width/2 - 250/2, frame.size.height/2 - 27/2, 250, 27)];
        progressView.hidden = false;
        [self addSubview:progressView];
        
        [self setUserInteractionEnabled:YES];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithActionBlock:^(UIGestureRecognizer *gesture) {
            [self hideView];
        }];
        [self addGestureRecognizer:tapGesture];
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.70]];
    }
    return self;
}

- (void)showViewToView:(UIView *)superview {
    [superview addSubview:self];
    [self setAlpha:0.f];
    [UIView animateWithDuration:0.35f animations:^{
        [self setAlpha:1.f];
    } completion:^(BOOL finished) {
        //None.
    }];
}

- (void)setImageWithURL:(NSURL *)url {
    progressView.hidden = false;
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    AFHTTPRequestOperation *httpRequest = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [httpRequest setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        float percent = (float)totalBytesRead/(float)totalBytesExpectedToRead;
        [progressView setProgress:percent];
    }];
    [httpRequest setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSData *imageData = responseObject;
        UIImage *image = [UIImage imageWithData:imageData];
        [self setImage:image];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideView];
    }];
    [httpRequest start];
}

- (void)hideView {
    [self setAlpha:1.f];
    [UIView animateWithDuration:0.35f animations:^{
        [self setAlpha:0.f];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)setImage:(UIImage *)image {
    imageView = [[UIImageView alloc] initWithImage:image];
    
    scrollView.contentSize = imageView.frame.size;
    [scrollView setContentMode:UIViewContentModeCenter];
    [scrollView addSubview:imageView];
    if (imageView.frame.size.height > imageView.frame.size.width) {
        scrollView.minimumZoomScale = scrollView.frame.size.height / imageView.frame.size.height;
    } else {
        scrollView.minimumZoomScale = scrollView.frame.size.width / imageView.frame.size.width;
    }
    
    scrollView.maximumZoomScale = 2.0;
    [scrollView setZoomScale:scrollView.minimumZoomScale];
    
    [progressView setHidden:YES];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    UIView *subView = [scrollView.subviews objectAtIndex:0];
    
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


@end
