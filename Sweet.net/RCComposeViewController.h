//
//  RCComposeViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 12/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADNKit.h"
#import "AutoCompleteViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import "JLActionSheet.h"
#import "RCKit.h"
#import "RCAutoCompleteView.h"

@interface RCComposeViewController : UIViewController <UITextViewDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UIToolbarDelegate,AutoCompleteDelegate, UIAlertViewDelegate, UIImagePickerControllerDelegate, JLActionSheetDelegate, RCAutoCompleteViewDelegate> {
    IBOutlet UINavigationBar *navigationBar;
    
    IBOutlet UITextView *textView_post;
    
    IBOutlet UILabel *label_counter;
    
    IBOutlet UIBarButtonItem *barButtonItem_cancel;
    IBOutlet UIBarButtonItem *barButtonItem_post;
    
    IBOutlet UIButton *button_nowPlaying;
    IBOutlet UIButton *button_getPicture;
    IBOutlet UIButton *button_addLocation;
    IBOutlet UIButton *button_addTwitterSharing;
    IBOutlet UIButton *button_hideKeyboard;
    
    IBOutlet UILabel *label_replyingTo;
    
    ANKPost *currentPost;

    BOOL hasAddedFile;
    BOOL hasAddedLocation;
    
    CLLocationManager *locationManager;
    CLLocation *location;
    
    ACAccount *twitterAccount;
    ACAccountType *accountType_twitter;
    ACAccountStore *sharedAccountStore;
    NSArray *accountArray;
    
    RCAutoCompleteView *autoCompleteView;
}
@property (nonatomic) int charLimit;

@property (nonatomic) UIImage *imageAttached;

@property (nonatomic) NSString *navTitle;
@property (nonatomic) ANKPost *postToReply;
@property (nonatomic) NSString *initialText;
- (void)setPostToReply:(ANKPost *)postToReply;
- (IBAction)post:(id)sender;
- (IBAction)dismiss:(id)sender;
- (IBAction)addNowPlaying:(id)sender;
- (IBAction)addLocation:(id)sender;
- (IBAction)openAutoComplete:(id)sender;

- (IBAction)addTwitterSharing:(id)sender;

- (void)addTextToPost:(NSString *)text;

- (IBAction)hideKeyboard:(id)sender;
@end
