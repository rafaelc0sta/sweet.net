//
//  RCCustomTabViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 21/06/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "RCCustomTabViewController.h"

@interface RCCustomTabViewController ()

@end

@implementation RCCustomTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
