//
//  AppDelegate.h
//  Sweet.net
//
//  Created by Rafael Costa on 11/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADNLogin.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate, ADNLoginDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ADNLogin *login;
@end
