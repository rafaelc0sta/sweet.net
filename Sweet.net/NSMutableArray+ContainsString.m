//
//  NSArray+ContainsString.m
//  SweetTweet7
//
//  Created by Rafael Costa on 25/12/12.
//  Copyright (c) 2012 Rafael Costa. All rights reserved.
//

#import "NSMutableArray+ContainsString.h"

@implementation NSMutableArray (ContainsString)
-(BOOL) containsString:(NSString*)string
{
    for (NSString* str in self) {
        if ([str isEqualToString:string])
            return YES;
    }
    return NO;
}
@end
