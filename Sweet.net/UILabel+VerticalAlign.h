//
//  UILabel+VerticalAlign.h
//  Sweet.net
//
//  Created by Rafael Costa on 13/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark VerticalAlign
@interface UILabel (VerticalAlign)
- (void)alignTop;
- (void)alignBottom;
@end