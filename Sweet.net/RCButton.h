//
//  RCButton.h
//  Sweet.net
//
//  Created by Rafael Costa on 2/18/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCButton : UIButton {
    BOOL tinted;
}
- (void)setButtonTinted:(BOOL)boolean;
//@property (nonatomic) UIColor *tintColor;
@end
