//
//  RCNavigationController.m
//  Sweet.net
//
//  Created by Rafael Costa on 17/04/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "RCNavigationController.h"

@interface RCNavigationController ()

@end

@implementation RCNavigationController

- (NSUInteger)supportedInterfaceOrientations {
    return [[self topViewController] supportedInterfaceOrientations];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) self.edgesForExtendedLayout = UIExtendedEdgeAll;
    [self.navigationBar setTranslucent:YES];
    NSLog(@"View Controller count: %i", self.viewControllers.count);
//    [self.navigationBar setBarTintColor:[UIColor whiteColor]];
	// Do any additional setup after loading the view.
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.visibleViewController.view layoutSubviews];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationBar setTintColor:[UIApplication sharedApplication].delegate.window.tintColor];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
