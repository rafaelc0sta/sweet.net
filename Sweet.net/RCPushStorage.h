//
//  RCPushStorage.h
//  Sweet.net
//
//  Created by Rafael Costa on 12/25/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RCPushStorage : NSObject

@property (nonatomic) NSString *deviceToken;

+ (instancetype)sharedPushStorage;
@end
