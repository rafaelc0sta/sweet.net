//
//  LocationMapViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 09/05/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "RCKit.h"

@interface LocationMapViewController : UIViewController <MKMapViewDelegate> {
    IBOutlet MKMapView *mapView;
}
@property (nonatomic) CLLocationCoordinate2D location2D;
@end
