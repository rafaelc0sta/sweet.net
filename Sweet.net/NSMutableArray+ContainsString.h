//
//  NSArray+ContainsString.h
//  SweetTweet7
//
//  Created by Rafael Costa on 25/12/12.
//  Copyright (c) 2012 Rafael Costa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (ContainsString)
-(BOOL) containsString:(NSString*)string;
@end
