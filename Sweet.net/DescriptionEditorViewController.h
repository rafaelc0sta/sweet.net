//
//  DescriptionEditorViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 11/07/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditProfileViewController.h"

@interface DescriptionEditorViewController : UIViewController <UITextViewDelegate, UIBarPositioningDelegate> {
    IBOutlet UITextView *textView_bio;
    
    EditProfileViewController *presentingViewController;
}
@property (nonatomic) NSString *currentBio;

- (void)postPresentationUpdates;

- (IBAction)save:(id)sender;
- (IBAction)cancel:(id)sender;
@end
