//
//  EditProfileViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 20/05/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "EditProfileViewController.h"
#import "UIImageView+WebCache.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#include "DescriptionEditorViewController.h"

@interface EditProfileViewController ()

@end

@implementation EditProfileViewController

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

- (IBAction)textFieldDone:(id)sender {
    [sender resignFirstResponder];
}


- (void)modifyBioToBeSent:(NSString *)newBio {
    textView_description.text = newBio;
}

- (IBAction)changeUserImage:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:self];
    [picker setAllowsEditing:YES];
    [picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}

- (IBAction)changeUserBio:(id)sender {
    DescriptionEditorViewController *descriptionEditor = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"DescriptionEditorViewController"];
    [descriptionEditor setCurrentBio:textView_description.text];
    [self presentViewController:descriptionEditor animated:YES completion:^{
        
    }];
}

- (IBAction)saveUserProfile:(id)sender {
    
    if (self.hasChangedProfileImage || textField_name.text.length > 0 || self.hasChangedProfileBio) {
        NSLog(@"Has changed something.");
        if (self.hasChangedProfileImage) {
            [[ANKClient sharedClient] updateCurrentUserAvatarWithImageData:UIImagePNGRepresentation(imageView_userImage.image) mimeType:@"image/png" completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
                if (error == nil) {
                    [MBProgressHUD hideAllHUDsForView:appWindow animated:YES];
                    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
                    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                    HUD.mode = MBProgressHUDModeCustomView;
                    HUD.labelText = @"Uploaded profile picture!";
                    [HUD show:YES];
                    [HUD hide:YES afterDelay:1.5];
                    updatePhaseOneComplete = TRUE;
                    [self dismissIfHasFinishedUpdating];
                } else {
                    UIAlertView *alertaErro = [[UIAlertView alloc] initWithTitle:@"Error" message:error.description delegate:nil cancelButtonTitle:@"Ok." otherButtonTitles: nil];
                    [alertaErro show];
                    updatePhaseTwoComplete = TRUE;
                    [self dismissIfHasFinishedUpdating];
                }
            }];
        } else {
            updatePhaseOneComplete = TRUE;
            [self dismissIfHasFinishedUpdating];
        }
        
        
        
        NSString *finalName = textField_name.text;
        if (textField_name.text.length == 0) finalName = self.user.name;
        
        
        [[ANKClient sharedClient] updateCurrentUserName:finalName locale:self.user.locale timezone:self.user.timezone descriptionText:textView_description.text completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                [MBProgressHUD hideAllHUDsForView:appWindow animated:YES];
                MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Updated User Profile!";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                updatePhaseTwoComplete = TRUE;
                [self dismissIfHasFinishedUpdating];
            } else {
                UIAlertView *alertaErro = [[UIAlertView alloc] initWithTitle:@"Error" message:error.description delegate:nil cancelButtonTitle:@"Ok." otherButtonTitles: nil];
                [alertaErro show];
                updatePhaseTwoComplete = TRUE;
                [self dismissIfHasFinishedUpdating];
            }
            
        }];
    } else  {
        updatePhaseOneComplete = TRUE;
        updatePhaseTwoComplete = updatePhaseOneComplete;
        [self dismissIfHasFinishedUpdating];
    }
    
    
    
}
        
- (void)dismissIfHasFinishedUpdating {
    
    if (updatePhaseOneComplete == TRUE && updatePhaseTwoComplete == TRUE) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
            
}

- (IBAction)cancelChanges:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        // DO NOTHING.
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [button_editDescription setTitleColor:[[[UIApplication sharedApplication] delegate] window].tintColor forState:UIControlStateNormal];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeUserImage:)];
    [imageView_userImage addGestureRecognizer:tapGestureRecognizer];
    [imageView_userImage setUserInteractionEnabled:YES];
    //[button_editDescription setTintColor:self.view.tintColor];
    [self updateUserProperties];
	// Do any additional setup after loading the view.
}

- (void)updateUserProperties {
    textField_name.placeholder = self.user.name;
    label_username.text = [NSString stringWithFormat:@"@%@ (#%@)", self.user.username, self.user.userID];
    textView_description.text = self.user.bio.text;
    
    [imageView_userImage setImageWithURL:self.user.avatarImage.URL placeholderImage:[UIImage imageNamed:@"user"]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //[imageView_userImage set]
    [imageView_userImage setImage:[info objectForKey:UIImagePickerControllerEditedImage]];
    [picker dismissViewControllerAnimated:YES completion:^{
        self.hasChangedProfileImage = YES;
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
