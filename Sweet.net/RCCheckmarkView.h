//
//  RCCheckmarkView.h
//  Sweet.net
//
//  Created by Rafael Costa on 16/06/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCCheckmarkView : UIImageView
- (id)initWithPosition:(CGPoint)position superview:(UIView *)superview andTimeout:(float)timeout;
@property (nonatomic) float animationDuration;
@end
