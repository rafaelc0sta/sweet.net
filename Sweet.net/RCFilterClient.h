//
//  RCFilterClient.h
//  Sweet.net
//
//  Created by Rafael Costa on 1/18/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import "RCFilter.h"

@interface RCFilterClient : RCFilter
- (BOOL)shouldRemovePost:(id)filteree;
@end
