//
//  RCTabViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 25/06/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCKit.h"

@interface RCTabViewController : UIViewController {
    IBOutlet RCMaskedImageView *maskedUserImage;
    IBOutlet UIButton *button_timeline;
    IBOutlet UIButton *button_mentions;
    IBOutlet UIButton *button_dms;
}

@end
