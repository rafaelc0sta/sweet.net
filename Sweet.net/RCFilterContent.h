//
//  RCFilterContent.h
//  Sweet.net
//
//  Created by Rafael Costa on 1/17/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import "RCFilter.h"
#import "ADNKit.h"

@interface RCFilterContent : RCFilter
- (BOOL)shouldRemovePost:(id)filteree;
@end
