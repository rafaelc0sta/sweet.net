//
//  UserDetailViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 20/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADNKit.h"

#import <QuartzCore/QuartzCore.h>
#import "AGMedallionView.h"
#import "RCKit.h"
#import "RCCellButton.h"
#import "MBProgressHUD.h"

@interface UserDetailViewController : UIViewController <UIScrollViewDelegate> {
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *scrollViewSubview;
    
    IBOutlet UILabel *label_username;
    IBOutlet UILabel *label_userRealName;
    IBOutlet UILabel *label_followsYou;
    
    IBOutlet UIImageView *imageView_userBG;
    IBOutlet UIImageView *imageView_userBGMask;
    IBOutlet RCMaskedImageView *imageView_userImage;
    IBOutlet UIImageView *imageView_verified;
    IBOutlet UIButton *button_followers;
    IBOutlet UIButton *button_following;
    
    IBOutlet RCButton *button_follow;
    IBOutlet RCButton *button_mute;

    IBOutlet UITextView *textView_userDescription;
    
    //LoadingViewController *load;
    
    CGRect frame_imageViewUserBG;
    
    NSMutableArray *array_MWPhotos;
}
@property (nonatomic) ANKUser *user;
@property (nonatomic) NSString *usernameID;

- (IBAction)muteOrUnmute:(id)sender;
- (IBAction)followOrUnfollow:(id)sender;

- (IBAction)showFollowers:(id)sender;
- (IBAction)showFollowing:(id)sender;
- (IBAction)showPosts:(id)sender;
- (IBAction)showStars:(id)sender;
@end
