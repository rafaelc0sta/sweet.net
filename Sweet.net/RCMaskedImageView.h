//
//  RCMaskedImageView.h
//  Sweet.net
//
//  Created by Rafael Costa on 13/06/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCMaskedImageView : UIImageView

@end
