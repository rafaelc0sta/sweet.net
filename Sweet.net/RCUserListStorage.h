//
//  RCUserListStorage.h
//  SweetTweet6
//
//  Created by Rafael Costa on 01/12/12.
//  Copyright (c) 2012 Rafael Costa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSMutableArray+ContainsString.h"
#import "ADNKit.h"

//typedef enum {
//    RCUserListTypeConversation,
//    RCUserListTypeFollowing
//} RCUserListType;

@interface RCUserListStorage : NSObject
+ (RCUserListStorage *)sharedInstance;
- (NSMutableArray *)userList;
- (void)saveUser:(NSString *)user;
- (void)clearUserDB;
@end
