//
//  RCPushStorage.m
//  Sweet.net
//
//  Created by Rafael Costa on 12/25/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "RCPushStorage.h"

@implementation RCPushStorage
+ (instancetype)sharedPushStorage {
    @synchronized(self) {
        static RCPushStorage *pushStorage;
        if (pushStorage == nil) {
            pushStorage = [[self alloc] init];
        }
        return pushStorage;
    }
}
@end
