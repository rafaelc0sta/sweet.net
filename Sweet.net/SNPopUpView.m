//
//  SNPopUpView.m
//  Sweet.net
//
//  Created by Rafael Costa on 12/27/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "SNPopUpView.h"
#import "AppDelegate.h"

@implementation SNPopUpView

- (id)initWithFrame:(CGRect)frame withTitles:(NSArray *)titles
{
    frame.size.height = 50*titles.count;
    frame.size.width = 280;
    self = [super initWithFrame:frame];
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    greenishBG = delegate.window.tintColor;
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
        self.blurRadius = 20;
        self.iterations = 1;
        
        NSLog(@"%f", [[UIScreen mainScreen] applicationFrame].size.height);
        
        if ([[UIScreen mainScreen] applicationFrame].size.height >= 548.f) {
            self.dynamic = YES;
            NSLog(@"Dynamic Blur enabled");
        } else {
            NSLog(@"Dynamic Blur disabled.");
            self.dynamic = NO;
        }
        self.tintColor = [UIColor clearColor];
        
        views = [NSMutableArray new];
        
        
        for (int i = 0; i < titles.count; i++) {
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, i * 50, frame.size.width, 50)];

            [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:.75]];
            
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
            [button setBackgroundColor:[UIColor clearColor]];
            [button setTitleColor:greenishBG forState:UIControlStateNormal];
            [button setTitle:[titles objectAtIndex:i] forState:UIControlStateNormal];
            [button setTag:100 + i];
            [button addTarget:self action:@selector(buttonOnClick:) forControlEvents:UIControlEventTouchUpInside];
            
            UIView *viewSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, i * 50, frame.size.width, 1)];
            [viewSeparator setBackgroundColor:[UIColor clearColor]];
            if ((i*50) != 0) {
                [viewSeparator setBackgroundColor:[UIColor lightGrayColor]];
            }
            
            [self addSubview:view];
            [view addSubview:button];
            [self addSubview:viewSeparator];
            [views addObject:view];
            self.layer.cornerRadius = 20;
            self.clipsToBounds = YES;
            [self setSelectedIndex:0];
        }
    }
    return self;
}

- (void)buttonOnClick:(id)sender {
    int selectedIndex = [sender tag] - 100;
    [_delegate popUpView:self didSelectItemAtIndex:selectedIndex];
}

- (void)setSelectedIndex:(int)selectedIndex {
    for (int i = 0; i < views.count; i++) {
        UIView *view = [views objectAtIndex:i];
        
        [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:.75]];
        [(UIButton *)[view.subviews objectAtIndex:0] setTitleColor:greenishBG forState:UIControlStateNormal];
        if (i == selectedIndex) {
            [view setBackgroundColor:[greenishBG colorWithAlphaComponent:.75]];
            [(UIButton *)[view.subviews objectAtIndex:0] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
