//
//  SearchViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 30/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCKit.h"

@interface SearchViewController : UIViewController <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate> {
    IBOutlet UITableView *tableView_results;
    
    NSMutableArray *array_users;
    
    RCActionSheet *actionSheet_repost;
    
    IBOutlet UISearchBar *searchBar;
    
    NSUInteger searchPriority;
}

@end
