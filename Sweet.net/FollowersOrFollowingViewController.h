//
//  FollowersOrFollowingViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 25/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADNKit.h"
#import "SVPullToRefresh.h"
#import "RCKit.h"

typedef enum {
  ViewTypeFollowers,
  ViewTypeFollowing
} ViewType;

@interface FollowersOrFollowingViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UITableView *tableView_users;
    
    NSMutableArray *array_users;
    
    NSString *lastId;
}
@property (nonatomic) ViewType viewType;
@property (nonatomic) NSString *userId;
@end
