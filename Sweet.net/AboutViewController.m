//
//  AboutViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 20/04/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "AboutViewController.h"
#import "RCKit.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

- (IBAction)done:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    tableView.delegate = self;
    tableView.dataSource = self;
    
    licenses = @[@"Sweet.net", @"Sweet.net (Privacy Policy)"].mutableCopy;
    
    NSString *bundleRoot = [[NSBundle mainBundle] bundlePath];
    fm = [NSFileManager defaultManager];
    NSArray *dirContents = [fm contentsOfDirectoryAtPath:bundleRoot error:nil];
    NSPredicate *fltr = [NSPredicate predicateWithFormat:@"self ENDSWITH '.license'"];
    
    for (NSString *file in [dirContents filteredArrayUsingPredicate:fltr]) {
        if ([file rangeOfString:@"Sweet.net"].location == NSNotFound)
        [licenses addObject:[file stringByReplacingOccurrencesOfString:@".license" withString:@""]];
    }
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == 1) return @"Acknowledgements";
    return @"App licenses";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) return 2;
    return licenses.count - 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    if (indexPath.section == 1) row += 2;
    NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSString *license = [licenses objectAtIndex:row];
    

    cell.textLabel.text = license;
    
    [cell layoutSubviews];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 44.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSInteger row = indexPath.row;
    if (indexPath.section == 1) row += 2;
    NSString *filename = [licenses objectAtIndex:row];
    
    NSString *path = [NSString stringWithFormat:@"%@/%@.license", [[NSBundle mainBundle] bundlePath], filename];
    
    NSString *licenseText = [NSString stringWithUTF8String:[fm contentsAtPath:path].bytes];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:filename message:licenseText delegate:nil cancelButtonTitle:@"Ok." otherButtonTitles: nil];
    [alert show];
}


@end
