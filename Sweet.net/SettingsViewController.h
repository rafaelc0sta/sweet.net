//
//  SettingsViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 17/04/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCKit.h"

@interface SettingsViewController : UIViewController <UIAlertViewDelegate, UIBarPositioningDelegate> {
    IBOutlet UISwitch *switch_push;
    
    IBOutlet UINavigationBar *navBar;
    
    NSUInteger shakeCount;
    
    BOOL pushEnabled;
    
    IBOutlet UILabel *label_none;
    
    IBOutlet UIView *view_pushSettings;
}
- (IBAction)clearAutocompleteCache:(id)sender;
- (IBAction)changePush:(id)sender;
- (IBAction)done:(id)sender;
@end
