//
//  TimelineViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 12/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCKit.h"
#import <Security/Security.h>
#import <CommonCrypto/CommonCrypto.h>
#import <MediaPlayer/MediaPlayer.h>
#import "JLActionSheet.h"

@interface TimelineViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, RCPostCellDelegate> {
    //RCActionSheet *actionSheet_repost;
    JLActionSheet *actionSheet_repost;
    IBOutlet UITableView *tableView_posts;
    
    IBOutlet UINavigationBar *navBar;
    NSMutableArray *array_posts;
    
    UIRefreshControl *refreshControl;
    
    NSString *lastPostID;
    
    
    int storedNewItems;
}
- (IBAction)compose:(id)sender;
- (IBAction)openSearch:(id)sender;
@end
