//
//  AutoCompleteViewController.h
//  SweetTweet7
//
//  Created by Rafael Costa on 25/12/12.
//  Copyright (c) 2012 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCUserListStorage.h"

@protocol AutoCompleteDelegate;

@interface AutoCompleteViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIToolbarDelegate>
@property (nonatomic) IBOutlet UITextField *textField;
@property (nonatomic) NSMutableArray *mutableArray_FollowingSavedCompletions;
@property (nonatomic) NSMutableArray *mutableArray_ConversationSavedCompletions;
@property (nonatomic) NSMutableArray *usableSavedCompletions;
@property (nonatomic) NSMutableArray *mutableArray_completions;
@property (nonatomic) NSMutableArray *mutableArray_alreadyCompleted;
@property (nonatomic) IBOutlet UITableView *autocompleteTableView;
@property (nonatomic) NSString *stringTyped;
@property (nonatomic) IBOutlet UIToolbar *toolbar;

@property (nonatomic) id<AutoCompleteDelegate> delegate;

- (IBAction)cancelar:(id)sender;
- (IBAction)textChanged:(id)sender;
@end

@protocol AutoCompleteDelegate <NSObject>

- (void)autoComplete:(AutoCompleteViewController *)autoComplete didReturnWithUsername:(NSString *)username andUserID:(NSString *)userID;

@end
