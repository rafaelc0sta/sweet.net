//
//  LoginViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 16/04/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCKit.h"
#import "KTLLoginViewController.h"
#import "AGMedallionView.h"
#import "MBProgressHUD.h"
#import "EAIntroPage.h"
#import "EAIntroView.h"

@interface LoginViewController : UIViewController <KTLLoginViewControllerDelegate, EAIntroDelegate> {
    IBOutlet UIImageView *imageView;
    
    BOOL shouldEnablePush;
    
}
@property (nonatomic) NSDictionary *payload;

@property (nonatomic) BOOL loggedFromADNApp;
@end
