//
//  RCPostCell.m
//  Sweet.net
//
//  Created by Rafael Costa on 13/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "RCPostCell.h"
#import "PostConversationDetailsViewController.h"

@implementation RCPostCell

- (void)setMiniOptionsEnabled:(BOOL)isMiniOptionsEnabled {
    if (!isMiniOptionsEnabled) {
        [miniOptionsGesture removeTarget:self action:@selector(showMiniOptions)];
        [self setThreadOptionsEnabled:NO];
    } else {
        [miniOptionsGesture addTarget:self action:@selector(showMiniOptions)];
        [self setThreadOptionsEnabled:YES];
    }
}

- (void)setThreadOptionsEnabled:(BOOL)isThreadOptionsEnabled {
    if (!isThreadOptionsEnabled) {
        [conversationGesture removeTarget:self action:@selector(showThread)];
    } else {
        [conversationGesture addTarget:self action:@selector(showThread)];
    }
}

- (void)setContentViewBackgorundColor:(UIColor *)color {
    postView.backgroundColor = color;
}

- (void)setRepostedButtonOn:(BOOL)on {
    if (on == TRUE) [button_repost setImage:[button_repost.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    else if (on == FALSE) [button_repost setImage:[button_repost.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
}

- (void)setStarredButtonOn:(BOOL)on {
    if (on == TRUE) [button_star setImage:[button_star.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    else if (on == FALSE) [button_star setImage:[button_star.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
}


- (IBAction)replyToThisPost:(id)sender {
    [self.delegate cell:self wantsTo:RCPostCellActionReply];
}

- (IBAction)repostThisPost:(id)sender {
    [self.delegate cell:self wantsTo:RCPostCellActionRepost];
}

- (IBAction)starThisPost:(id)sender {
    [self.delegate cell:self wantsTo:RCPostCellActionStar];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		// TO-DO: Add something here.
	}
    return self;
}

- (void)layoutSubviews {
    [self hideMiniOptions];
    [super layoutSubviews];
}

-(void)awakeFromNib {
    [super awakeFromNib];
    
    self.accessoryView.backgroundColor = [UIColor clearColor];
    
    [self setClipsToBounds:YES];
    
    overlayView.frame = CGRectMake(overlayView.frame.size.width, overlayView.frame.origin.y, overlayView.frame.size.width, overlayView.frame.size.height);
    
    postView.frame = CGRectMake(0, 420, 320, 420);
    
    miniOptionsGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(showMiniOptions)];
    [miniOptionsGesture setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self addGestureRecognizer:miniOptionsGesture];
    
    conversationGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(showThread)];
    [conversationGesture setDirection:UISwipeGestureRecognizerDirectionRight];
    [self addGestureRecognizer:conversationGesture];
    
    [self.label_post alignTop];
    
    isShowingMiniOptions = FALSE;
    
    overlayView.hidden = YES;
    threadView.hidden = YES;
}

- (void)showThread {
    CGRect rect = postView.frame;
    threadView.hidden = NO;
    [UIView animateWithDuration:0.35 animations:^{
        postView.frame = CGRectMake(85, 0, 320, postView.frame.size.height);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.15 animations:^() {
            postView.frame = rect;
        } completion:^(BOOL finished) {
            if (finished) {
                threadView.hidden = YES;
                [self.delegate cell:self wantsTo:RCPostCellActionThread];
            }
        }];
    }];
}

- (void)showMiniOptions {
    [self prepareViewColors];
    [self prepareViewPosition];
    if (!isShowingMiniOptions && !self.isAnimating) {
        overlayView.hidden = NO;
        self.isAnimating = true;
        [UIView animateWithDuration:.35f animations:^{
            [postView setFrame:CGRectMake(-290, postView.frame.origin.y, postView.frame.size.width, postView.frame.size.height)];
            overlayView.frame = CGRectMake(0, overlayView.frame.origin.y, overlayView.frame.size.width, overlayView.frame.size.height);
        } completion:^(BOOL finished) {
            if (finished) {
                isShowingMiniOptions = TRUE;
                currentTouchPosition = CGPointZero;
                firstTouchPosition = CGPointZero;
                [self.delegate cell:self didMakeSwipeMotionWithDirection:RCDirectionLeftToRight];
                self.isAnimating = false;
            }
        }];
    }
    
}

- (void)prepareViewPosition {
    overlayView.frame = CGRectMake(overlayView.frame.size.width, overlayView.frame.origin.y, overlayView.frame.size.width, overlayView.frame.size.height);
}

- (void)prepareViewColors {
    if ([self respondsToSelector:@selector(setTintColor:)]) [self setTintColor:[UIApplication sharedApplication].delegate.window.tintColor];
    
    if ([self.post.user.userID isEqualToString:[[RCUserStorage sharedUserStorage] userObject].userID]) {
        button_repost.hidden = YES;
        button_star.hidden = YES;
    }
    if (self.post.isRepostedByCurrentUser == TRUE) [button_repost setImage:[button_repost.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    else [button_repost setImage:[button_repost.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    if (self.post.isStarredByCurrentUser == TRUE) [button_star setImage:[button_star.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    else [button_star setImage:[button_star.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
}

- (void)hideMiniOptions {
    if (isShowingMiniOptions && !self.isAnimating) {
        self.isAnimating = true;
        [UIView animateWithDuration:0.35f animations:^{
            [postView setFrame:CGRectMake(0, postView.frame.origin.y, postView.frame.size.width, postView.frame.size.height)];
            overlayView.frame = CGRectMake(overlayView.frame.size.width, overlayView.frame.origin.y, overlayView.frame.size.width, overlayView.frame.size.height);
            
        } completion:^(BOOL finished) {
            if (finished == true) {
                isShowingMiniOptions = FALSE;
                overlayView.hidden = YES;
                self.isAnimating = false;
            }
        }];
    }
}

- (BOOL)isShowingMiniOptions {
    return isShowingMiniOptions;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
