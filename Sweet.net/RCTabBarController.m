//
//  RCTabBarController.m
//  Sweet.net
//
//  Created by Rafael Costa on 11/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "RCTabBarController.h"
#import "AppDelegate.h"
#import "UIGestureRecognizer+Blocks.h"
@interface RCTabBarController ()

@end

@implementation RCTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (NSUInteger)supportedInterfaceOrientations {
    return [self.selectedViewController supportedInterfaceOrientations];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self.tabBar respondsToSelector:@selector(setTranslucent:)]) [self.tabBar setTranslucent:YES];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) self.edgesForExtendedLayout = UIRectEdgeAll;
    [self.tabBar setBarStyle:UIBarStyleBlack];
    //[self.tabBar setBarTintColor:UIColor.blackColor];
    //[self.tabBar setBarStyle:UIBarStyleBlack];
//    [self.tabBar setBackgroundImage:[UIImage imageNamed:@"tabBarTexture_2"]];
//    [self.tabBar setSelectionIndicatorImage:[UIImage imageNamed:@"new_tabBarSelectionImage"]];
    //[self.tabBar setSelectionIndicatorImage:[UIImage imageNamed:@"tabBarSelectionImage"]];
    //[self.tabBar setClipsToBounds:YES];
    
//    [self.tabBar.items[0] setFinishedSelectedImage:[UIImage imageNamed:@"new_speechbubble_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"new_speechbubble"]];
//    [self.tabBar.items[1] setFinishedSelectedImage:[UIImage imageNamed:@"new_mention_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"new_mention"]];
//    [self.tabBar.items[2] setFinishedSelectedImage:[UIImage imageNamed:@"new_messages_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"new_messages"]];
//    [self.tabBar.items[2] setFinishedSelectedImage:[UIImage imageNamed:@"new_global_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"new_global"]];
//    [self.tabBar.items[3] setFinishedSelectedImage:[UIImage imageNamed:@"new_user_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"new_user"]];
    
//    // DEBUG
//    if (self.tabBar.items.count == 5) {
//        //[self.tabBar.items[0] setFinishedSelectedImage:[UIImage imageNamed:@"new_speechbubble_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"new_speechbubble"]];
//        [self.tabBar.items[0] setFinishedSelectedImage:[UIImage imageNamed:@"newPXM_home_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"newPXM_home"]];
//        [self.tabBar.items[1] setFinishedSelectedImage:[UIImage imageNamed:@"new_mention_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"new_mention"]];
//        //[self.tabBar.items[2] setFinishedSelectedImage:[UIImage imageNamed:@"new_chats_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"new_chats"]];
//        [self.tabBar.items[2] setFinishedSelectedImage:[UIImage imageNamed:@"newPXM_chat_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"newPXM_chat"]];
//        [self.tabBar.items[3] setFinishedSelectedImage:[UIImage imageNamed:@"new_global_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"new_global"]];
//        //[self.tabBar.items[4] setFinishedSelectedImage:[UIImage imageNamed:@"new_user_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"new_user"]];
//        [self.tabBar.items[4] setFinishedSelectedImage:[UIImage imageNamed:@"newPXM_user_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"newPXM_user"]];
//    }
//    // END DEBUG
    
//    
//    for (UITabBarItem *item in self.tabBar.items) {
//        [item setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor colorWithRed:113.f/255.f green:120.f/255.f blue:128.f/255.f alpha:1.0f]} forState:UIControlStateNormal];
//        //[item setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor blackColor]} forState:UIControlStateSelected];
//        [item setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor colorWithRed:35.f/255.f green:147.f/255.f blue:238.f/255.f alpha:1.f]} forState:UIControlStateSelected];
//        //[item setTitle:@""];
//        //[item setImageInsets:UIEdgeInsetsMake(7.0, 0, -7.0, 0)];
//    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
