//
//  UIImageView.h
//  Sweet.net
//
//  Created by Rafael Costa on 1/28/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

@interface UIImage (Resize)
- (UIImage*)scaleToSize:(CGSize)size;
@end
