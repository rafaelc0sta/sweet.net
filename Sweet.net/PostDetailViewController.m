//
//  PostDetailViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 17/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "PostDetailViewController.h"


//#import "OHAttributedLabel.h"

#import "UserDetailViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "UIGestureRecognizer+Blocks.h"
#import "PostConversationDetailsViewController.h"
#import "UIButton+WebCache.h"
#import "WebsiteViewController.h"
#import "LocationMapViewController.h"
#import "AppDelegate.h"

@interface PostDetailViewController ()

@end

@implementation PostDetailViewController

- (IBAction)showLocation:(id)sender {
    LocationMapViewController *locMap = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"LocationMapViewController"];
    [locMap setLocation2D:attatched_geolocation];
    
    [self.navigationController pushViewController:locMap animated:YES];
}

- (IBAction)goToAuthorsProfile:(id)sender {
    
    UserDetailViewController *userDetailViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"UserDetailViewController2"];
    [userDetailViewController setUser:self.post.user];
    [self.navigationController pushViewController:userDetailViewController animated:YES];
}

- (IBAction)viewConversation:(id)sender {
    
    PostConversationDetailsViewController *pcdvc = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"PostConversationDetailsViewController"];
    [pcdvc setPost:self.post];
    [[self navigationController] pushViewController:pcdvc animated:YES];
}

- (IBAction)repost:(id)sender {
    
    NSString *string_repostButton = @"Repost";
    if (self.post.isRepostedByCurrentUser == TRUE) string_repostButton = @"Unrepost";
    
    actionSheet_repost = [[JLActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@[string_repostButton, @"Repost with comment"]];
    [actionSheet_repost setStyle:JLSTYLE_SUPERCLEAN];
    [actionSheet_repost setClickedButtonBlock:^(JLActionSheet *actionSheet, NSInteger clickedButtonIndex) {
        if (clickedButtonIndex == 2) {
            //Repost
            [self repostPost:self.post];
        } else if (clickedButtonIndex == 1) {
            [self repostPostWithQuote:self.post];
        }
    }];
    [actionSheet_repost showInView:self.view];
}
- (IBAction)reply:(id)sender {
    
    [self replyToPost:self.post];
}
- (IBAction)star:(id)sender {
    if (self.post.isStarredByCurrentUser == TRUE) {
        [[ANKClient sharedClient] unstarPost:self.post completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Unstarred";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                [button_star setImage:[button_star.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
                self.post.isStarredByCurrentUser = !self.post.isStarredByCurrentUser;
                [self layoutStarButton];
            }
        }];
    } else {
        [[ANKClient sharedClient] starPost:self.post completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Starred";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                [button_star setImage:[button_star.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                self.post.isStarredByCurrentUser = !self.post.isStarredByCurrentUser;
                [self layoutStarButton];
            }
        }];
    }
}

- (void)repostPost:(ANKPost *)post1 {
    if (post1.isRepostedByCurrentUser == TRUE) {
        [[ANKClient sharedClient] unrepostPost:post1 completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Unreposted";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                [button_repost setImage:[button_repost.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
                post1.isRepostedByCurrentUser = !post1.isRepostedByCurrentUser;
            }
        }];
    } else {
        [[ANKClient sharedClient] repostPost:post1 completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Reposted";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                [button_repost setImage:[button_repost.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                post1.isRepostedByCurrentUser = !post1.isRepostedByCurrentUser;
            }
        }];
    }
}

- (void)repostPostWithQuote:(ANKPost *)post {
    RCComposeViewController *cpVC = [[RCComposeViewController alloc]initWithNibName:@"RCComposeViewController" bundle:[NSBundle mainBundle]];
    
    if (post != nil) cpVC.navTitle = @"Quote";
    
    [cpVC setInitialText:[NSString stringWithFormat:@" >> @%@: %@", post.user.username, post.text]];
    [cpVC setPostToReply:post];
    
    [self presentViewController:cpVC animated:YES completion:^(void) {
        
    }];
}

- (void)replyToPost:(ANKPost *)post {
    RCComposeViewController *cpVC = [[RCComposeViewController alloc]initWithNibName:@"RCComposeViewController" bundle:[NSBundle mainBundle]];
    
    [cpVC setPostToReply:post];
    
    if (post != nil) cpVC.navTitle = @"Reply";
    
    [self presentViewController:cpVC animated:YES completion:^(void) {
        
    }];
}

- (void)deletePost {
    [[ANKClient sharedClient] deletePost:self.post completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
        if (error == nil) {
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
            [appWindow addSubview:HUD];
            HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
            HUD.mode = MBProgressHUDModeCustomView;
            HUD.labelText = @"Deleted";
            [HUD show:YES];
            [HUD hide:YES afterDelay:1.5];
            self.post.isDeleted = !self.post.isDeleted;
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
            [appWindow addSubview:HUD];
            HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
            HUD.mode = MBProgressHUDModeCustomView;
            HUD.labelText = @"Error deleting post";
            [HUD show:YES];
            [HUD hide:YES afterDelay:1.5];
        }
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [scrollView_label setScrollEnabled:FALSE];
    
    array_photos = [[NSMutableArray alloc] init];
    
    if ([self.post.user.userID isEqualToString:[[RCUserStorage sharedUserStorage] userObject].userID]) {
        //Hide repost and fav button.   xw
        button_repost.hidden = TRUE;
        button_star.hidden = TRUE;
        
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deletePost)];
        [self.navigationItem setRightBarButtonItem:barButton];
    }
    
    if (self.post.isRepostedByCurrentUser) {
        [button_repost setImage:[button_repost.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    }
    
    if (self.post.isStarredByCurrentUser) {
        [button_star setImage:[button_star.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    }
    
    button_conversation.hidden = TRUE;
    if (self.post.repliedToPostID != nil || self.post.repliesCount != 0) {
        button_conversation.hidden = FALSE;
    }
    
//    view_buttonContainer.layer.cornerRadius = 8.f;
//    view_buttonContainer.layer.borderColor = [UIColor colorWithRed:171.f/255.f green:171.f/255.f blue:171.f/255.f alpha:1.f].CGColor;
//    view_buttonContainer.layer.borderWidth = .35f;
    view_buttonContainer.layer.borderColor = [UIColor colorWithRed:171.f/255.f green:171.f/255.f blue:171.f/255.f alpha:1.f].CGColor;
    view_buttonContainer.layer.borderWidth = .35f;
    
//    view_postContainer.layer.cornerRadius = 8.f;
//    view_postContainer.layer.borderColor = [UIColor colorWithRed:171.f/255.f green:171.f/255.f blue:171.f/255.f alpha:1.f].CGColor;
    view_postContainer.layer.borderColor = [UIColor colorWithRed:171.f/255.f green:171.f/255.f blue:171.f/255.f alpha:1.f].CGColor;
    view_postContainer.layer.borderWidth = .35f;
    
    
    view_buttonContainer.backgroundColor = [UIColor whiteColor];
    view_postContainer.backgroundColor = [UIColor whiteColor];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.post.text];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Light" size:15.f] range:NSMakeRange(0, attributedString.string.length)];
    [label_text setText:attributedString];
    
    //[label_text setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:15.f]];
    //[label_text setAttributedText:[attributedString copy]];
    [label_text setDelegate:self];
    //[attributedString setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:15.f]];
    
    //[label_text setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:30.f]];
    
    [label_text setVerticalAlignment:TTTAttributedLabelVerticalAlignmentTop];
    
    [label_text setLinkAttributes:@{(__bridge NSString *)kCTForegroundColorAttributeName : defaultBlue, (__bridge NSString *)kCTUnderlineStyleAttributeName : [NSNumber numberWithBool:NO]}];
    
    [label_text setActiveLinkAttributes:@{(__bridge NSString *)kCTForegroundColorAttributeName : defaultBlue, (__bridge NSString *)kCTUnderlineStyleAttributeName : [NSNumber numberWithBool:YES]}];
    
    for (id object in self.post.entities.mentions) {
        [label_text addLinkToURL:[NSURL URLWithString:[NSString stringWithFormat:@"openUSER:%@", [[object username] stringByReplacingOccurrencesOfString:@"@" withString:@""]]] withRange:[self.post.text rangeOfString:[NSString stringWithFormat:@"@%@", [object username]]]];
        //[attributedString setLink:[NSURL URLWithString:[NSString stringWithFormat:@"openUSER:%@", [[object username] stringByReplacingOccurrencesOfString:@"@" withString:@""]]] range:[self.post.text rangeOfString:[NSString stringWithFormat:@"@%@", [object username]]]];
    }
    
    for (id object in self.post.entities.links) {
        //[attributedString setLink:[object URL] range:[object range]];
        [label_text addLinkToURL:[object URL] withRange:[object range]];
    }
    

    [button_userImage setTitle:@"" forState:UIControlStateNormal];
    [[button_userImage layer] setCornerRadius:10.f];
    [[button_userImage layer] setMasksToBounds:YES];
    
    label_source.text = [NSString stringWithFormat:@"via %@ ", self.post.source.name];
    label_realName.text = self.post.user.name;
    [button_username setTitle:[NSString stringWithFormat:@"@%@",self.post.user.username] forState:UIControlStateNormal];
    [button_userImage setContentMode:UIViewContentModeScaleToFill];
    [button_userImage setClipsToBounds:YES];
    
    NSString *finalURLString = [self.post.user.avatarImage.URL.absoluteString stringByAppendingString:@"?h=120&w=120"];
    NSURL *userURL = [NSURL URLWithString:finalURLString];
    
    [button_userImage setImageWithURL:userURL forState:UIControlStateNormal];
    [imageView_userImage setImageWithURL:userURL];
    button_location.hidden = TRUE;
    
    for (ANKAnnotation *annotation in self.post.annotations) {
        if ([[annotation.value objectForKey:@"type"] isEqualToString:@"photo"]) {
            [array_photos addObject:annotation];
        } else if ([[annotation type] isEqualToString:kANKCoreAnnotationGeolocation]) {
            attatched_geolocation.latitude = [[[annotation value] objectForKey:@"latitude"] floatValue];
            attatched_geolocation.longitude = [[[annotation value] objectForKey:@"longitude"] floatValue];
            button_location.hidden = FALSE;
        }
    }
    
    [button_location setImage:[button_location.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    
	// Do any additional setup after loading the view.
}

- (void)viewWillLayoutSubviews {
    [label_text setPreferredMaxLayoutWidth:0.];
}

- (void)layoutStarButton {
    if (button_conversation.isHidden == TRUE) {
        button_star.frame = CGRectMake(view_buttonContainer.bounds.size.width/2 - button_star.bounds.size.width/2, button_star.frame.origin.y, button_star.frame.size.width, button_star.frame.size.height);
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [view_buttonContainer layoutSubviews];
    
    [self layoutStarButton];
    
    scrollView_label.contentSize = [label_text sizeThatFits:CGSizeMake(label_text.bounds.size.width, 900.f)];
    scrollView_label.alwaysBounceHorizontal = NO;
    
    CGRect frame = label_text.frame;
    frame.size.height = scrollView_label.contentSize.height;
    label_text.frame = frame;
}

- (void)viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        
    }
    [super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    if (scrollView_label.contentSize.height <= scrollView_label.bounds.size.height) {
        [scrollView_label setScrollEnabled:FALSE];
    } else {
        [scrollView_label setScrollEnabled:TRUE];
        [scrollView_label flashScrollIndicators];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[[self.navigationController navigationBar] topItem] setTitle:@"Post Detail"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)attributedLabel:(TTTAttributedLabel *)attributedLabel didSelectLinkWithURL:(NSURL *)url {
    
    if ([url.absoluteString hasPrefix:@"openUSER:"]) {
        NSString *clickedUsername = [url.absoluteString stringByReplacingOccurrencesOfString:@"openUSER:" withString:@""];
        NSString *userIDToBeOpened = [[NSString alloc] init];
        for (ANKMentionEntity *user in self.post.entities.mentions) {
            if ([[user username] rangeOfString:clickedUsername].location != NSNotFound) {
                userIDToBeOpened = user.userID;
            }
        }
        
        UserDetailViewController *userDetailViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"UserDetailViewController"];
        [userDetailViewController setUser:nil];
        [userDetailViewController setUsernameID:userIDToBeOpened];
        [self.navigationController pushViewController:userDetailViewController animated:YES];
    } else if ([url.absoluteString rangeOfString:[NSString stringWithFormat:@"photos.app.net/%@/", self.post.postID]].location != NSNotFound && array_photos.count > 0) {
        
        AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        unichar uni = [url.absoluteString characterAtIndex:url.absoluteString.length - 1];
        
        NSUInteger photoNumber = [[NSString stringWithCharacters:&uni length:1] integerValue];
        
        NSURL *url = [NSURL URLWithString:[[(ANKAnnotation *)[array_photos objectAtIndex:photoNumber - 1] value] objectForKey:@"url"]];
        
        
        RCShowdownImageView *imageView_overlay = [[RCShowdownImageView alloc] initWithFrame:delegate.window.frame];
        [imageView_overlay setImageWithURL:url];
        [imageView_overlay showViewToView:delegate.window];
        
//        array_MWPhotos = [[NSMutableArray alloc] init];
//        
//        MWPhoto *photo = [MWPhoto photoWithURL:url];
//        [photo setCaption:self.post.text];
//        
//        [array_MWPhotos addObject:photo];
//        
//        MWPhotoBrowser *photoBrowser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//        
//        [photoBrowser setDisplayActionButton:NO];
//        
//        [self.navigationController pushViewController:photoBrowser animated:YES];
        
//        NSLog(@"link: %@", url.absoluteString);
//        [webSite setUrl:url];
//        [self.navigationController pushViewController:webSite animated:YES];
        
    } else {
        WebsiteViewController *webSite = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"WebsiteViewController"];
        [webSite setUrl:url];
        [self.navigationController pushViewController:webSite animated:YES];
    }
}


#pragma mark - ActionSheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        [self repostPost:self.post];
    } else if (buttonIndex == 1) {
        [self repostPostWithQuote:self.post];
    }
}

@end
