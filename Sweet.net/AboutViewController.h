//
//  AboutViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 20/04/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController  <UITableViewDataSource, UITableViewDelegate, UIBarPositioningDelegate> {
    IBOutlet UITableView *tableView;
    
    NSMutableArray *licenses;
    
    NSFileManager *fm;
}
- (IBAction)done:(id)sender;
@end
