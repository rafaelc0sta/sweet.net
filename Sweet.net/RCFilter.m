//
//  RCFilter.m
//  Sweet.net
//
//  Created by Rafael Costa on 1/17/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import "RCFilter.h"

@implementation RCFilter
- (BOOL)shouldRemovePost:(id)filteree {
    //OVERRIDE IN SUBCLASS.
    return false;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    //Encode the properties of the object
    [encoder encodeObject:self.query forKey:@"query"];
    [encoder encodeObject:self.ID forKey:@"ID"];
    [encoder encodeInteger:self.searchOption forKey:@"searchOption"];
}

-(id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self != nil)
    {
        //decode the properties
        self.query = [decoder decodeObjectForKey:@"query"];
        self.ID = [decoder decodeObjectForKey:@"ID"];
        self.searchOption = [decoder decodeIntegerForKey:@"searchOption"];
    }
    return self;
}
@end
