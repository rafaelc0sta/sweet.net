//
//  RCActionSheet.m
//  Sweet.net
//
//  Created by Rafael Costa on 23/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "RCActionSheet.h"

@implementation RCActionSheet

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithTitle:(NSString *)title cancelButtonTitle:(NSString *)cancel destructiveButtonTitle:(NSString *)destructive otherButtonTitles:(NSArray *)buttonTitles completionBlock:(DismissBlock)block {
    self  = [super initWithTitle:title delegate:[self self] cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    
    if (self) {
        for (NSString *title in buttonTitles) {
            [self addButtonWithTitle:title];
        }
        [self addButtonWithTitle:cancel];
        [self setCancelButtonIndex:[self numberOfButtons] - 1];
    }
    blockToExecute = block;
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    blockToExecute((int)buttonIndex);
}

@end
