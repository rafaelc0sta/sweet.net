//
//  RCFilterClient.m
//  Sweet.net
//
//  Created by Rafael Costa on 1/18/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import "RCFilterClient.h"

@implementation RCFilterClient
- (BOOL)shouldRemovePost:(id)filteree {
    ANKPost *post = (ANKPost *)filteree;
    
    if (self.searchOption == RCFilterCompareOptionEquals) {
        if ([post.source.name.lowercaseString isEqualToString:self.query.lowercaseString]) return true;
        return false;
    } else if (self.searchOption == RCFilterCompareOptionContains) {
        if ([post.source.name.lowercaseString rangeOfString:self.query.lowercaseString].location != NSNotFound) return true;
        return false;
    }
}
@end
