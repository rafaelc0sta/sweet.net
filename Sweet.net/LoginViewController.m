//
//  LoginViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 16/04/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "LoginViewController.h"
#import "ADNKit.h"
#import "RCKit.h"
#import "ADNLogin.h"
#import <QuartzCore/QuartzCore.h>
#import "PDKeychainBindings.h"
#import "AGMedallionView.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import <AFNetworking/AFNetworking.h>
#import "UIImageResize.h"


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)changeViews {
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        [self performSegueWithIdentifier:@"goToMainUI_6" sender:self];
        return;
    }
    
    if ([[RCPushStorage sharedPushStorage] deviceToken] != nil) {
        AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"https://rafaelc.com.br/push/devices/registerDevice.php"]];
        [client setParameterEncoding:AFJSONParameterEncoding];
        [client postPath:@"" parameters:@{@"username" : [[RCUserStorage sharedUserStorage] user], @"apiKey" : @"70daa50b2014", @"deviceToken" : [[RCPushStorage sharedPushStorage] deviceToken]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //Registered device.
            NSLog(@"SUCCESS");
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //Not registered.
            NSLog(@"Error: %@", error.description);
        }];
    }
    
    
    
    if (!self.payload) [self performSegueWithIdentifier:@"goToMainUI" sender:self];
    else {
        if ([[self.payload objectForKey:@"action"] isEqualToString:@"post"]) {
            RCComposeViewController *composeView = [[RCComposeViewController alloc] initWithNibName:@"RCComposeViewController" bundle:nil];
            [composeView setInitialText:[self.payload objectForKey:@"initialText"]];
            [self presentViewController:composeView animated:YES completion:^{
                self.payload = nil;
            }];
        }
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if ([[UIApplication sharedApplication] delegate].window.frame.size.height < 568.f) {
        //4S:
        [imageView setImage:[UIImage imageNamed:@"default_sw3_small.png"]];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ANKGeneralParameters *params = [ANKGeneralParameters new];
    [params setIncludeAnnotations:YES];
    [params setIncludePostAnnotations:YES];
    [params setIncludeMarker:YES];
    [params setIncludeRecentMessage:YES];
    
    [[ANKClient sharedClient] setGeneralParameters:params];
    
    [[RCUserStorage sharedUserStorage] setAccountStore:[[ACAccountStore alloc] init]];
    
    [[RCFilterManager sharedFilterManager] loadFilters];
	// Do any additional setup after loading the view.
}

- (void)saveDictionaryToDisk:(NSDictionary *)dict fileName:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:fileName];
    [dict writeToFile:plistPath atomically:YES];
}

- (void)deleteDictionaryWithName:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:fileName];
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:plistPath error:&error];
}

- (NSDictionary *)loadDictionaryFromDiskWithFileName:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:fileName];
    
    // check to see if Data.plist exists in documents
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        return NSDictionary.new;
    }
    
    NSDictionary *dictionary = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    return dictionary;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self updateUserData];
}


- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)updateUserData {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isFirstBoot"] == nil) {
        [[PDKeychainBindings sharedKeychainBindings] removeObjectForKey:@"sweetdotnet:accessToken"];
        [[NSUserDefaults standardUserDefaults] setObject:[NSString new] forKey:@"isFirstBoot"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        EAIntroPage *page1 = [EAIntroPage page];
        page1.title = @"Welcome to Sweet.net 3.0";
        page1.desc = @"Somethings changed in this version! Let's get started?";
        page1.bgImage = [UIImage imageNamed:@"bg_intro"];
        page1.titleImage = [UIImage imageNamed:@"30_logo"];
        
        EAIntroPage *page2 = [EAIntroPage page];
        page2.bgImage = [UIImage imageNamed:@"bg_intro"];
        page2.title = @"Menu Button";
        page2.desc = @"Now the bottom bar doesn't take space that is meant for posts! The new \"Menu Button\" is small and disappears when no longer needed!";
        page2.titleImage = [[UIImage imageNamed:@"menu_demo"] scaleToSize:CGSizeMake(240, 240)];
        
        EAIntroPage *page3 = [EAIntroPage page];
        page3.bgImage = [UIImage imageNamed:@"bg_intro"];
        page3.title = @"Global Feed";
        page3.desc = @"The Global Feed is now on the \"My Profile\" tab. Check it out.";
        page3.titleImage = [UIImage imageNamed:@"global_image"];
        
        EAIntroPage *page4 = [EAIntroPage page];
        page4.bgImage = [UIImage imageNamed:@"bg_intro"];
        page4.title = @"Last, but not least...";
        page4.desc = @"Make the most of Sweet.net 3.0! It's a big update so we hope you enjoy it. Give some feedback (@rafaelcosta) and rate if you like. =)";
        page4.titleImage = [UIImage imageNamed:@"welcome"];
        
        EAIntroView *view = [[EAIntroView alloc] initWithFrame:self.view.frame];
        [view setPages:@[page1, page2, page3, page4]];
        [view setDelegate:self];
        [view setShowSkipButtonOnlyOnLastPage:YES];
        
        [self.view addSubview:view];
        return;
    }
    
    if ((id)[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"sweetdotnet:accessToken"] == nil) {
        [self presentLogin];
        
    } else if ((id)[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"sweetdotnet:accessToken"] != nil) {
        if ([[self loadDictionaryFromDiskWithFileName:@"user.plist"] isEqualToDictionary:[NSDictionary new]]) {
            NSLog(@"No user plist :(");
            KTLLoginViewController *loginViewController = [[KTLLoginViewController alloc] init];
            [loginViewController setHasLoggedFromApp:YES];
            UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:loginViewController];
            [loginViewController setDelegate:self];
            [self presentViewController:navC animated:YES completion:^{
                
            }];
            return;
        }
        NSString *accessToken = [[PDKeychainBindings sharedKeychainBindings] objectForKey:@"sweetdotnet:accessToken"];
        
        [[ANKClient sharedClient] setAccessToken:accessToken];
        
        ANKUser *user = [[ANKUser alloc] initWithJSONDictionary:[self loadDictionaryFromDiskWithFileName:@"user.plist"]];
        
        [[RCUserStorage sharedUserStorage] setUser:user.username];
        [[RCUserStorage sharedUserStorage] setUserObject:user];
        
        [self changeViews];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loginController:(KTLLoginViewController *)loginController didAddAccountWithUserDictionary:(NSDictionary *)userDict {
    
    ANKUser *user = [[ANKUser alloc] initWithJSONDictionary:userDict];
    
    [[RCUserStorage sharedUserStorage] setUserObject:user];
    [[RCUserStorage sharedUserStorage] setUser:user.username];
    [self saveDictionaryToDisk:userDict fileName:@"user.plist"];
    
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"https://rafaelc.com.br/push/api/putUser.php"]];
    NSMutableDictionary *jsonParameters = [NSMutableDictionary new];
    [jsonParameters setObject:user.username forKey:@"username"];
    [jsonParameters setObject:@"70daa50b2014" forKey:@"apiKey"];
    [client setParameterEncoding:AFJSONParameterEncoding];
    [client postPath:@"" parameters:jsonParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [loginController dismissViewControllerAnimated:YES completion:^{
            [self changeViews];
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [loginController dismissViewControllerAnimated:YES completion:^{
            [self changeViews];
        }];
    }];
    
    [[PDKeychainBindings sharedKeychainBindings] setString:[ANKClient sharedClient].accessToken forKey:@"sweetdotnet:accessToken"];
}

- (void)presentLogin {
    KTLLoginViewController *loginViewController = [[KTLLoginViewController alloc] init];
    UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    [loginViewController setDelegate:self];
    [self presentViewController:navC animated:YES completion:^{
        
    }];
}

- (void)introDidFinish:(EAIntroView *)introView {
    [self presentLogin];
}

@end
