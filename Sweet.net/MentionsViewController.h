//
//  MentionsViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 31/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCKit.h"
#import <Security/Security.h>
#import <CommonCrypto/CommonCrypto.h>
#import "ADNKit.h"
#import "JLActionSheet.h"

@interface MentionsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, RCPostCellDelegate> {
    JLActionSheet *actionSheet_repost;
    
    IBOutlet UITableView *tableView_posts;
    
    IBOutlet UINavigationBar *navBar;
    NSMutableArray *array_posts;
    
    UIRefreshControl *refreshControl;
    
    NSString *lastPostID;
}
- (IBAction)openSearch:(id)sender;

@end
