//
//  PostDetailViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 17/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ADNKit.h"
#import "TimelineViewController.h"
//#import "OHAttributedLabel.h"
#import "UserDetailViewController.h"
#import "RCKit.h"
#import "TTTAttributedLabel.h"
#import "JLActionSheet.h"

@interface PostDetailViewController : UIViewController <TTTAttributedLabelDelegate, UIActionSheetDelegate> {
    
    JLActionSheet *actionSheet_repost;
    
    IBOutlet UIView *view_postContainer;
    
    IBOutlet UILabel *label_realName;
    IBOutlet UIButton *button_username;
    IBOutlet UIButton *button_userImage;
    IBOutlet RCMaskedImageView *imageView_userImage;
    
    IBOutlet UILabel *label_source;
    
    IBOutlet UIButton *containerButton1;
    IBOutlet UIButton *containerButton2;
    
    IBOutlet UIScrollView *scrollView_label;
    IBOutlet TTTAttributedLabel *label_text;
    
    IBOutlet UINavigationBar *navBar;
    
    UIView *maskView;
    UIView *maskView_navController;
    
    
    IBOutlet UIView *view_buttonContainer;
    IBOutlet UIButton *button_star;
    IBOutlet UIButton *button_repost;
    IBOutlet UIButton *button_conversation;
    
    IBOutlet UIButton *button_location;
    
    NSMutableArray *array_photos;
    NSMutableArray *array_MWPhotos;
    
    CLLocationCoordinate2D attatched_geolocation;
}
@property (nonatomic) NSIndexPath *indexPath;
- (IBAction)repost:(id)sender;
- (IBAction)reply:(id)sender;
- (IBAction)star:(id)sender;

- (IBAction)viewConversation:(id)sender;
- (IBAction)showLocation:(id)sender;

- (IBAction)goToAuthorsProfile:(id)sender;

@property (nonatomic) ANKPost *post;
@end
