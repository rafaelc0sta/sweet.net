//
//  RCInteractionCell.m
//  Sweet.net
//
//  Created by Rafael Costa on 11/07/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "RCInteractionCell.h"

@implementation RCInteractionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // The touch began
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    // The touch moved
    //[super touchesMoved:touches withEvent:event];
    //[self setSelected:NO animated:NO];
    [(UITableView *)self.superview deselectRowAtIndexPath:self.indexPath animated:YES];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    // The touch ended
    [super touchesEnded:touches withEvent:event];
    UITableView *superTableView = (UITableView *)self.superview;
    [[superTableView delegate] tableView:superTableView didSelectRowAtIndexPath:self.indexPath];
    //if (self.isSelected)
    [(UITableView *)self.superview setScrollEnabled:YES];
}

@end
