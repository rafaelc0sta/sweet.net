//
//  PostConversationDetailsViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 23/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "PostConversationDetailsViewController.h"
#import "ADNKit.h"
#import "SVPullToRefresh.h"
#import "PostDetailViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"

#import <QuartzCore/QuartzCore.h>

@interface PostConversationDetailsViewController ()

@end

@implementation PostConversationDetailsViewController

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	self.navigationController.navigationBar.topItem.title = @"Thread";
}

- (void)repostWithQuote:(ANKPost *)post {
    RCComposeViewController *cpVC = [[RCComposeViewController alloc]initWithNibName:@"RCComposeViewController" bundle:[NSBundle mainBundle]];
    
    if (post != nil) cpVC.navTitle = @"Quote";
    
    [cpVC setInitialText:[NSString stringWithFormat:@" >> @%@: %@", post.user.username, post.text]];
    [cpVC setPostToReply:post];
    
    [self presentViewController:cpVC animated:YES completion:^{
        
    }];
}


- (void)replyToPost:(ANKPost *)post {
    RCComposeViewController *cpVC = [[RCComposeViewController alloc]initWithNibName:@"RCComposeViewController" bundle:[NSBundle mainBundle]];
    
    [cpVC setPostToReply:post];
    
    if (post != nil) cpVC.navTitle = @"Reply";
    
    [self presentViewController:cpVC animated:YES completion:^{
        
    }];
}

- (void)starPost:(ANKPost *)post1 {
    if (post1.isStarredByCurrentUser == TRUE) {
        [[ANKClient sharedClient] unstarPost:post1 completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Unstarred";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];                int i;
                for (i = 0; i <= [array_posts count]; i++) {
                    RCPostCell *postCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    if ([postCell.post.postID isEqualToString:post1.postID]) {
                        [postCell setStarredButtonOn:FALSE];
                        postCell.post.isStarredByCurrentUser = !postCell.post.isStarredByCurrentUser;
                    }
                }
            }
        }];
    } else {
        [[ANKClient sharedClient] starPost:post1 completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Starred";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                int i;
                for (i = 0; i <= [array_posts count]; i++) {
                    RCPostCell *postCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    if ([postCell.post.postID isEqualToString:post1.postID]) {
                        [postCell setStarredButtonOn:TRUE];
                        postCell.post.isStarredByCurrentUser = !postCell.post.isStarredByCurrentUser;
                    }
                }
            }
        }];
    }
}

- (void)repostPost:(ANKPost *)post1 {
    if (post1.isRepostedByCurrentUser == TRUE) {
        [[ANKClient sharedClient] unrepostPost:post1 completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Unreposted";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                int i;
                for (i = 0; i <= [array_posts count]; i++) {
                    RCPostCell *postCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    if ([postCell.post.postID isEqualToString:post1.postID]) {
                        [postCell setRepostedButtonOn:FALSE];
                        postCell.post.isRepostedByCurrentUser = !postCell.post.isRepostedByCurrentUser;
                    }
                }
            }
        }];
    } else {
        [[ANKClient sharedClient] repostPost:post1 completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Reposted";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                int i;
                for (i = 0; i <= [array_posts count]; i++) {
                    RCPostCell *postCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    if ([postCell.post.postID isEqualToString:post1.postID]) {
                        [postCell setRepostedButtonOn:TRUE];
                        postCell.post.isRepostedByCurrentUser = !postCell.post.isRepostedByCurrentUser;
                    }
                }
            }
        }];
    }
    
}

- (void)abortChild:(UIViewController *)controller {
    [UIView animateWithDuration:0.25F delay:.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [maskView setAlpha:.0f];
        [maskView_navController setAlpha:.0f];
        controller.view.frame = CGRectMake(320/2 - controller.view.frame.size.width/2, -controller.view.frame.size.height, controller.view.frame.size.width, controller.view.frame.size.height);
    } completion:^(BOOL finished) {
        if (finished) {
            [maskView removeFromSuperview];
            [maskView_navController removeFromSuperview];
            [controller.view removeFromSuperview];
            [controller removeFromParentViewController];
        }
    }];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar.topItem setTitle:self.title];
    
    [tableView_posts registerNib:[UINib nibWithNibName:@"RCPostCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    array_posts = [[NSMutableArray alloc]init];
    
    [self reloadPosts];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadPosts) forControlEvents:UIControlEventValueChanged];
    [tableView_posts addSubview:refreshControl];
    
    
	// Do any additional setup after loading the view.
}

- (void)hideOptions:(NSNotification *)note {
    int nowIndex = 0;
    for (nowIndex = 0; nowIndex <= array_posts.count; nowIndex++) {
        RCPostCell *cell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:nowIndex inSection:0]];
        if (cell != [[note userInfo] objectForKey:@"cell"]) [cell hideMiniOptions];
    }
}

- (void)reloadPosts {
    [[ANKClient sharedClient] fetchRepliesToPost:self.post completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
        if (error == nil) {
            NSMutableArray *posts = responseObject;
            
            NSMutableArray *nilPosts = @[].mutableCopy;
            for (ANKPost *post in posts) {
                if (post == nil || post.text == nil || post.user == nil) {
                    [nilPosts addObject:post];
                }
            }
            [posts removeObjectsInArray:nilPosts.copy];
            
            
            array_posts = [posts mutableCopy];
            [tableView_posts reloadData];
            [refreshControl endRefreshing];
            int i = 0;
            for (i = 0; i < array_posts.count; i++) {
                if ([[[array_posts objectAtIndex:i] postID] isEqualToString:self.post.postID]) {
                    [tableView_posts scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
                    RCPostCell *cell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    [cell setContentViewBackgorundColor:[UIColor colorWithRed:233.f/255.f green:233.f/255.f blue:233.f/255.f alpha:1.0f]];
                }
            }
        } else {
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
            [appWindow addSubview:HUD];
            HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
            HUD.mode = MBProgressHUDModeCustomView;
            HUD.labelText = @"Error loading conversation posts";
            [HUD show:YES];
            [HUD hide:YES afterDelay:1.5];
            [self.navigationController popViewControllerAnimated:YES];
            [refreshControl endRefreshing];
            [tableView_posts.infiniteScrollingView stopAnimating];
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [refreshControl endRefreshing];
    
    for (RCComposeViewController *VC in [self childViewControllers]) {
        [VC.view removeFromSuperview];
        [VC removeFromParentViewController];
    }
    for (UIView *view in [self.view subviews]) {
        if (view.backgroundColor == [UIColor blackColor] && view.alpha == .7f) [view removeFromSuperview];
    }
    for (UIView *view in [self.navigationController.view subviews]) {
        if (view.backgroundColor == [UIColor blackColor] && view.alpha == .7f) [view removeFromSuperview];
    }
    
    if (array_posts.count > 0) {
        CGPoint offset = tableView_posts.contentOffset;
        NSMutableArray *array_itemsToRemove = @[].mutableCopy;
        for (ANKPost *post in array_posts) {
            if (post.isDeleted) [array_itemsToRemove addObject:post];
        }
        [array_posts removeObjectsInArray:array_itemsToRemove.copy];
        [tableView_posts reloadData];
        [tableView_posts setContentOffset:offset];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        
    }
    int nowIndex = 0;
    for (nowIndex = 0; nowIndex <= array_posts.count; nowIndex = nowIndex + 1) {
        RCPostCell *currentCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:nowIndex inSection:0]];
        [currentCell hideMiniOptions];
    }
}

- (void)cell:(RCPostCell *)cell wantsTo:(RCPostCellAction)action {
    if (action == RCPostCellActionReply) {
        [self replyToPost:cell.post];
    } else if (action == RCPostCellActionRepost) {
        NSString *stringRepostButton = @"Repost";
        if ([cell.post isRepostedByCurrentUser]) stringRepostButton = @"Unrepost";
        //actionSheet_repost = [[RCActionSheet alloc] initWithTitle:@"Repost options:" post:cell.post delegate:self otherButtonTitles:@[stringRepostButton, @"Repost with comment", @"Cancel"]];
        actionSheet_repost = [[JLActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@[stringRepostButton, @"Repost with comment"]];
        [actionSheet_repost setStyle:JLSTYLE_SUPERCLEAN];
        [actionSheet_repost setClickedButtonBlock:^(JLActionSheet *actionSheet, NSInteger clickedButtonIndex) {
            if (clickedButtonIndex == 2) {
                //Repost
                [self repostPost:cell.post];
            } else if (clickedButtonIndex == 1) {
                [self repostWithQuote:cell.post];
            }
        }];
        [actionSheet_repost showInView:self.view];
    } else if (action == RCPostCellActionStar) {
        [self starPost:cell.post];
    }
}

- (void)cell:(RCPostCell *)cell didMakeSwipeMotionWithDirection:(RCDirection)direction {
    if (direction == RCDirectionLeftToRight) {
        int nowIndex = 0;
        for (nowIndex = 0; nowIndex <= array_posts.count; nowIndex = nowIndex + 1) {
            RCPostCell *currentCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:nowIndex inSection:0]];
            if (currentCell != cell) {
                if (currentCell.isShowingMiniOptions) [currentCell hideMiniOptions];
            }
        }
    }
    
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [array_posts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"Cell";
    RCPostCell *cell = (RCPostCell *)[tableView_posts dequeueReusableCellWithIdentifier:identifier];
    [cell setMiniOptionsEnabled:false];
    if (cell == nil) cell = [[RCPostCell alloc] init];
    cell.indexPath = indexPath;
    cell.delegate = self;
    
    ANKPost *currentPost = (ANKPost *)[array_posts objectAtIndex:indexPath.row];
    ANKPost *originalPost = currentPost;
    if ([currentPost repostedPost] != nil) currentPost = [currentPost repostedPost];
    
    cell.label_username.text = [NSString stringWithFormat:@"@%@", currentPost.user.username];
    
    
//    cell.label_post.text = currentPost.text;
//    if (currentPost == [originalPost repostedPost]) {
//        cell.label_post.text = [cell.label_post.text stringByAppendingString:[NSString stringWithFormat:@"\n\nReposted by @%@", originalPost.user.username]];
//    }
    
#pragma mark - Start of colored stuff
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:currentPost.text attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Light" size:15.f], NSForegroundColorAttributeName : [UIColor blackColor]}];
    
    
    
    for (id object in currentPost.entities.mentions) {
        [string addAttributes:@{NSForegroundColorAttributeName : defaultBlue} range:[currentPost.text rangeOfString:[NSString stringWithFormat:@"@%@", [object username]]]];
    }
    
    //Disable for links = improved reading...
    //    for (id object in currentPost.entities.links) {
    //        [string addAttributes:@{NSForegroundColorAttributeName : defaultBlue} range:[object range]];
    //    }
    
    [cell.label_post setAttributedText:string];
    
    
    
    
    
    if (currentPost == [originalPost repostedPost]) {
        NSMutableAttributedString *attr1 = cell.label_post.attributedText.mutableCopy;
        NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n\nReposted by @%@", originalPost.user.username]];
        [attr1 appendAttributedString:attr2];
        cell.label_post.attributedText = attr1;
    }
#pragma mark - End of colored stuff.
    
    
    [cell setContentViewBackgorundColor:[UIColor whiteColor]];
    if ([currentPost.postID isEqualToString:self.post.postID]) [cell setContentViewBackgorundColor:[UIColor colorWithRed:233.f/255.f green:233.f/255.f blue:233.f/255.f alpha:1.0f]];
    cell.imageView_userImage.image = [UIImage imageNamed:@"placeholder"];
    
    NSDate *now = [NSDate date];
    
    NSTimeInterval timeInterval = [now timeIntervalSinceDate:currentPost.createdAt];
    NSUInteger minutes = timeInterval/60;
    NSUInteger seconds = timeInterval/1;
    NSUInteger hours = timeInterval/3600;
    NSString *finalTime = [NSString stringWithFormat:@"%i min(s) ", minutes];
    if (timeInterval <= 60) {
        finalTime = [NSString stringWithFormat:@"%i sec(s) ", seconds];
    }else if (timeInterval >= 3600) {
        finalTime = [NSString stringWithFormat:@"%i hour(s) ", hours];
    }
    
    cell.label_longAgo.text = finalTime;
    
    cell.post = currentPost;
    
    NSString *finalURLString = [currentPost.user.avatarImage.URL.absoluteString stringByAppendingString:@"?h=120&w=120"];
    NSURL *userURL = [NSURL URLWithString:finalURLString];
    
    [cell.imageView_userImage setImageWithURL:userURL placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    for (UITableViewCell *cell in tableView_posts.visibleCells) {
        if ([cell.class isSubclassOfClass:[UITableViewCell class]]) {
            [(RCPostCell *)cell hideMiniOptions];
        }
    }
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    ANKPost *currentPost = (ANKPost *)[array_posts objectAtIndex:indexPath.row];
    ANKPost *orignialPost = currentPost;
    if ([currentPost repostedPost] != nil) currentPost = [currentPost repostedPost];
    NSString *cellText =  [currentPost text];
    if (orignialPost != currentPost) cellText = [cellText stringByAppendingString:[NSString stringWithFormat:@"\n\nReposted by @%@", orignialPost.user.username]];
    UIFont *cellFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
    CGSize constraintSize = CGSizeMake(231.f, 5000.f);
    CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    
    return labelSize.height + 55.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PostDetailViewController *postDetailViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"PostDetailViewController"];
    
    [postDetailViewController setPost:[array_posts objectAtIndex:indexPath.row]];
    if ([[array_posts objectAtIndex:indexPath.row] repostedPost]) {
        [postDetailViewController setPost:[[array_posts objectAtIndex:indexPath.row] repostedPost]];
    }
    [postDetailViewController setIndexPath:indexPath];
    [[self navigationController] pushViewController:postDetailViewController animated:YES];
    
}


#pragma mark - Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        //Repost
        [self repostPost:[(RCActionSheet *)actionSheet post]];
    } else if (buttonIndex == 1) {
        [self repostWithQuote:[(RCActionSheet *)actionSheet post]];
    }
}

@end
