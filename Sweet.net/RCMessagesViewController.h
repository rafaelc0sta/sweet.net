//
//  RCMessagesViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 03/05/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "JSMessagesViewController.h"
#import "ADNKit.h"

@interface RCMessagesViewController : JSMessagesViewController <JSMessagesViewDataSource, JSMessagesViewDelegate>
@property (nonatomic) NSMutableArray *messages;
@property (nonatomic) ANKChannel *thisChannel;
@property (nonatomic) NSString *destinationUserID;
@property (nonatomic) NSString *titleToShow;
@end
