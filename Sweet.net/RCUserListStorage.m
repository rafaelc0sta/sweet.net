//
//  RCUserListStorage.m
//  SweetTweet6
//
//  Created by Rafael Costa on 01/12/12.
//  Copyright (c) 2012 Rafael Costa. All rights reserved.
//

#import "RCUserListStorage.h"
#import "NSMutableArray+ContainsString.h"

#define RCNormalList_KEY  @"userDB7"
//#define RCConversationList_KEY @"conversationUserDB"

@implementation RCUserListStorage


+ (RCUserListStorage *)sharedInstance {
    static RCUserListStorage *userStorage;
    @synchronized(self) {
        if (!userStorage) userStorage = [[self alloc] init];
        return userStorage;
    }
}

- (NSMutableArray *)userList {
    NSMutableArray *array;
    array = [[NSUserDefaults standardUserDefaults] objectForKey:RCNormalList_KEY];
    if(!array) {
        array = [[NSMutableArray alloc] init];
    } else {
        array = [[NSMutableArray alloc] initWithArray:array];
    }
    return array;
}

- (void)saveUser:(NSString *)user {
    NSString *listKey = RCNormalList_KEY;
    
    NSMutableArray *array;
    array = [[NSUserDefaults standardUserDefaults] objectForKey:listKey];
    if(!array) {
        array = [[NSMutableArray alloc] init];
    } else {
        array = [[NSMutableArray alloc] initWithArray:array];
    }
    if (![array containsString:user]) {
        [array addObject:user];
    }
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:listKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)clearUserDB {
    NSMutableArray *array;
    array = [[NSUserDefaults standardUserDefaults] objectForKey:RCNormalList_KEY];
    if(!array) {
        array = [[NSMutableArray alloc] init];
    } else {
        array = [[NSMutableArray alloc] initWithArray:array];
    }
    [array removeAllObjects];
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:RCNormalList_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
