//
//  RCMessagesViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 03/05/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "RCMessagesViewController.h"
#import "JSMessagesViewController.h"
#import "RCKit.h"

@interface RCMessagesViewController ()

@end

@implementation RCMessagesViewController

#pragma mark - View lifecycle

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.delegate = self;
    self.dataSource = self;
    
    [self setBackgroundColor:[UIColor whiteColor]];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 30, 0)];
    UIBarButtonItem *barButtonItemCompose = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadMessages)];
    self.navigationItem.rightBarButtonItem = barButtonItemCompose;
    if (self.thisChannel == nil) {
        // DO NOTHING!
    } else {
        self.title = self.titleToShow;
        
        [NSTimer scheduledTimerWithTimeInterval:20.f target:self selector:@selector(loadMessages) userInfo:nil repeats:YES];
        
        [self loadMessages];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        
    }
    [super viewWillDisappear:animated];
}

- (void)loadMessages {
    ANKPaginationSettings *paginationSettings = [ANKPaginationSettings settingsWithCount:200];
    
    [[[ANKClient sharedClient] clientWithPagination:paginationSettings] fetchMessagesInChannel:self.thisChannel completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
        if (error == nil) {
            if (self.messages == nil) {
                self.messages = [[[responseObject reverseObjectEnumerator] allObjects] mutableCopy];
                
                [self.tableView reloadData];
                [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.messages.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                
                
                ANKStreamMarker *streamMarker = self.thisChannel.marker;
                [streamMarker setTopPostID:[self.messages[self.messages.count - 1] messageID]];
                
                //[streamMarker setStreamName:self.thisChannel.marker.streamName];
                
                [[ANKClient sharedClient] updateStreamMarker:streamMarker completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
                    if (error == nil) {
                        NSLog(@"Success Updating Marker");
                    } else {
                        NSLog(@"Error: %@", error.description);
                    }
                }];
            }
            else {
                NSMutableArray *array_response = responseObject;
                NSMutableArray *array_newItems = [[NSMutableArray alloc] init];
                NSMutableArray *array_itemsToBeRemoved = [[NSMutableArray alloc] init];
                for (ANKMessage *message in self.messages) {
                    [array_itemsToBeRemoved addObject:message.messageID];
                }
                
                for (ANKMessage *message1 in array_response) {
                    if (![array_itemsToBeRemoved containsString:message1.messageID]) [array_newItems addObject:message1];
                }
                
                array_newItems = [[[array_newItems reverseObjectEnumerator] allObjects] mutableCopy];
                
                for (ANKMessage *newMessage in array_newItems) {
                    if (![newMessage.user isEqual:[[RCUserStorage sharedUserStorage] userObject]]) {
                        [JSMessageSoundEffect playMessageReceivedSound];
                    }
                }
                self.messages = [[self.messages arrayByAddingObjectsFromArray:array_newItems] mutableCopy];
                
                [self.tableView reloadData];
                [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.messages.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                
            }
            
        }
    }];
}

- (void)configureCell:(JSBubbleMessageCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if([cell messageType] == JSBubbleMessageTypeOutgoing) {
        cell.bubbleView.textView.textColor = [UIColor whiteColor];
        
        if([cell.bubbleView.textView respondsToSelector:@selector(linkTextAttributes)]) {
            NSMutableDictionary *attrs = [cell.bubbleView.textView.linkTextAttributes mutableCopy];
            [attrs setValue:[UIColor blueColor] forKey:NSForegroundColorAttributeName];
            
            cell.bubbleView.textView.linkTextAttributes = attrs;
        }
    }
    
    if(cell.timestampLabel) {
        cell.timestampLabel.textColor = [UIColor lightGrayColor];
        cell.timestampLabel.shadowOffset = CGSizeZero;
    }
    
    if(cell.subtitleLabel) {
        cell.subtitleLabel.textColor = [UIColor lightGrayColor];
    }
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.messages.count;
}


- (JSMessagesViewTimestampPolicy)timestampPolicy {
    return JSMessagesViewTimestampPolicyEveryFive;
}

#pragma mark - Messages view delegate

- (JSBubbleMessageType)messageTypeForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[[(ANKMessage *)[self.messages objectAtIndex:indexPath.row] user] username] isEqualToString:[[RCUserStorage sharedUserStorage] user]]) {
        // Post by me
        return JSBubbleMessageTypeOutgoing;
    } else {
        return JSBubbleMessageTypeIncoming;
    }
}

- (JSMessagesViewAvatarPolicy)avatarPolicy {
    return JSMessagesViewAvatarPolicyNone;
}

- (JSMessagesViewSubtitlePolicy)subtitlePolicy {
    return JSMessagesViewSubtitlePolicyNone;
}



- (UIImageView *)bubbleImageViewWithType:(JSBubbleMessageType)type forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (type == JSBubbleMessageTypeIncoming) {
        return [JSBubbleImageViewFactory bubbleImageViewForType:type color:[UIColor js_bubbleLightGrayColor]];
    } else {
        return [JSBubbleImageViewFactory bubbleImageViewForType:type color:[UIColor js_bubbleBlueColor]];
    }
}

- (JSMessageInputViewStyle)inputViewStyle
{
    return JSMessageInputViewStyleFlat;
}

- (void)didSendText:(NSString *)text {
    ANKMessage *message = [[ANKMessage alloc] init];
    [message setText:text];
    
    NSString *channelID = @"pm";
    if (self.thisChannel != nil) channelID = self.thisChannel.channelID;
    else [message setDestinationUserIDs:@[self.destinationUserID]];
    [[ANKClient sharedClient] createMessage:message inChannelWithID:channelID completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
        if (error == nil) {
            ANKMessage *message = responseObject;
            if (self.thisChannel == nil) {
                [[ANKClient sharedClient] fetchChannelWithID:message.channelID completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
                    self.thisChannel = responseObject;
                    [self loadMessages];
                }];
            }
            [self.messages addObject:responseObject];
            [JSMessageSoundEffect playMessageSentSound];
            [self loadMessages];
            NSLog(@"Sucesso!");
        } else {
            NSLog(@"error: %@", error.description);
        }
    }];
    [self finishSend];
}

#pragma mark - Messages view data source
- (NSString *)textForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [(ANKMessage *)[self.messages objectAtIndex:indexPath.row] text];
}

- (NSDate *)timestampForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [(ANKMessage *)[self.messages objectAtIndex:indexPath.row] createdAt];
}

@end
