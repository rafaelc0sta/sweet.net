//
//  SNPopUpView.h
//  Sweet.net
//
//  Created by Rafael Costa on 12/27/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXBlurView.h"

@class SNPopUpView;

@protocol SNPopUpViewDelegate <NSObject>

- (void)popUpView:(SNPopUpView *)view didSelectItemAtIndex:(NSInteger)index;

@end

@interface SNPopUpView : FXBlurView {
    NSMutableArray *views;
    UIColor *greenishBG;
}
- (id)initWithFrame:(CGRect)frame withTitles:(NSArray *)titles;
- (void)setSelectedIndex:(int)selectedIndex;
@property (nonatomic) NSObject <SNPopUpViewDelegate> *delegate;
@end
