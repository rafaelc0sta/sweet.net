//
//  UserViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 16/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "UserViewController.h"
#import "ADNKit.h"
#import <QuartzCore/QuartzCore.h>
#import "SDWebImage/UIImageView+WebCache.h"
#import <CoreText/CoreText.h>
#import <Foundation/NSAttributedString.h>
#import "PDKeychainBindings.h"
#import "PostsOrStarsViewController.h"
#import "FollowersOrFollowingViewController.h"
#import "AGMedallionView.h"
#import "MBProgressHUD.h"
#import "UIGestureRecognizer+Blocks.h"
#import "AppDelegate.h"
#import "EditProfileViewController.h"

@interface UserViewController ()

@end

@implementation UserViewController

- (IBAction)showSettings:(id)sender {
//    scrollView.contentOffset = CGPointZero;
//    [scrollView setContentInset:UIEdgeInsetsMake(65, 0, 0, 0)];
//    [imageView_userBG setFrame:frame_imageViewUserBG];
//    [imageView_userBGmask setFrame:frame_imageViewUserBG];
    //The actual ~SHOW~ code is done via IB. No need to crap here.
}

- (IBAction)showFollowers:(id)sender {
    
    FollowersOrFollowingViewController *followersViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"FollowersOrFollowingViewController"];
    [followersViewController setUserId:self.user.userID];
    [followersViewController setViewType:ViewTypeFollowers];
    [self.navigationController pushViewController:followersViewController animated:YES];
}

- (IBAction)showFollowing:(id)sender {
    
    FollowersOrFollowingViewController *followingViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"FollowersOrFollowingViewController"];
    [followingViewController setUserId:self.user.userID];
    [followingViewController setViewType:ViewTypeFollowing];
    [self.navigationController pushViewController:followingViewController animated:YES];
}

- (IBAction)showPosts:(id)sender {
    
    PostsOrStarsViewController *postsViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"PostsOrStarsViewController"];
    [postsViewController setViewType:PostsViewTypePosts];
    [postsViewController setUserId:self.user.userID];
    [self.navigationController pushViewController:postsViewController animated:YES];
}

- (IBAction)showStars:(id)sender {
    
    PostsOrStarsViewController *postsViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"PostsOrStarsViewController"];
    [postsViewController setViewType:PostsViewTypeStars];
    [postsViewController setUserId:self.user.userID];
    [self.navigationController pushViewController:postsViewController animated:YES];
}

- (IBAction)logout:(id)sender {
    
    JLActionSheet *actionSheet = [[JLActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Logout"]];
    [actionSheet setStyle:JLSTYLE_SUPERCLEAN];
    [actionSheet setClickedButtonBlock:^(JLActionSheet *actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            //Cancel
        } else if (buttonIndex == 1) {
            //Logout
            [[PDKeychainBindings sharedKeychainBindings] removeObjectForKey:@"sweetdotnet:accessToken"];
            [[RCUserListStorage sharedInstance] clearUserDB];
            [self deleteUserPlist];
            
            AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"https://rafaelc.com.br/push/api/rmvUser.php"]];
            NSMutableDictionary *jsonParameters = [NSMutableDictionary new];
            [jsonParameters setObject:[[RCUserStorage sharedUserStorage] user] forKey:@"username"];
            [jsonParameters setObject:@"70daa50b2014" forKey:@"apiKey"];
            [client setParameterEncoding:AFJSONParameterEncoding];
            [client postPath:@"" parameters:jsonParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"Logout success on server.");
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"Logout unsuccessful on server.");
            }];
            [self performSegueWithIdentifier:@"logoutSegue" sender:self];
        }
    }];
    
    //UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Confirm your choice:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Logout" otherButtonTitles: nil];
    [actionSheet showInView:self.view];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLayoutSubviews {
    [scrollView setContentSize:CGSizeMake(320, 520)];
    [scrollView setScrollEnabled:YES];
    frame_imageViewUserBG = imageView_userBG.frame;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([[UIApplication sharedApplication].delegate.window respondsToSelector:@selector(tintColor)]) {
        [button_settings setTitleColor:[UIApplication sharedApplication].delegate.window.tintColor forState:UIControlStateNormal];
        [button_logout setTitleColor:[UIApplication sharedApplication].delegate.window.tintColor forState:UIControlStateNormal];
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //hasSet = FALSE;
    
    //[scrollView setContentInset:UIEdgeInsetsMake(65, 0, 0, 0)];
    
    [self.navigationItem setTitle:@"User"];
    label_name.text = @"";
    textView_description.text = @"";
    
    [scrollView setDelegate:self];
    
    [[imageView_userImage layer] setBorderWidth:1.5f];
    [[imageView_userImage layer] setBorderColor:[[UIColor.whiteColor colorWithAlphaComponent:.5f] CGColor]];
    
    label_username.layer.shadowColor = [[UIColor blackColor] CGColor];
    label_name.layer.shadowColor = [[UIColor blackColor] CGColor];
    label_username.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    label_name.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    label_username.layer.shadowRadius = 2.0;
    label_name.layer.shadowRadius = 2.0;
    label_username.layer.shadowOpacity = 0.5;
    label_name.layer.shadowOpacity = 0.5;
    
    UILongPressGestureRecognizer *longpressGestureRecognizer_profilePic = [[UILongPressGestureRecognizer alloc] initWithActionBlock:^(UIGestureRecognizer *gesture) {
        if (gesture.state == UIGestureRecognizerStateBegan) {
            
            AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            RCShowdownImageView *imageView_overlay = [[RCShowdownImageView alloc] initWithFrame:delegate.window.frame];
            [imageView_overlay setImage:imageView_userImage.image];
            [imageView_overlay showViewToView:delegate.window];
        }
    }];
    [imageView_userImage addGestureRecognizer:longpressGestureRecognizer_profilePic];
    
    [self.navigationController setDelegate:self];
    [self.navigationController.navigationBar.topItem setTitle:self.title];
    
    UIBarButtonItem *button_editUser = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(editUser)];
    [self.navigationItem setLeftBarButtonItem:button_editUser];
    
	UIBarButtonItem *button_global = [[UIBarButtonItem alloc] initWithTitle:@"Global" style:UIBarButtonItemStylePlain target:self action:@selector(showGlobal)];
    [self.navigationItem setRightBarButtonItem:button_global];
    
    [self updateUserIntel];
	// Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[ANKClient sharedClient] fetchCurrentUserWithCompletion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
        if (error == nil) {
            [[RCUserStorage sharedUserStorage] setUserObject:(ANKUser *)responseObject];
            [self saveUserWithDictionary:[responseObject JSONDictionary]];
            [self updateUserIntel];
        }
    }];
}



- (void)showGlobal {
	UIViewController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"GlobalStreamViewController"];
	[self.navigationController pushViewController:viewController animated:YES];
}

- (void)editUser {
    EditProfileViewController *editProfileViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"EditProfileViewController"];
    [editProfileViewController setUser:self.user];
    
    [self presentViewController:editProfileViewController animated:YES completion:^{
        
    }];
}

- (void)updateUserIntel {
    ANKUser *user = [[RCUserStorage sharedUserStorage] userObject];
    
    self.user = user;
    
    if (self.user.verifiedDomain == nil) {
        imageView_verified.hidden = true;
    } else {
        imageView_verified.hidden = false;
    }
    
    
    label_name.text = user.name;
    label_username.text = [NSString stringWithFormat:@"@%@", user.username];
    
    //Fix a bug with URL detection in some textviews.
    textView_description.text = user.bio.text;
    textView_description.text = user.bio.text;
    textView_description.text = user.bio.text;
    
//    NSMutableAttributedString *attributedString_followers = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Followers: %i", user.counts.followers]];
//    [attributedString_followers setAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:17.0f], NSForegroundColorAttributeName : [UIColor blackColor]} range:NSMakeRange(0, 10)];
//    [attributedString_followers setAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f], NSForegroundColorAttributeName : [UIColor blackColor]} range:NSMakeRange(10, attributedString_followers.string.length - 10)];
//    [button_followers setAttributedTitle:attributedString_followers forState:UIControlStateNormal];
    
    [button_followers setTitle:[NSString stringWithFormat:@"Followers: %i", user.counts.followers] forState:UIControlStateNormal];
    
//    NSMutableAttributedString *attributedString_following = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Following: %i", user.counts.following]];
//    [attributedString_following setAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:17.0f], NSForegroundColorAttributeName : [UIColor blackColor]} range:NSMakeRange(0, 10)];
//    [attributedString_following setAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f], NSForegroundColorAttributeName : [UIColor blackColor]} range:NSMakeRange(10, attributedString_following.string.length - 10)];
//    [button_following setAttributedTitle:attributedString_following forState:UIControlStateNormal];
    
    [button_following setTitle:[NSString stringWithFormat:@"Following: %i", user.counts.following] forState:UIControlStateNormal];
    
    
    //[imageView_userImage setImageWithURL:user.avatarImage.URL placeholderImage:nil];
    //medallion = [[AGMedallionView alloc] init];
    //[self.view addSubview:medallion];
    
    NSString *finalURLString = [user.avatarImage.URL.absoluteString stringByAppendingString:@"?h=180&w=180"];
    NSURL *userURL = [NSURL URLWithString:finalURLString];
    
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:userURL options:SDWebImageDownloaderUseNSURLCache progress:^(NSUInteger receivedSize, long long expectedSize) {
        // Do nothing;
    } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        if (image) [imageView_userImage setImage:image];
    }];
    
    [imageView_userBG setImageWithURL:user.coverImage.URL placeholderImage:nil];
}
        
        
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)saveUserWithDictionary:(NSDictionary *)dict {
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"user.plist"];
    [dict writeToFile:plistPath atomically:YES];
}

- (void)deleteUserPlist {
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"user.plist"];
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:plistPath error:&error];
}

#pragma mark UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView1 {
    NSLog(@"COF: %f",scrollView.contentOffset.y);
    if (scrollView.contentOffset.y < 0) {
        
        CGRect frame = frame_imageViewUserBG;
        CGFloat fixedContentOffset = scrollView1.contentOffset.y + 0;
        
        frame.size.height = frame.size.height + (fixedContentOffset * (-1));
        [imageView_userBG setFrame:frame];
        [imageView_userBGmask setFrame:frame];
    } else if (scrollView1.contentOffset.y > 0) {
//        CGRect frame = frame_imageViewUserBG;
//        CGFloat fixedContentOffset = scrollView1.contentOffset.y + 65;
//        frame.origin.y = frame_imageViewUserBG.origin.y - fixedContentOffset;
//        [imageView_userBG setFrame:frame];
//        [imageView_userBGmask setFrame:frame];
    }
}


#pragma mark NavBarDelegate


- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (viewController == self) {
        [self.navigationController.navigationBar.topItem setTitle:self.title];
        if ([self.navigationController.parentViewController isKindOfClass:[SNTabBarController class]]) {
            [(SNTabBarController *)[self.navigationController parentViewController] showBottomBar];
        }
    } else {
        if ([self.navigationController.parentViewController isKindOfClass:[SNTabBarController class]]) {
            [(SNTabBarController *)[self.navigationController parentViewController] hideBottomBar];
        }
    }
    
}

@end
