//
//  RCAutoCompleteView.m
//  Sweet.net
//
//  Created by Rafael Costa on 2/26/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//
#import "RCKit.h"
#import "RCAutoCompleteView.h"

@implementation RCAutoCompleteView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        array_autoCompleteItems = NSMutableArray.new;
        
        array_savedAutoCompleteItems = NSMutableArray.new;
        
        array_savedAutoCompleteItems = [[RCUserListStorage sharedInstance] userList];
        
        _typedString = NSString.new;
        
        typedRange = NSMakeRange(0, 0);
        
        
    }
    return self;
}

- (void)setIsTypingAutocomplete:(BOOL)isTypingAutocomplete {
    _isTypingAutocomplete = isTypingAutocomplete;
    if (isTypingAutocomplete == false) {
        for (UIView *view in self.subviews) {
            if ([view isKindOfClass:[UIButton class]]) [view removeFromSuperview];
        }
    }
}

- (void)setTypedString:(NSString *)typedString withRange:(NSRange)range; {
    _typedString = typedString;
    typedRange = range;
    
    _typedString = [_typedString stringByReplacingOccurrencesOfString:@"@" withString:@""];
    
    [array_autoCompleteItems removeAllObjects];
    for(NSString *string in array_savedAutoCompleteItems) {
        NSRange substringRange = [[string lowercaseString] rangeOfString:[_typedString stringByReplacingOccurrencesOfString:@" " withString:@""]];
        if (substringRange.location == 1 || substringRange.location == 0) {
            [array_autoCompleteItems addObject:[NSString stringWithFormat:@"@%@", string]];
        }
    }
    [array_autoCompleteItems sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    if (_typedString.length > 1) {
        [array_autoCompleteItems addObject:[NSString stringWithFormat:@"@%@", _typedString]];
    }
    
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[UIButton class]]) [view removeFromSuperview];
    }

    
    [self reload];
}

- (void)reload {
    if (array_autoCompleteItems.count > 6) {
        array_autoCompleteItems = [array_autoCompleteItems subarrayWithRange:NSMakeRange(0, 6)].mutableCopy;
    }
    
    float addedSpacing = 5.f;
    
    for (NSString *completion in array_autoCompleteItems) {
        
        UIButton *button_completion = [[UIButton alloc] initWithFrame:CGRectMake(addedSpacing, 0, 46, 30)];
        [button_completion setTitleColor:self.tintColor forState:UIControlStateNormal];
        [button_completion setTitle:completion forState:UIControlStateNormal];
        [button_completion sizeToFit];
        addedSpacing += button_completion.frame.size.width + 5.f;
        [button_completion addTarget:self action:@selector(completionClickedWithSender:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button_completion];
    }
    
    [self setContentSize:CGSizeMake(addedSpacing, 30)];
}

- (void)completionClickedWithSender:(id)sender {
    //Add method to return completion with sender's title.
    
    NSLog(@"Completion Clicked: %@", [(UIButton *)sender titleLabel].text);
    
    [self.delegate returnedCompletion:[(UIButton *)sender titleLabel].text withRange:typedRange];
    typedRange = NSMakeRange(0, 0);
    //[self removeFromSuperview];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
