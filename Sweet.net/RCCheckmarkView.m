//
//  RCCheckmarkView.m
//  Sweet.net
//
//  Created by Rafael Costa on 16/06/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "RCCheckmarkView.h"

@implementation RCCheckmarkView

- (id)initWithPosition:(CGPoint)position superview:(UIView *)superview andTimeout:(float)timeout
{
    self = [super init];
    if (self) {
        // Initialization code
        self.frame = CGRectMake(position.x, position.y, 44, 44);
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.35f];
        [self.layer setCornerRadius:7.5f];
        self.image = [UIImage imageNamed:@"positiveCheckmark"];
        [superview addSubview:self];
        [NSTimer scheduledTimerWithTimeInterval:timeout target:self selector:@selector(animateOut) userInfo:nil repeats:NO];
        self.animationDuration = .75;
    }
    return self;
}

- (void)animateOut {
    [UIView animateWithDuration:self.animationDuration animations:^{
        self.alpha = .0;
    } completion:^(BOOL finished) {
        if (finished) [self removeFromSuperview];
    }];
}


@end
