//
//  GlobalStreamViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 17/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADNKit.h"
#import "RCKit.h"
#import "JLActionSheet.h"

@interface GlobalStreamViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, RCPostCellDelegate> {
    JLActionSheet *actionSheet_repost;
    
    IBOutlet UINavigationBar *navBar;
    IBOutlet UITableView *tableView_posts;
    NSMutableArray *array_posts;
    
    IBOutlet UIRefreshControl *refreshControl;
    
    NSString *lastPostID;
}
- (IBAction)openSearch:(id)sender;
@end
