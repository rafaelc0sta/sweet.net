//
//  AddFilterViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 1/17/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import "AddFilterViewController.h"
#import "RCKit.h"

@interface AddFilterViewController ()

@end

@implementation AddFilterViewController

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

- (IBAction)add:(id)sender {
    id filter;
    
    if (segmentedControl_filterType.selectedSegmentIndex == 0) {
        filter = [[RCFilterContent alloc] init];
    } else {
        filter = [[RCFilterClient alloc] init];
    }
    
    [filter setQuery:textField_query.text];
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    NSNumber *timeStampObj = [NSNumber numberWithDouble:timeStamp];
    [filter setID:[NSString stringWithFormat:@"%@", timeStampObj]];
    if (segmentedControl_searchOptions.selectedSegmentIndex == 0) {
        //Contains
        [filter setSearchOption:RCFilterCompareOptionContains];
    } else if (segmentedControl_searchOptions.selectedSegmentIndex == 1) {
        //Equals to
        [filter setSearchOption:RCFilterCompareOptionEquals];
    }
    
    [[RCFilterManager sharedFilterManager] addFilter:filter];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)textDidChange:(id)sender {
    barButtonItem_save.enabled = true;
    
    if ([textField_query.text isEqualToString:@" "] || [textField_query.text isEqualToString:@"  "]  || textField_query.text.length == 0) {
        barButtonItem_save.enabled = false;
    }
}

@end
