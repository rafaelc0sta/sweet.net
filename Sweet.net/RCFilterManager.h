//
//  RCFilterManager.h
//  Sweet.net
//
//  Created by Rafael Costa on 1/17/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RCKit.h"

@interface RCFilterManager : NSObject {
    
}
@property (nonatomic) NSMutableArray *filters;

+ (instancetype)sharedFilterManager;

- (BOOL)addFilter:(RCFilter *)filter;
- (BOOL)removeFilterWithID:(NSString *)filterID;
- (BOOL)removeFilter:(RCFilter *)filter;

- (void)saveFilters;
- (void)loadFilters;


- (NSArray *)applyFiltersToArray:(NSArray *)filterees;
@end
