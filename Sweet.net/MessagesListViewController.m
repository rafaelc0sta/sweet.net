//
//  MessagesListViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 03/05/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "MessagesListViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIGestureRecognizer+Blocks.h"
#import "PostDetailViewController.h"
#import "UIImageView+WebCache.h"
#import "ADNKit.h"
#import "SVPullToRefresh.h"
#import "SearchViewController.h"
#import "NSTimer+Blocks.h"
#import "RCMessagesViewController.h"
#import "MBProgressHUD.h"

@interface MessagesListViewController ()

@end

@implementation MessagesListViewController

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self.navigationController.navigationBar setTranslucent:YES];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadConversations) forControlEvents:UIControlEventValueChanged];
    [tableView_conversations addSubview:refreshControl];
    
    //[tableView_conversations setEditing:YES];
    
    [self.navigationController setDelegate:self];
    
//    UIBarButtonItem *barButtonItemCompose = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(compose)];
//    self.navigationItem.rightBarButtonItem = barButtonItemCompose;
    
	// Do any additional setup after loading the view.
    
    [self reloadConversations];
}

- (void)compose {
    AutoCompleteViewController *ac = [[AutoCompleteViewController alloc] init];
    [ac setDelegate:self];
    [self presentViewController:ac animated:YES completion:nil];
}


#pragma mark AutoCompleteDelegate

- (void)autoComplete:(AutoCompleteViewController *)autoComplete didReturnWithUsername:(NSString *)username andUserID:(NSString *)userID {
    RCMessagesViewController *viewController = [[RCMessagesViewController alloc] init];
    [viewController setHidesBottomBarWhenPushed:YES];
    [viewController setDestinationUserID:userID];
    
    for (UIView *view in self.view.subviews) {
        [view setUserInteractionEnabled:NO];
    }
    
    
    [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.navigationController pushViewController:viewController animated:YES];
        for (UIView *view in self.view.subviews) {
            [view setUserInteractionEnabled:YES];
        }
        [MBProgressHUD hideAllHUDsForView:appWindow animated:YES];
    });
    
    
}

- (void)reloadConversations {
    for (id view in tableView_conversations.subviews) {
        if ([view isKindOfClass:[UIRefreshControl class]]) {
            [view beginRefreshing];
        }
    }
    [[ANKClient sharedClient] fetchCurrentUserPrivateMessageChannelsWithCompletion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
        if (error == nil) {
            array_conversations = [responseObject mutableCopy];
            [tableView_conversations reloadData];
            for (id view in tableView_conversations.subviews) {
                if ([view isKindOfClass:[UIRefreshControl class]]) {
                    [view endRefreshing];
                }
            }
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:tableView_conversations]) return [array_conversations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView_conversations dequeueReusableCellWithIdentifier:identifier];
    ANKChannel *channel = (ANKChannel *)array_conversations[indexPath.row];
    
    
//    CALayer *mask = [CALayer layer];
//    mask.contents = (id)[[UIImage imageNamed:@"profileMask"] CGImage];
//    mask.frame = CGRectMake(0, 0, cell.imageView.bounds.size.width, cell.imageView.bounds.size.height);
//    cell.imageView.layer.mask = mask;
//    cell.imageView.layer.masksToBounds = YES;
    
    cell.imageView.image = [UIImage imageWithCIImage:[[self imageWithImage:[UIImage imageNamed:@"placeholder"] convertToSize:CGSizeMake(48, 48)] CIImage] scale:2.0 orientation:UIImageOrientationUp];
    
    [cell.imageView.layer setMasksToBounds:YES];
    [cell.imageView.layer setCornerRadius:48.f/2.0];
    
    cell.textLabel.text = [@"" stringByAppendingString:@"Loading..."];
    
    
    NSString *finalURLString = [channel.owner.avatarImage.URL.absoluteString stringByAppendingString:@"?h=120&w=120"];
    NSURL *userURL = [NSURL URLWithString:finalURLString];
    
    if ([channel.owner.userID isEqualToString:[[RCUserStorage sharedUserStorage] userObject].userID]) {
        [[ANKClient sharedClient] fetchUserWithID:channel.writers.userIDs[0] completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                ANKUser *user = responseObject;
                cell.textLabel.text = [@"@" stringByAppendingString:user.username];
                
                [cell.imageView setImageWithURL:userURL placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                    cell.imageView.image = [self imageWithImage:cell.imageView.image convertToSize:CGSizeMake(48, 48)];
                }];
            } else {
                cell.textLabel.text = @"Unknown User";
                [cell.imageView setImageWithURL:userURL placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                    cell.imageView.image = [self imageWithImage:cell.imageView.image convertToSize:CGSizeMake(48, 48)];
                }];
            }
        }];
    } else {
        cell.textLabel.text = [@"@" stringByAppendingString:channel.owner.username];
        [cell.imageView setImageWithURL:userURL placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            cell.imageView.image = [self imageWithImage:cell.imageView.image convertToSize:CGSizeMake(48, 48)];
        }];
    }
    
    
    
    
    for (id object in cell.contentView.subviews) {
        if ([object isKindOfClass:[CustomBadge class]]) {
            [object removeFromSuperview];
        }
    }
    
    if (channel.latestMessage.text.length >= 28) {
        cell.detailTextLabel.text = [channel.latestMessage.text substringToIndex:28];
        cell.detailTextLabel.text = [cell.detailTextLabel.text stringByAppendingString:@"..."];
    } else {
        cell.detailTextLabel.text = channel.latestMessage.text;
    }
    
    if (channel.hasUnreadMessages == TRUE && ![channel.latestMessage.user.username isEqualToString:[[RCUserStorage sharedUserStorage] user]]) {
        
        [cell.contentView setBackgroundColor:[UIColor colorWithRed:233.f/255.f green:233.f/255.f blue:233.f/255.f alpha:1.0f]];
        CustomBadge *badge = [CustomBadge customBadgeWithString:@"New"];
        NSUInteger customSpacing = 5;
        [badge setFrame:CGRectMake(cell.frame.size.width - badge.frame.size.width - customSpacing, cell.contentView.frame.size.height/2 - badge.frame.size.height/2, badge.frame.size.width, badge.frame.size.height)];
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [cell.contentView addSubview:badge];
    } else {
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
    
    [cell layoutSubviews];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = cell.contentView.backgroundColor;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[ANKClient sharedClient] unsubscribeToChannel:[array_conversations objectAtIndex:indexPath.row] completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                [self reloadConversations];
            }
        }];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 65.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if ([cell.textLabel.text rangeOfString:@"Loading..."].location == NSNotFound) {
        RCMessagesViewController *viewController = [[RCMessagesViewController alloc] init];
        [viewController setThisChannel:[array_conversations objectAtIndex:indexPath.row]];
        [viewController setHidesBottomBarWhenPushed:YES];
        [viewController setTitleToShow:[cell textLabel].text];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
}

#pragma mark NavBarDelegate


- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (viewController == self) {
        [self.navigationController.navigationBar.topItem setTitle:self.title];
        [self reloadConversations];
        if ([self.navigationController.parentViewController isKindOfClass:[SNTabBarController class]]) {
            [(SNTabBarController *)[self.navigationController parentViewController] showBottomBar];
        }
    } else {
        if ([self.navigationController.parentViewController isKindOfClass:[SNTabBarController class]]) {
            [(SNTabBarController *)[self.navigationController parentViewController] hideBottomBar];
        }
    }
}


@end
