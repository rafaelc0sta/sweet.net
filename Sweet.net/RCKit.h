//
//  RCKit.h
//  Sweet.net
//
//  Created by Rafael Costa on 29/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#ifndef Sweet_net_RCKit_h
#define Sweet_net_RCKit_h

#define defaultBlue [UIColor colorWithRed:0.f/255.f green:185.f/255.f blue:1.f alpha:1.f]
#define appWindow [UIApplication sharedApplication].windows[0]

//#define appWindow [UIApplication sharedApplication].windows[0].subviews[0]

#import "RCUserListStorage.h"

#import "RCPushStorage.h"
#import "RCPostCell.h"
#import "RCInteractionCell.h"
#import "RCComposeViewController.h"
#import "AutoCompleteViewController.h"
#import "RCUserStorage.h"
#import "RCTabBarController.h"
#import "UILabel+VerticalAlign.h"
#import "NSMutableArray+ContainsString.h"
#import "RCActionSheet.h"
#import "RCNavigationController.h"
#import "RCMessagesViewController.h"
#import "RCMapPin.h"
#import "RCMaskedImageView.h"
#import "RCCheckmarkView.h"
#import "SNTabBarController.h"
#import "RCShowdownImageView.h"
#import "RCFilter.h"
#import "RCFilterContent.h"
#import "RCFilterClient.h"
#import "RCButton.h"
#import "RCAutoCompleteView.h"
#import "RCFilterManager.h"

#endif
