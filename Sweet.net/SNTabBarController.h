//
//  SNTabBarController.h
//  SweetDotNet
//
//  Created by Rafael Costa on 13/08/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SNPopUpView.h"

@interface SNTabBarController : UIViewController <SNPopUpViewDelegate> {
    IBOutlet UIView *mainContainer;

    UITapGestureRecognizer *tapGestureRecognizer;
    
    SNPopUpView *popUpView_main;
    
    IBOutlet UIButton *button_popUp;
    
    UIViewController *first;
    UIViewController *second;
    UIViewController *third;
    UIViewController *fourth;
    
    
    CGRect firstButtonRect;
    CGRect defaultPopupRect;
    

	//TO-DO: Handle this lost view controller.
    UIViewController *globalFeed;
    
    UINavigationController *nav1;
    UINavigationController *nav2;
    UINavigationController *nav3;
    UINavigationController *nav4;

    BOOL firstInstatiated;
}
@property (nonatomic, setter = setSelectedIndex:) int selectedIndex;

- (IBAction)changePopUpState:(id)sender;
- (void)showBottomBar;
- (void)hideBottomBar;
@end
