//
//  MessagesListViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 03/05/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AutoCompleteViewController.h"
#import "CustomBadge.h"
@interface MessagesListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, AutoCompleteDelegate> {
    
    NSMutableArray *array_conversations;
    
    IBOutlet UITableView *tableView_conversations;
    
}
- (void)completionViewHasReturnedUser;
@end
