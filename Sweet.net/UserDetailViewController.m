//
//  UserDetailViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 20/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "UserDetailViewController.h"
#import "UIGestureRecognizer+Blocks.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "PostsOrStarsViewController.h"
#import "FollowersOrFollowingViewController.h"
#import "AppDelegate.h"

@interface UserDetailViewController ()

@end

@implementation UserDetailViewController

- (IBAction)showFollowers:(id)sender {
    FollowersOrFollowingViewController *followersViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"FollowersOrFollowingViewController"];
    [followersViewController setUserId:self.user.userID];
    [followersViewController setViewType:ViewTypeFollowers];
    [self.navigationController pushViewController:followersViewController animated:YES];
}

- (IBAction)showFollowing:(id)sender {
    
    FollowersOrFollowingViewController *followingViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"FollowersOrFollowingViewController"];
    [followingViewController setUserId:self.user.userID];
    [followingViewController setViewType:ViewTypeFollowing];
    [self.navigationController pushViewController:followingViewController animated:YES];
}

- (IBAction)showPosts:(id)sender {
    
    PostsOrStarsViewController *postsViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"PostsOrStarsViewController"];
    [postsViewController setViewType:PostsViewTypePosts];
    [postsViewController setUserId:self.user.userID];
    [self.navigationController pushViewController:postsViewController animated:YES];
}

- (IBAction)showStars:(id)sender {
    
    PostsOrStarsViewController *postsViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"PostsOrStarsViewController"];
    [postsViewController setViewType:PostsViewTypeStars];
    [postsViewController setUserId:self.user.userID];
    [self.navigationController pushViewController:postsViewController animated:YES];
}


- (IBAction)muteOrUnmute:(id)sender {
    [button_mute setUserInteractionEnabled:FALSE];
    if (self.user.currentUserMuted == TRUE) {
        [[ANKClient sharedClient] unmuteUser:self.user completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Unmuted";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                [button_mute setUserInteractionEnabled:TRUE];
                self.user.currentUserMuted = !self.user.currentUserMuted;
                [self updateUserIntel];
            }
        }];
    } else {
        [[ANKClient sharedClient] muteUser:self.user completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Muted";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                [button_mute setUserInteractionEnabled:TRUE];
                self.user.currentUserMuted = !self.user.currentUserMuted;
                [self updateUserIntel];
            }
        }];
    }
}
- (IBAction)followOrUnfollow:(id)sender {
    
    [button_follow setUserInteractionEnabled:FALSE];
    if (self.user.currentUserFollows == TRUE) {
        [[ANKClient sharedClient] unfollowUser:self.user completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Unfollowed";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                [button_follow setUserInteractionEnabled:TRUE];
                self.user.currentUserFollows = !self.user.currentUserFollows;
                [self updateUserIntel];
            }
        }];
    } else {
        [[ANKClient sharedClient] followUser:self.user completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Followed";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                [button_follow setUserInteractionEnabled:TRUE];
                self.user.currentUserFollows = !self.user.currentUserFollows;
                [self updateUserIntel];
            }
        }];
    }
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.delegate = self;
    
    [button_follow setTintColor:defaultBlue];
    [button_follow setButtonTinted:NO];
    
    [button_mute setTintColor:defaultBlue];
    
    [button_mute setButtonTinted:NO];
    
    [[imageView_userImage layer] setBorderWidth:1.5f];
    [[imageView_userImage layer] setBorderColor:[[UIColor.whiteColor colorWithAlphaComponent:.5f] CGColor]];
    
    label_username.layer.shadowColor = [[UIColor blackColor] CGColor];
    label_userRealName.layer.shadowColor = [[UIColor blackColor] CGColor];
    label_username.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    label_userRealName.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    label_username.layer.shadowRadius = 2.0;
    label_userRealName.layer.shadowRadius = 2.0;
    label_username.layer.shadowOpacity = 0.5;
    label_userRealName.layer.shadowOpacity = 0.5;
    
    UILongPressGestureRecognizer *longpressGestureRecognizer_profilePic = [[UILongPressGestureRecognizer alloc] initWithActionBlock:^(UIGestureRecognizer *gesture) {
        if (gesture.state == UIGestureRecognizerStateBegan) {
            
            AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            
            RCShowdownImageView *imageView_overlay = [[RCShowdownImageView alloc] initWithFrame:delegate.window.frame];
            [imageView_overlay setImage:imageView_userImage.image];
            [imageView_overlay showViewToView:delegate.window];
        }
    }];
    
    [imageView_userImage addGestureRecognizer:longpressGestureRecognizer_profilePic];
    
    UIBarButtonItem *barButtonItemCompose = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(compose)];
    self.navigationItem.rightBarButtonItem = barButtonItemCompose;
    
    [label_followsYou setHidden:TRUE];
    
    button_follow.titleLabel.textAlignment = NSTextAlignmentCenter;
    button_follow.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    textView_userDescription.text = @"";
    label_username.text = @"";
    label_userRealName.text = @"";
    
	// Do any additional setup after loading the view.
}

- (void)compose {
    
    RCComposeViewController *cpVC = [[RCComposeViewController alloc]initWithNibName:@"RCComposeViewController" bundle:[NSBundle mainBundle]];
    
    [cpVC setPostToReply:nil];
    [cpVC setInitialText:[[@"@" stringByAppendingString:self.user.username] stringByAppendingString:@" "]];
    
    
    
    [self presentViewController:cpVC animated:YES completion:^(void){
        
    }];
}

- (void)viewDidLayoutSubviews {
    [scrollView setContentSize:CGSizeMake(320, 509)];
    [scrollView setScrollEnabled:YES];
}

- (void)disableAllItems {
    for (id object in self.view.subviews) {
        [object setUserInteractionEnabled:FALSE];
    }
}

- (void)enableAllItems {
    for (id object in self.view.subviews) {
        [object setUserInteractionEnabled:TRUE];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [scrollView setContentOffset:CGPointZero];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.user == nil) {
        [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
        
        [self disableAllItems];
        
        [[ANKClient sharedClient] fetchUserWithID:self.usernameID completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                ANKUser *user = responseObject;
                self.user = user;
                [MBProgressHUD hideHUDForView:appWindow animated:YES];
                [self updateUserIntel];
                [self enableAllItems];
            } else {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
                [hud setLabelText:@"Error loading user information!"];
                [hud hide:YES afterDelay:1.f];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    } else {
        [self updateUserIntel];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    self.navigationItem.title = @"User Detail";
    frame_imageViewUserBG = imageView_userBG.frame;
}

- (void)updateUserIntel {
    if (self.user.verifiedDomain == nil) {
        imageView_verified.hidden = true;
    } else {
        imageView_verified.hidden = false;
    }
    
    if (self.user.currentUserFollows == TRUE) {
        [button_follow setTitle:@"Unfollow" forState:UIControlStateNormal];
        [button_follow setButtonTinted:YES];
    } else {
        [button_follow setTitle:@"Follow" forState:UIControlStateNormal];
        [button_follow setButtonTinted:NO];
    }
    
    if (self.user.currentUserMuted == TRUE) {
        [button_mute setTitle:@"Unmute" forState:UIControlStateNormal];
        [button_mute setButtonTinted:YES];
    } else {
        [button_mute setTitle:@"Mute" forState:UIControlStateNormal];
        [button_mute setButtonTinted:NO];
    }
    
    [button_followers setTitle:[NSString stringWithFormat:@"Followers: %i", self.user.counts.followers] forState:UIControlStateNormal];
    

    [button_following setTitle:[NSString stringWithFormat:@"Following: %i", self.user.counts.following] forState:UIControlStateNormal];
    
    label_username.text = [NSString stringWithFormat:@"@%@",self.user.username];
    label_userRealName.text = self.user.name;
    textView_userDescription.text = self.user.bio.text;
    textView_userDescription.text = self.user.bio.text;
    textView_userDescription.text = self.user.bio.text;
    [[imageView_userImage layer] setShadowColor:[UIColor blackColor].CGColor];
    
    NSString *finalURLString = [self.user.avatarImage.URL.absoluteString stringByAppendingString:@"?h=180&w=180"];
    NSURL *userURL = [NSURL URLWithString:finalURLString];
    
    
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:userURL options:SDWebImageDownloaderUseNSURLCache progress:^(NSUInteger receivedSize, long long expectedSize) {
        
    } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        if (image) [imageView_userImage setImage:image];
    }];
    
    [imageView_userBG setImageWithURL:self.user.coverImage.URL placeholderImage:nil];

    
    if ([self.user followsCurrentUser]) {
        [label_followsYou setHidden:false];
        label_followsYou.text = @"Follows you.";
    }
    
    
    if ([self.user.userID isEqualToString:[[RCUserStorage sharedUserStorage] userObject].userID]) {
        button_follow.hidden = TRUE;
        button_mute.hidden = TRUE;
        [label_followsYou setHidden:FALSE];
        label_followsYou.text = @"This is you.";
    }

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView1 {
    if (scrollView1.contentOffset.y < 0) {
        CGRect frame = frame_imageViewUserBG;
        frame.size.height = frame.size.height + (scrollView1.contentOffset.y * (-1));
        [imageView_userBG setFrame:frame];
        [imageView_userBGMask setFrame:frame];
    } else if (scrollView1.contentOffset.y > 0) {
//        CGRect frame = frame_imageViewUserBG;
//        frame.origin.y = frame_imageViewUserBG.origin.y - scrollView1.contentOffset.y;
//        [imageView_userBG setFrame:frame];
//        [imageView_userBGMask setFrame:frame];
    }
}

@end
