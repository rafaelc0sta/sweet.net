//
//  RCActionSheet.h
//  Sweet.net
//
//  Created by Rafael Costa on 23/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADNKit.h"

typedef void (^DismissBlock)(int buttonIndex);
typedef void (^VoidBlock)();

@interface RCActionSheet : UIActionSheet <UIActionSheetDelegate> {
    DismissBlock blockToExecute;
}
@property (nonatomic) ANKPost *post;

- (id)initWithTitle:(NSString *)title cancelButtonTitle:(NSString *)cancel destructiveButtonTitle:(NSString *)destructive otherButtonTitles:(NSArray *)buttonTitles completionBlock:(DismissBlock)block;
@end
