//
//  FollowersOrFollowingViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 25/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "FollowersOrFollowingViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ADNKit.h"
#import "UserDetailViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"

@implementation FollowersOrFollowingViewController

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    lastId = @"0";
    
    tableView_users.showsInfiniteScrolling = NO;
    [tableView_users addInfiniteScrollingWithActionHandler:^{
        [self loadMoreUsers];
    }];
    [self loadMoreUsers];
    
    if (self.viewType == ViewTypeFollowers) [self.navigationItem setTitle:@"Followers"];
    else if (self.viewType == ViewTypeFollowing) [self.navigationItem setTitle:@"Following"];
}

- (void)loadMoreUsers {
    ANKPaginationSettings *paginationSettings = [ANKPaginationSettings settingsWithCount:100];
    
    
    if (![lastId isEqualToString:@"0"]) [paginationSettings setBeforeID:lastId];
    
    //Constant
    [paginationSettings setSinceID:@"0"];
    if (self.viewType == ViewTypeFollowers) {
        
        [[[ANKClient sharedClient] clientWithPagination:paginationSettings] fetchUsersFollowingUserWithID:self.userId completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            NSArray *users = responseObject;
            if (error == nil) {
                if (array_users.count > 0) array_users = [[array_users arrayByAddingObjectsFromArray:users] mutableCopy];
                else array_users = [users mutableCopy];
                lastId = meta.minID;
                tableView_users.showsInfiniteScrolling = YES;
                if (meta.moreDataAvailable == FALSE) tableView_users.showsInfiniteScrolling = NO;
                [tableView_users reloadData];
                [tableView_users.infiniteScrollingView stopAnimating];
            } else {
                
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Error loading followers";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                
                if (array_users.class <= 0) [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    } else if (self.viewType == ViewTypeFollowing) {
        [[[ANKClient sharedClient] clientWithPagination:paginationSettings] fetchUsersUserWithIDFollowing:self.userId completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                NSArray *users = responseObject;
                if (array_users.count > 0) array_users = [[array_users arrayByAddingObjectsFromArray:users] mutableCopy];
                else array_users = [users mutableCopy];
                lastId = meta.minID;
                tableView_users.showsInfiniteScrolling = YES;
                if (meta.moreDataAvailable == FALSE) tableView_users.showsInfiniteScrolling = NO;
                [tableView_users reloadData];
                [tableView_users.infiniteScrollingView stopAnimating];
            } else {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Error loading following";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                if (array_users.class <= 0) [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        
    }
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [array_users count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView_users dequeueReusableCellWithIdentifier:identifier];
    
    ANKUser *user = [array_users objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [@"@" stringByAppendingString:user.username];
    
    cell.imageView.image = [UIImage imageNamed:@"placeholder"];
    
    [UIImage imageWithCIImage:[[self imageWithImage:[UIImage imageNamed:@"placeholder"] convertToSize:CGSizeMake(48, 48)] CIImage] scale:2.0 orientation:UIImageOrientationUp];
    
    [cell.imageView.layer setMasksToBounds:YES];
    [cell.imageView.layer setCornerRadius:24.f];
    
    NSString *finalURLString = [user.avatarImage.URL.absoluteString stringByAppendingString:@"?h=120&w=120"];
    NSURL *userURL = [NSURL URLWithString:finalURLString];
    
    [cell.imageView setImageWithURL:userURL placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        cell.imageView.image = [self imageWithImage:cell.imageView.image convertToSize:CGSizeMake(48, 48)];
    }];
    [cell layoutSubviews];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 65.f;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UserDetailViewController *userDetailViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"UserDetailViewController"];
    [userDetailViewController setUser:[array_users objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:userDetailViewController animated:YES];
}

@end
