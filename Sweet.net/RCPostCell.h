//
//  RCPostCell.h
//  Sweet.net
//
//  Created by Rafael Costa on 13/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UILabel+VerticalAlign.h"

#import "ADNKit.h"
#import "RCKit.h"
typedef enum {
    RCPostCellActionRepost,
    RCPostCellActionStar,
    RCPostCellActionReply,
    RCPostCellActionThread,
} RCPostCellAction;

typedef enum {
    RCDirectionRightToLeft,
    RCDirectionLeftToRight,
} RCDirection;

@protocol RCPostCellDelegate;

@interface RCPostCell : UITableViewCell  <UIGestureRecognizerDelegate> {
    IBOutlet UIView *overlayView;
    IBOutlet UIView *postView;
    IBOutlet UIView *threadView;
    
    IBOutlet UIButton *button_repost;
    IBOutlet UIButton *button_star;
    IBOutlet UIButton *button_reply;
    
    BOOL isShowingMiniOptions;
    
    UISwipeGestureRecognizer *miniOptionsGesture;
    UISwipeGestureRecognizer *conversationGesture;
    
    // Alpha-prototyping stage. Improving animations.
    CGPoint firstTouchPosition;
    CGPoint currentTouchPosition;
}
@property (nonatomic) BOOL isAnimating;

@property (nonatomic) IBOutlet UILabel *label_username;
@property (nonatomic) IBOutlet UILabel *label_longAgo;
@property (nonatomic) IBOutlet UILabel *label_post;
@property (nonatomic) IBOutlet UIImageView *imageView_userImage;

@property (nonatomic) IBOutlet UIImageView *imageView_picture;
@property (nonatomic) IBOutlet UIImageView *imageView_location;

@property (nonatomic) ANKPost *post;
@property (nonatomic) NSIndexPath *indexPath;
@property (nonatomic, setter = setMiniOptionsEnabled:) BOOL isMiniOptionsEnabled;
@property (nonatomic, setter = setThreadOptionsEnabled:) BOOL isThreadOptionsEnabled;

@property (nonatomic) id<RCPostCellDelegate> delegate;

- (BOOL)isShowingMiniOptions;

- (void)setRepostedButtonOn:(BOOL)on;
- (void)setStarredButtonOn:(BOOL)on;

- (IBAction)replyToThisPost:(id)sender;
- (IBAction)repostThisPost:(id)sender;
- (IBAction)starThisPost:(id)sender;
- (void)hideMiniOptions;
- (void)setContentViewBackgorundColor:(UIColor *)color;

@end

@protocol RCPostCellDelegate <NSObject>

- (void)cell:(RCPostCell *)cell wantsTo:(RCPostCellAction)action;
// Should I replace this with a "non-nothing contained" function?
//- (void)cell:(RCPostCell *)cell did!_remove_!ShowMiniOptions:(id)nothing;
// Improve later:
//- (void)didDisplayMiniOptionsForCell:(RCPostCell *)cell;
- (void)cell:(RCPostCell *)cell didMakeSwipeMotionWithDirection:(RCDirection)direction;
//- (void)displayedMiniOptionsForCell:(RCPostCell *)cell;

@end
