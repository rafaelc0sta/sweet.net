//
//  FiltersViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 1/16/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import "FiltersViewController.h"
#import "RCKit.h"

@interface FiltersViewController ()

@end

@implementation FiltersViewController

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

- (IBAction)addFilter:(id)sender {
    [self performSegueWithIdentifier:@"addFilterSegue" sender:self];
}

- (IBAction)done:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [tableView_filters reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[RCFilterManager sharedFilterManager] saveFilters];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[RCFilterManager sharedFilterManager] filters].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    id filter = [[[RCFilterManager sharedFilterManager] filters] objectAtIndex:indexPath.row];
    
    NSString *filteredObject = @"";
    if ([filter isMemberOfClass:[RCFilterContent class]]) {
        filteredObject = @"text";
    } else if ([filter isMemberOfClass:[RCFilterClient class]]) {
        filteredObject = @"client";
    }
    
    NSString *searchOption = @"";
    
    if ([(RCFilter *)filter searchOption] == RCFilterCompareOptionContains) searchOption = @"contains";
    else if ([(RCFilter *)filter searchOption] == RCFilterCompareOptionEquals) searchOption = @"equals";
    
    cell.textLabel.text = [NSString stringWithFormat:@"If %@ %@", filteredObject, searchOption];
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"\"%@\"", [(RCFilter *)filter query]];
    
    [cell layoutSubviews];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSArray *filters = [[RCFilterManager sharedFilterManager] filters];
        [[RCFilterManager sharedFilterManager] removeFilter:[filters objectAtIndex:indexPath.row]];
        [tableView_filters deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 65.f;
}


@end
