//
//  RCCellButton.m
//  Sweet.net
//
//  Created by Rafael Costa on 3/15/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import "RCCellButton.h"

@implementation RCCellButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id) awakeAfterUsingCoder:(NSCoder*)aDecoder {
    BOOL theThingThatGotLoadedWasJustAPlaceholder = ([[self subviews] count] == 0);
    if (theThingThatGotLoadedWasJustAPlaceholder) {
        RCCellButton *theRealThing = [[(id) [NSBundle mainBundle] loadNibNamed:@"RCCellButton" owner:nil options:nil] objectAtIndex:0];
        
        // pass properties through
        
        theRealThing.frame = self.frame;
        theRealThing.autoresizingMask = self.autoresizingMask;
        theRealThing.alpha = self.alpha;
        // ...
        
        return theRealThing;
    }
    return self;
}

- (void)setTitle:(NSString *)title withSubtitle:(NSString *)subtitle {
    [label_title setText:title];
    [label_subtitle setText:subtitle];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
