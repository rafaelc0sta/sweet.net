//
//  AddFilterViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 1/17/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddFilterViewController : UIViewController <UITextFieldDelegate> {
    
    IBOutlet UIBarButtonItem *barButtonItem_save;
    
    IBOutlet UITextField *textField_query;
    IBOutlet UISegmentedControl *segmentedControl_filterType;
    IBOutlet UISegmentedControl *segmentedControl_searchOptions;
}
- (IBAction)add:(id)sender;
- (IBAction)cancel:(id)sender;
@end
