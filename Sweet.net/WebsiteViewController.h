//
//  WebsiteViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 06/04/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCKit.h"
#import "JLActionSheet.h"

@interface WebsiteViewController : UIViewController <UIActionSheetDelegate, UIWebViewDelegate> {
    IBOutlet UIWebView *webView;
    
    JLActionSheet *actionSheet;
    
    IBOutlet UIBarButtonItem *barButtonItem_back;
    IBOutlet UIBarButtonItem *barButtonItem_forward;
    
    UIBarButtonItem *barButtonItem_share;
}
@property (nonatomic) NSURL *url;
- (void)openInThirdPartyApp;
- (IBAction)readerMode:(id)sender;
@end
