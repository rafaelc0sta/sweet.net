//
//  KTLLoginViewController.h
//  LoginViewController
//
//  Created by Steven Baranski on 1/2/13.
//  Copyright (c) 2013 komorka technology, llc. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@protocol KTLLoginViewControllerDelegate;
@interface KTLLoginViewController : UITableViewController 
@property (nonatomic) NSObject <KTLLoginViewControllerDelegate> *delegate;
@property (nonatomic) BOOL canLoginWithADNApp;
@property (nonatomic) int numberOfSections;
@property (nonatomic) BOOL hasLoggedFromApp;
@end
@protocol KTLLoginViewControllerDelegate <NSObject>

- (void)loginController:(KTLLoginViewController *)loginController didAddAccountWithUserDictionary:(NSDictionary *)userDict;

@end
