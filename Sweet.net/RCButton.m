//
//  RCButton.m
//  Sweet.net
//
//  Created by Rafael Costa on 2/18/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import "RCButton.h"

@implementation RCButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self redrawBounds];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self redrawBounds];
}

- (void)redrawBounds {
    [[self layer] setCornerRadius:7.5f];
    [[self layer] setBorderWidth:1.0f];
    [[self layer] setBorderColor:self.tintColor.CGColor];
}

- (void)setTintColor:(UIColor *)tintColor {
    [super setTintColor:tintColor];
    [self redrawBounds];
}

- (void)setButtonTinted:(BOOL)boolean {
    tinted = boolean;
    if (tinted == true) {
        self.backgroundColor = self.tintColor;
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else {
        self.backgroundColor = [UIColor whiteColor];
        [self setTitleColor:self.tintColor forState:UIControlStateNormal];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
