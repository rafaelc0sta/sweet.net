//
//  AppDelegate.m
//  Sweet.net
//
//  Created by Rafael Costa on 11/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "AppDelegate.h"
#import "PDKeychainBindingsController/PDKeychainBindings.h"
#import "RCComposeViewController.h"
#import "RCPushStorage.h"
#import "LoginViewController.h"

#define defaultBlue [UIColor colorWithRed:0.f/255.f green:185.f/255.f blue:1.f alpha:1.f]

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeBadge |
     UIRemoteNotificationTypeAlert |
     UIRemoteNotificationTypeSound];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    self.window.backgroundColor = [UIColor clearColor];
    
    self.window.opaque = NO;
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UINavigationBar appearance] setBarStyle:UIBarStyleDefault];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [[UINavigationBar appearance] setBarTintColor:defaultBlue];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
//    [[UINavigationBar appearance] setBarTintColor:UIColor.whiteColor];
//    [[UINavigationBar appearance] setTintColor:defaultBlue];
//    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : defaultBlue}];
    
    if ([self.window respondsToSelector:@selector(setTintColor:)]) {
        //Set default tint color.
        [self.window setTintColor:defaultBlue];

    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isFirstBoot"] == nil) {
        
    }
    
    self.login = [[ADNLogin alloc] init];
    self.login.delegate = self;
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    
    return YES;
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken {
    NSString *deviceToken = [[[[newDeviceToken description] stringByReplacingOccurrencesOfString:@">" withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //Save to local singleton.
    //This might be useful later.
    [[RCPushStorage sharedPushStorage] setDeviceToken:deviceToken];
    if ([[RCUserStorage sharedUserStorage] user] != nil) {
        AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"https://rafaelc.com.br/push/devices/registerDevice.php"]];
        [client setParameterEncoding:AFJSONParameterEncoding];
        [client postPath:@"" parameters:@{@"username" : [[RCUserStorage sharedUserStorage] user], @"apiKey" : @"70daa50b2014", @"deviceToken" : [[RCPushStorage sharedPushStorage] deviceToken]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //Registered device.
            NSLog(@"SUCCESS");
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //Not registered.
            NSLog(@"Error: %@", error.description);
        }];
    }
    
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
    //Handle push
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    if ([url.absoluteString hasPrefix:@"adn1310:"]) {
        return [self.login openURL:url sourceApplication:sourceApplication annotation:annotation];
    } else if ([url.absoluteString hasPrefix:@"sweetdotnet:"]) {
        [self handleOpenURL:url];
        return YES;
    } else {
        return NO;
    }
}

- (void)handleOpenURL:(NSURL *)url {
    
    NSLog(@"URL: %@", url.absoluteString);
    NSString *action = url.host;
    NSString *property = url.pathComponents[1];
    NSLog(@"Action: %@", action);
    NSLog(@"Property: %@", property);
    if ([action isEqualToString:@"post"]) {
        LoginViewController *viewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateInitialViewController];
        if (property == nil) property = @"";
        NSLog(@"Will show Login with Payload");
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) [viewController setPayload:@{@"action": action, @"initialText" : property}];
        else if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
            [[[UIAlertView alloc] initWithTitle:@"Error. No URL scheme support on iOS 6 or less." message:nil delegate:nil cancelButtonTitle:@"Ok." otherButtonTitles: nil] show];
        }
        self.window.rootViewController = viewController;
    }
}

- (void)adnLoginDidSucceedForUserWithID:(NSString *)userID token:(NSString *)accessToken {
    // ... do some stuff
    [[PDKeychainBindings sharedKeychainBindings] setString:accessToken forKey:@"sweetdotnet:accessToken"];
    
    [[ANKClient sharedClient] setAccessToken:accessToken];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    LoginViewController *loginViewController = [storyboard instantiateInitialViewController];
    [loginViewController setLoggedFromADNApp:YES];
    // Set root view controller and make windows visible
    self.window.rootViewController = loginViewController;
    [self.window makeKeyAndVisible];
}

- (void)adnLoginDidFailWithError:(NSError *)error {
    if (error.code != 0) {
        UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"Failed." message:@"Login failed. Please try from within the app." delegate:nil cancelButtonTitle:@"Ok." otherButtonTitles: nil];
        [alerta show];
    }
}

- (void)application:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    if ([error code] == 3010) {
        NSLog(@"Push notifications don't work in the simulator!");
    } else {
        NSLog(@"didFailToRegisterForRemoteNotificationsWithError: %@", error.description);
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if ([self.window.rootViewController.presentedViewController isKindOfClass:[UITabBarController class]]) {
        if ([[(UITabBarController *)self.window.rootViewController.presentedViewController selectedViewController] isKindOfClass:[UINavigationController class]]) {
            [[(UINavigationController *)[(UITabBarController *)self.window.rootViewController.presentedViewController selectedViewController] visibleViewController] viewWillAppear:YES];
        } else {
            [[(UITabBarController *)self.window.rootViewController.presentedViewController selectedViewController] viewWillAppear:YES];
        }
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
