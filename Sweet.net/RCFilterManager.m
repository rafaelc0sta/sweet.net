//
//  RCFilterManager.m
//  Sweet.net
//
//  Created by Rafael Costa on 1/17/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import "RCFilterManager.h"
#import "ADNKit.h"

@implementation RCFilterManager

+ (instancetype)sharedFilterManager {
    static RCFilterManager *manager;
    @synchronized(self) {
        if (manager == nil) manager = [[RCFilterManager alloc] init];
        return manager;
    }
}

- (id)init {
    self.filters = [NSMutableArray new];
    return [super init];
}

- (void)saveFilters {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.filters];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"rc_sn_filers"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)loadFilters {
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"rc_sn_filers"];
    if (data != nil) self.filters = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    else self.filters = [NSArray new].mutableCopy;
}

- (BOOL)addFilter:(RCFilter *)filter {
    if (![filter isKindOfClass:[RCFilter class]]) return false;
    
    if (![self.filters containsObject:filter]) {
        [self.filters addObject:filter];
        return true;
    }
    return false;
}
- (BOOL)removeFilterWithID:(NSString *)filterID {
    for (RCFilter *filter in self.filters) {
        if ([filter.ID isEqualToString:filterID]) {
            return [self removeFilter:filter];
        }
    }
    return false;
}
- (BOOL)removeFilter:(RCFilter *)filter {
    if (![filter isKindOfClass:[RCFilter class]]) return false;
    
    if ([self.filters containsObject:filter]) {
        [self.filters removeObject:filter];
        return true;
    }
    return false;
}

- (NSArray *)applyFiltersToArray:(NSArray *)filterees {
    NSMutableArray *filtereeStart = filterees.mutableCopy;
    for (id filteree in filterees) {
        for (id filter in self.filters) {
            BOOL shouldRemove = [filter shouldRemovePost:filteree];
            if (shouldRemove) {
                [filtereeStart removeObject:filteree];
                break;
            }
        }
    }
    
    return filtereeStart.copy;
}

@end
