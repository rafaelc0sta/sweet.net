//
//  RCFilter.h
//  Sweet.net
//
//  Created by Rafael Costa on 1/17/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADNKit.h"

//Defines the search option:
typedef enum {
    RCFilterCompareOptionContains,
    RCFilterCompareOptionEquals
} RCFilterCompareOption;

@interface RCFilter : NSObject
@property (nonatomic) RCFilterCompareOption searchOption;
@property (nonatomic) NSString *query;
@property (nonatomic) NSString *ID;

- (BOOL)shouldRemovePost:(id)filteree;
@end
