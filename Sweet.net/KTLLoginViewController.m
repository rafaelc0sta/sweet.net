//
//  KTLLoginViewController.m
//  LoginViewController
//
//  Created by Steven Baranski on 1/2/13.
//  Copyright (c) 2013 komorka technology, llc. All rights reserved.
//

#import "KTLLoginViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ADNKit.h"
#import "RCKit.h"
#import "PDKeychainBindings.h"
#import "AppDelegate.h"

@interface KTLLoginViewController ()

<UITextFieldDelegate>

@property (nonatomic, strong) UIBarButtonItem *loginBarButtonItem;

@property (nonatomic, strong) UITextField *usernameTextField;
@property (nonatomic, strong) UITextField *passwordTextField;

- (IBAction)loginTapped:(id)sender;

- (void)setupView;
- (void)updateView;
- (void)updateLoginButton;

@end

enum KTLLoginTableSections {
    KTLLoginSection = 0,
    KTLLoginTableSectionCount,
};

enum KTLLoginRows {
    KTLLoginSectionRowUsername = 0,
    KTLLoginSectionRowPassword,
    KTLLoginSectionRowCount,
};

@implementation KTLLoginViewController

// NB: these magic numbers should be addressed
static CGFloat KTLLoginTextFieldWidth = 180.0f;
static CGFloat KTLLoginTextFieldHeight = 44.0f;

static NSString *KTLCellReuseIdentifier = @"Cell";

#pragma mark - Initialization

- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self)
    {
        self.title = @"Add an App.Net account";

        // TODO: localize me
        
//        load = [[LoadingViewController alloc]initWithNibName:@"LoadingViewController" bundle:nil];
//        [[load.view layer] setCornerRadius:7.5f];
//        [[load.view layer] setMasksToBounds:YES];
    }
    return self;
}

- (void)openLoginInADNApp {
    [[(AppDelegate *)[[UIApplication sharedApplication] delegate] login] loginWithScopes:@[@"basic", @"stream", @"write_post", @"follow", @"public_messages", @"messages", @"update_profile", @"files"]];
}

#pragma mark - UIViewController lifecycle

- (void)didReceiveMemoryWarning
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.hasLoggedFromApp == YES) [self fetchCurrentUserAndFinish];
    [self setupView];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.numberOfSections = 2;
    if ([[(AppDelegate *)[[UIApplication sharedApplication] delegate] login] isLoginAvailable]) {
        self.numberOfSections = 3;
    }
    
    [self.tableView reloadData];
    [super viewWillAppear:animated];
    [self updateView];
}

- (void)showPrivacyPolicy {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://rafaelc.com.br/privacy"]];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - UITableViewCell

- (void)updateCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.section == KTLLoginSection) {
        switch (indexPath.row)
        {
            case KTLLoginSectionRowUsername :
            {
                cell.textLabel.text = @"Username";              // TODO: localize me
                cell.accessoryView = self.usernameTextField;
                return;
            }
            case KTLLoginSectionRowPassword :
            {
                cell.textLabel.text = @"Password";              // TODO: localize me
                cell.accessoryView = self.passwordTextField;
                return;
            }
            default :
                NSLog(@"%s - unintelligible row %i", __PRETTY_FUNCTION__, indexPath.row);
                break;
        }
    } else if (indexPath.section == 1) {
        if (self.numberOfSections == 3) {
            cell.textLabel.text = @"Login with App.net Passport";
            return;
        } else if (self.numberOfSections == 2) {
            cell.textLabel.text = @"Privacy Policy";
            return;
        }
    } else if (indexPath.section == 2) {
        cell.textLabel.text = @"Privacy Policy";
        return;
    }
    
//    switch (indexPath.section)
//    {
//        case KTLLoginSection:
//        {
//            
//        } case 1:
//            cell.textLabel.text = @"Login with App.net Passport";
//            return;
//        default :
//            NSLog(@"%s - unintelligible section %i", __PRETTY_FUNCTION__, indexPath.section);
//            break;
//    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section)
    {
        case KTLLoginSection:
            return KTLLoginSectionRowCount;
        case 1:
            return 1;
        default :
            return 1;
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:KTLCellReuseIdentifier
                                                            forIndexPath:indexPath];
    [self updateCell:cell forRowAtIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1) {
        if (self.numberOfSections == 3) [self openLoginInADNApp];
        else if (self.numberOfSections == 2) [self showPrivacyPolicy];
    } else if (indexPath.section == 2) {
        [self showPrivacyPolicy];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self updateLoginButton];
    }];
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.usernameTextField)
    {
        [self.passwordTextField becomeFirstResponder];
    }
    else if (textField == self.passwordTextField)
    {
        [self updateLoginButton];
        if (YES == self.loginBarButtonItem.enabled)
        {
            [self loginTapped:textField];
        }
    }
	return NO;
}

#pragma mark - IBActions

- (void)fetchCurrentUserAndFinish {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
    [progressHUD setYOffset:-50];
    [[ANKClient sharedClient] fetchCurrentUserWithCompletion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
        if (error == nil) {
            ANKUser *user = responseObject;
            [self.delegate loginController:self didAddAccountWithUserDictionary:[user JSONDictionary]];
            [MBProgressHUD hideHUDForView:appWindow animated:YES];
            
        } else {
            [MBProgressHUD hideHUDForView:appWindow animated:YES];
            UIAlertView *alertaErro = [[UIAlertView alloc]initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok." otherButtonTitles: nil];
            [alertaErro show];
        }
    }];
}

- (IBAction)loginTapped:(id)sender
{
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
    [progressHUD setYOffset:-50];
    
    ANKAuthScope scopes = ANKAuthScopeBasic | ANKAuthScopeEmail | ANKAuthScopeFiles | ANKAuthScopeFollow |
    ANKAuthScopeMessages | ANKAuthScopePublicMessages | ANKAuthScopeStream | ANKAuthScopeUpdateProfile | ANKAuthScopeWritePost;
    [[ANKClient sharedClient] authenticateUsername:self.usernameTextField.text password:self.passwordTextField.text clientID:@"u6TeYE9yYprhkWPjVNgacJLngkhVrDFw" passwordGrantSecret:@"mmZvDH3aCb8WaDnLZKJjPtEXQpD4XQQx" authScopes:scopes completionHandler:^(BOOL success, NSError *error) {
        if (success) {
            if (error == nil) {
                [MBProgressHUD hideHUDForView:appWindow animated:NO];
                [self fetchCurrentUserAndFinish];
            } else {
                [MBProgressHUD hideHUDForView:appWindow animated:YES];
                UIAlertView *alertaErro = [[UIAlertView alloc]initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok." otherButtonTitles: nil];
                [alertaErro show];
            }
        } else {
            [MBProgressHUD hideHUDForView:appWindow animated:YES];
            UIAlertView *alertaErro = [[UIAlertView alloc]initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok." otherButtonTitles: nil];
            [alertaErro show];
        }
    
}];

}

#pragma mark - Private behavior

- (void)setupView
{
    // navigation item
    self.loginBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Add"   // TODO: localize me
                                                               style:UIBarButtonItemStyleDone
                                                              target:self
                                                              action:@selector(loginTapped:)];
    
    
    self.navigationItem.rightBarButtonItem = self.loginBarButtonItem;
    
    // table
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2
                                                   reuseIdentifier:KTLCellReuseIdentifier];
    //[self.tableView setBackgroundColor:[UIColor scrollViewTexturedBackgroundColor]];
    [self.tableView registerClass:[cell class]
           forCellReuseIdentifier:KTLCellReuseIdentifier];

    
    // text fields
    UITextField *userField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, KTLLoginTextFieldWidth, KTLLoginTextFieldHeight)];
    userField.keyboardType = UIKeyboardTypeEmailAddress;
    userField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    userField.autocorrectionType = UITextAutocorrectionTypeNo;
    userField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    userField.delegate = self;
    userField.returnKeyType = UIReturnKeyNext;
    userField.placeholder = @"Enter username";      // TODO: localize me
    self.usernameTextField = userField;
    
    UITextField *pwdField = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, KTLLoginTextFieldWidth, KTLLoginTextFieldHeight)];
    pwdField.secureTextEntry = YES;
    pwdField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    pwdField.delegate = self;
    pwdField.returnKeyType = UIReturnKeyGo;
    pwdField.placeholder = @"Required";             // TODO: localize me
    self.passwordTextField = pwdField;
}

- (void)updateView
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self.usernameTextField becomeFirstResponder];
    }];
    [self.tableView setBackgroundView:nil];
    //[self.tableView setBackgroundColor:[UIColor scrollViewTexturedBackgroundColor]];
    [self updateLoginButton];
}

- (void)updateLoginButton
{
    // NB: contrived username & password policy
    BOOL usernameIsCompliant = ([self.usernameTextField.text length] > 0);
    BOOL passwordIsCompliant = ([self.passwordTextField.text length] > 0);
    
    self.loginBarButtonItem.enabled = (usernameIsCompliant && passwordIsCompliant);
}

@end
