//
//  SNTabBarController.m
//  SweetDotNet
//
//  Created by Rafael Costa on 13/08/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "SNTabBarController.h"

#define deviceHeight [[UIScreen mainScreen] bounds].size.height

#define DEFAULT_ANIM_DURATION 0.35

@interface SNTabBarController ()

@end

@implementation SNTabBarController

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    first = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"TimelineViewController"];
    second = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"MentionsViewController"];
    third = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"MessagesListViewController"];
    fourth = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"UserViewController"];
    
    
    tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePopUp)];
    //[tapGestureRecognizer setNumberOfTapsRequired:1];
    [tapGestureRecognizer setNumberOfTouchesRequired:1];
    [tapGestureRecognizer setEnabled:NO];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    
    
//    fourth = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"GlobalStreamViewController"];
//    fifth = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"UserViewController"];
    
    nav1 = [[UINavigationController alloc] initWithRootViewController:first];
    nav2 = [[UINavigationController alloc] initWithRootViewController:second];
    nav3 = [[UINavigationController alloc] initWithRootViewController:third];
    nav4 = [[UINavigationController alloc] initWithRootViewController:fourth];
    
//    nav5 = [[UINavigationController alloc] initWithRootViewController:fifth];
    
    
    [self addChildViewController:nav1];
    [self addChildViewController:nav2];
    [self addChildViewController:nav3];
    [self addChildViewController:nav4];
    
    _selectedIndex = 0;
	// Do any additional setup after loading the view.
}

- (void)viewDidLayoutSubviews {
    if (!firstInstatiated) {
        [self setNavControllersFrames];
        [mainContainer addSubview:[nav1 view]];
        [nav1.view layoutSubviews];
        [nav1 didMoveToParentViewController:self];
        firstInstatiated = TRUE;
        
        NSArray *titles = @[@"Timeline", @"Mentions", @"Messages", @"My Profile"];
        popUpView_main = [[SNPopUpView alloc] initWithFrame:CGRectMake(20, button_popUp.frame.origin.y - (titles.count * 50), 0, 0) withTitles:titles];
        [popUpView_main setDelegate:self];
        [self.view addSubview:popUpView_main];
                            [popUpView_main setHidden:true];
        
        firstButtonRect = button_popUp.frame;
        [self.view layoutIfNeeded];
    }
}

- (void)setNavControllersFrames {
    [nav1.view setFrame:[mainContainer frame]];
    [nav2.view setFrame:[mainContainer frame]];
    [nav3.view setFrame:[mainContainer frame]];
    [nav4.view setFrame:[mainContainer frame]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[button_popUp layer] setCornerRadius:24];
    [button_popUp setTransform:CGAffineTransformMakeRotation(M_PI)];
    
}

- (IBAction)changePopUpState:(id)sender {
    if (popUpView_main.hidden == YES) {
        [self showPopUp];
    } else {
        [self hidePopUp];
    }
}

- (void)hidePopUp {
    if (popUpView_main.hidden == NO) {
        popUpView_main.alpha = 1.f;
        [UIView animateWithDuration:DEFAULT_ANIM_DURATION animations:^{
            popUpView_main.alpha = 0.f;
            [button_popUp setTransform:CGAffineTransformMakeRotation(M_PI - 0.001)];
        } completion:^(BOOL finished) {
            if (finished) {
                [button_popUp setTransform:CGAffineTransformMakeRotation(M_PI)];
                popUpView_main.hidden = YES;
                [tapGestureRecognizer setEnabled:NO];
                [mainContainer setUserInteractionEnabled:YES];
            }
        }];
    }
}

- (void)hidePopUpButton {
    if (button_popUp.hidden == NO) {
        CGRect frame = button_popUp.frame;
        frame.origin.y = frame.origin.y - 10;
        [UIView animateWithDuration:DEFAULT_ANIM_DURATION/2 animations:^{
            button_popUp.frame = frame;
        } completion:^(BOOL finished) {
            if (finished) {
                CGRect finalRect = frame;
                finalRect.origin.y = self.view.frame.size.height + 1;
                [UIView animateWithDuration:DEFAULT_ANIM_DURATION/2 animations:^{
                    button_popUp.frame = finalRect;
                } completion:^(BOOL finished) {
                    if (finished) {
                        button_popUp.hidden = YES;
                    }
                }];
            }
        }];
    }
}

- (void)showPopUpButton {
    if (button_popUp.hidden == YES) {
        button_popUp.hidden = NO;
        CGRect frame = firstButtonRect;
        frame.origin.y = frame.origin.y - 10;
        [UIView animateWithDuration:DEFAULT_ANIM_DURATION/2 animations:^{
            button_popUp.frame = frame;
        } completion:^(BOOL finished) {
            if (finished) {
                CGRect finalRect = firstButtonRect;
                [UIView animateWithDuration:DEFAULT_ANIM_DURATION/2 animations:^{
                    button_popUp.frame = finalRect;
                }];
            }
        }];
    }
}

- (void)showPopUp {
    if (popUpView_main.hidden == YES) {
        
        for (id view in [[[[[[[[mainContainer.subviews objectAtIndex:0] subviews] objectAtIndex:0] subviews] objectAtIndex:0] subviews] objectAtIndex:0] subviews]) {
            if ([view isKindOfClass:[UITableView class]]) {
                UIScrollView *scrollView = (UIScrollView *)view;
                [(UIScrollView *)view setContentOffset:scrollView.contentOffset animated:NO];
            }
        }
        
        popUpView_main.hidden = NO;
        popUpView_main.alpha = 0.f;
        [UIView animateWithDuration:DEFAULT_ANIM_DURATION animations:^{
            [button_popUp setTransform:CGAffineTransformMakeRotation(0)];
            popUpView_main.alpha = 1.f;
        } completion:^(BOOL finished) {
            if (!finished) return;
        }];
        [tapGestureRecognizer setEnabled:YES];
        [mainContainer setUserInteractionEnabled:NO];
    }
}

- (IBAction)selectedButton:(int)index {
    
    int pressedIndex = index;
    
    if (_selectedIndex == pressedIndex) return;
    
    for (UIView *view in mainContainer.subviews) {
        [view removeFromSuperview];
    }
    
    [popUpView_main setSelectedIndex:index];
    
    if (pressedIndex == 0) {
        [mainContainer addSubview:[nav1 view]];
        [nav1.view layoutSubviews];
        [nav1 didMoveToParentViewController:self];
    } else if (pressedIndex == 1) {
        [mainContainer addSubview:[nav2 view]];
        [nav2.view layoutSubviews];
        [nav2 didMoveToParentViewController:self];
    } else if (pressedIndex == 2){
        [mainContainer addSubview:[nav3 view]];
        [nav3.view layoutSubviews];
        [nav3 didMoveToParentViewController:self];
    } else if (pressedIndex == 3) {
        [mainContainer addSubview:[nav4 view]];
        [nav4.view layoutSubviews];
        [nav4 didMoveToParentViewController:self];
    }
    [self hidePopUp];
    _selectedIndex = pressedIndex;
    
}

- (void)popUpView:(SNPopUpView *)popUpView didSelectItemAtIndex:(int)selectedIndex {
    if (popUpView == popUpView_main) {
        [self selectedButton:selectedIndex];
    }
}

- (void)hideBottomBar {
    //Keep compatibility
    [self hidePopUp];
    [self hidePopUpButton];
}

- (void)showBottomBar {
    //Keep compatibility
    [self showPopUpButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
