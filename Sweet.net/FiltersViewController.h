//
//  FiltersViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 1/16/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FiltersViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UITableView *tableView_filters;
}
- (IBAction)addFilter:(id)sender;
- (IBAction)done:(id)sender;
@end
