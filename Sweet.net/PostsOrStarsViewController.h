//
//  PostsOrStarsViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 26/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADNKit.h"
#import "RCKit.h"
#import "JLActionSheet.h"
typedef enum {
    PostsViewTypePosts,
    PostsViewTypeStars
} PostsViewType;
@interface PostsOrStarsViewController : UIViewController <UIActionSheetDelegate, RCPostCellDelegate, UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UITableView *tableView_posts;
    UIRefreshControl *refreshControl;
    NSMutableArray *array_posts;
    
    UIView *maskView;
    UIView *maskView_navController;
    
    JLActionSheet *actionSheet_repost;
    
    NSString *lastPostID;
}
@property (nonatomic) PostsViewType viewType;
@property (nonatomic) NSString *userId;
@end
