//
//  DescriptionEditorViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 11/07/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "DescriptionEditorViewController.h"

@interface DescriptionEditorViewController ()

@end

@implementation DescriptionEditorViewController

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

- (IBAction)save:(id)sender {
    if (![textView_bio.text isEqualToString:self.currentBio]) {
        [presentingViewController setHasChangedProfileBio:YES];
    } else {
        [presentingViewController setHasChangedProfileBio:NO];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        if (presentingViewController.hasChangedProfileBio) [presentingViewController modifyBioToBeSent:textView_bio.text];
    }];
}
- (IBAction)cancel:(id)sender {
    [presentingViewController setHasChangedProfileBio:NO];
    [self dismissViewControllerAnimated:YES completion:^{
    
    }];
}

- (void)postPresentationUpdates {
    [textView_bio setText:self.currentBio];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    presentingViewController = (EditProfileViewController *)self.presentingViewController;
    [textView_bio setDelegate:self];
    [textView_bio becomeFirstResponder];
    [self postPresentationUpdates];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
