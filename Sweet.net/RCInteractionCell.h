//
//  RCInteractionCell.h
//  Sweet.net
//
//  Created by Rafael Costa on 11/07/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UILabel+VerticalAlign.h"

#import "ADNKit.h"
#import "RCKit.h"

@interface RCInteractionCell : UITableViewCell
@property (nonatomic) IBOutlet UILabel *label_longAgo;
@property (nonatomic) IBOutlet UILabel *label_interaction;
@property (nonatomic) IBOutlet UIImageView *imageView_userImage;
@property (nonatomic) ANKInteraction *interaction;
@property (nonatomic) NSIndexPath *indexPath;

@end
