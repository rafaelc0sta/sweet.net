//
//  RCTokenStorage.m
//  Sweet.net
//
//  Created by Rafael Costa on 12/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "RCUserStorage.h"

@implementation RCUserStorage
+ (instancetype)sharedUserStorage {
    @synchronized(self) {
        static RCUserStorage *userStorage;
        if (userStorage == nil) {
            userStorage = [[self alloc] init];
        }
        return userStorage;
    }
}
@end
