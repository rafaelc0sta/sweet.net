//
//  RCTabBarController.h
//  Sweet.net
//
//  Created by Rafael Costa on 11/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface RCTabBarController : UITabBarController {
    UITabBarItem *lastSelectedItem;
}
@end
