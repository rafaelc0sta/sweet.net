//
//  RCMapPin.m
//  Sweet.net
//
//  Created by Rafael Costa on 09/05/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "RCMapPin.h"

@implementation RCMapPin
@synthesize coordinate;
@synthesize title;
@synthesize subtitle;

- (id)initWithCoordinates:(CLLocationCoordinate2D)location
                placeName:(NSString *)placeName
              description:(NSString *)description;
{
    self = [super init];
    if (self)
    {
        coordinate = location;
        title = placeName;
        subtitle = description;
    }
    
    return self;
}

@end
