//
//  SettingsViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 17/04/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "SettingsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

- (IBAction)clearAutocompleteCache:(id)sender {
    [[RCUserListStorage sharedInstance] clearUserDB];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
    [hud setMode:MBProgressHUDModeText];
    [hud setLabelText:@"Cleared"];
    [hud hide:YES afterDelay:1.5];
}

- (void)updateSwitchers {
    [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"https://rafaelc.com.br/push/api/checkUser.php"]];
    NSMutableDictionary *jsonParameters = [NSMutableDictionary new];
    [jsonParameters setObject:[[RCUserStorage sharedUserStorage] user] forKey:@"username"];
    [client setParameterEncoding:AFJSONParameterEncoding];
    [client postPath:@"" parameters:jsonParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //User may get push.
        switch_push.on = TRUE;
        pushEnabled = true;
        
        [MBProgressHUD hideAllHUDsForView:appWindow animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //Failed to log user on push_registration server.
        //User may not get push.
        switch_push.on = FALSE;
        pushEnabled = false;
        [MBProgressHUD hideAllHUDsForView:appWindow animated:YES];
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    if(types == UIRemoteNotificationTypeNone)
    {
        label_none.hidden = false;
        label_none.text = @"Push is disabled in the Settings app. Please, enable and come back.";
        view_pushSettings.hidden = true;
    }
    else
    {
        label_none.hidden = true;
        view_pushSettings.hidden = false;
    }
    
    [self updateSwitchers];
    shakeCount = 0;
}

- (IBAction)changePush:(id)sender {
    
    [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
    
    if (pushEnabled == TRUE) {
        AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"https://rafaelc.com.br/push/api/rmvUser.php"]];
        NSMutableDictionary *jsonParameters = [NSMutableDictionary new];
        [jsonParameters setObject:[[RCUserStorage sharedUserStorage] user] forKey:@"username"];
        [jsonParameters setObject:@"70daa50b2014" forKey:@"apiKey"];
        [client setParameterEncoding:AFJSONParameterEncoding];
        [client postPath:@"" parameters:jsonParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            pushEnabled = FALSE;
            [switch_push setOn:pushEnabled];
            [MBProgressHUD hideAllHUDsForView:appWindow animated:YES];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            pushEnabled = TRUE;
            [switch_push setOn:pushEnabled];
            [MBProgressHUD hideAllHUDsForView:appWindow animated:YES];
            NSLog(@"Error: %@", error.description);
        }];
    } else if (pushEnabled == FALSE) {
        AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"https://rafaelc.com.br/push/api/putUser.php"]];
        NSMutableDictionary *jsonParameters = [NSMutableDictionary new];
        [jsonParameters setObject:[[RCUserStorage sharedUserStorage] user] forKey:@"username"];
        [jsonParameters setObject:@"70daa50b2014" forKey:@"apiKey"];
        [client setParameterEncoding:AFJSONParameterEncoding];
        [client postPath:@"" parameters:jsonParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            pushEnabled = TRUE;
            [switch_push setOn:pushEnabled];
            [MBProgressHUD hideAllHUDsForView:appWindow animated:YES];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            pushEnabled = FALSE;
            [switch_push setOn:pushEnabled];
            [MBProgressHUD hideAllHUDsForView:appWindow animated:YES];
            NSLog(@"Error: %@", error.description);
        }];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateSwitchers];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnteredForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    // Do any additional setup after loading the view.
    [self becomeFirstResponder];
}

- (void)applicationEnteredForeground:(NSNotification *)note {
    [self viewWillAppear:NO];
}

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (event.type == UIEventSubtypeMotionShake) {
        [self updateShakeCount];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
        hud.mode = MBProgressHUDModeText;
        NSUInteger remainingShakes = 5 - shakeCount;
        hud.labelText = [NSString stringWithFormat:@"%i more time(s) for easter", remainingShakes];
        [hud hide:YES afterDelay:.8];
        [self becomeFirstResponder];
    }
}

- (void)updateShakeCount {
    shakeCount = shakeCount + 1;
    if (shakeCount == 5) {
         [self performSegueWithIdentifier:@"developerArea" sender:self];
    }
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (IBAction)done:(id)sender {
    
    [self.presentingViewController viewWillAppear:YES];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
