//
//  GlobalStreamViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 17/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "GlobalStreamViewController.h"
#import "SVPullToRefresh.h"
#import "PostDetailViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import "SearchViewController.h"
#import "PostConversationDetailsViewController.h"

@interface GlobalStreamViewController ()

@end

@implementation GlobalStreamViewController

- (IBAction)openSearch:(id)sender {
    
    SearchViewController *searchViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"SearchViewController"];
    UINavigationController *navigationControllerSearch = [[UINavigationController alloc] initWithRootViewController:searchViewController];
    [self presentViewController:navigationControllerSearch animated:YES completion:nil];
}

- (IBAction)compose:(id)sender {
    if (sender != self) 
    [self replyToPost:nil];
}

- (void)repostWithQuote:(ANKPost *)post {
    RCComposeViewController *cpVC = [[RCComposeViewController alloc]initWithNibName:@"RCComposeViewController" bundle:[NSBundle mainBundle]];
    
    if (post != nil) cpVC.navTitle = @"Quote";
    
    [cpVC setInitialText:[NSString stringWithFormat:@" >> @%@: %@", post.user.username, post.text]];
    [cpVC setPostToReply:post];
    
    [self presentViewController:cpVC animated:YES completion:^(void){
        
    }];
}


- (void)replyToPost:(ANKPost *)post {
    RCComposeViewController *cpVC = [[RCComposeViewController alloc]initWithNibName:@"RCComposeViewController" bundle:[NSBundle mainBundle]];
    
    [cpVC setPostToReply:post];
    
    if (post != nil) cpVC.navTitle = @"Reply";
    
    [self presentViewController:cpVC animated:YES completion:^(void){
        
    }];
}

- (void)starPost:(ANKPost *)post1 {
    if (post1.isStarredByCurrentUser == TRUE) {
        [[ANKClient sharedClient] unstarPost:post1 completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Unstarred";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                int i;
                for (i = 0; i <= [array_posts count]; i++) {
                    RCPostCell *postCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    if ([postCell.post.postID isEqualToString:post1.postID]) {
                        [postCell setStarredButtonOn:FALSE];
                        post1.isStarredByCurrentUser = !post1.isStarredByCurrentUser;
                    }
                }
            }

        }];
    } else {
        [[ANKClient sharedClient] starPost:post1 completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Starred";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                int i;
                for (i = 0; i <= [array_posts count]; i++) {
                    RCPostCell *postCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    if ([postCell.post.postID isEqualToString:post1.postID]) {
                        [postCell setStarredButtonOn:TRUE];
                        post1.isStarredByCurrentUser = !post1.isStarredByCurrentUser;
                    }
                }
            }
        }];
    }
}

- (void)threadForPost:(ANKPost *)post1 {
    if (post1.repliesCount > 0 || post1.repliedToPostID != nil) {
        PostConversationDetailsViewController *pcdvc = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"PostConversationDetailsViewController"];
        [pcdvc setPost:post1];
        [[self navigationController] pushViewController:pcdvc animated:YES];
    } else {
        MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
        HUD.mode = MBProgressHUDModeText;
        [HUD setLabelText:@"Thread is empty"];
        [HUD hide:YES afterDelay:1.5];
    }
}

- (void)repostPost:(ANKPost *)post1 {
    if (post1.isRepostedByCurrentUser == TRUE) {
        [[ANKClient sharedClient] unrepostPost:post1 completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Unreposted";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];                int i;
                for (i = 0; i <= [array_posts count]; i++) {
                    RCPostCell *postCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    if ([postCell.post.postID isEqualToString:post1.postID]) {
                        [postCell setRepostedButtonOn:FALSE];
                        post1.isRepostedByCurrentUser = !post1.isRepostedByCurrentUser;
                    }
                }
            }
        }];
    } else {
        [[ANKClient sharedClient] repostPost:post1 completion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
            if (error == nil) {
                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
                [appWindow addSubview:HUD];
                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                HUD.mode = MBProgressHUDModeCustomView;
                HUD.labelText = @"Reposted";
                [HUD show:YES];
                [HUD hide:YES afterDelay:1.5];
                int i;
                for (i = 0; i <= [array_posts count]; i++) {
                    RCPostCell *postCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    if ([postCell.post.postID isEqualToString:post1.postID]) {
                        [postCell setRepostedButtonOn:TRUE];
                        post1.isRepostedByCurrentUser = !post1.isRepostedByCurrentUser;
                    }
                }
            }
        }];
    }

}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    lastPostID = @"0";

    UIBarButtonItem *barButtonItemCompose = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(compose:)];
    self.navigationItem.rightBarButtonItem = barButtonItemCompose;

    [tableView_posts registerNib:[UINib nibWithNibName:@"RCPostCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    array_posts = [[NSMutableArray alloc]init];
    
    tableView_posts.showsInfiniteScrolling = NO;
    
    [self reloadPosts];
    
    
    [tableView_posts addInfiniteScrollingWithActionHandler:^{
        [self reloadMorePosts];
    }];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadPosts) forControlEvents:UIControlEventValueChanged];
    [tableView_posts addSubview:refreshControl];

	// Do any additional setup after loading the view.
}

- (void)hideOptions:(NSNotification *)note {
    int nowIndex = 0;
    for (nowIndex = 0; nowIndex <= array_posts.count; nowIndex++) {
        RCPostCell *cell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:nowIndex inSection:0]];
        if (cell != [[note userInfo] objectForKey:@"cell"]) [cell hideMiniOptions];
    }
}

- (void)reloadPosts {
    ANKPaginationSettings *paginationSettings = [ANKPaginationSettings settingsWithCount:100];
    [[[ANKClient sharedClient] clientWithPagination:paginationSettings] fetchGlobalStreamWithCompletion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
        if (error == nil) {
            NSMutableArray *posts = responseObject;
            
            
            NSMutableArray *nilPosts = @[].mutableCopy;
            for (ANKPost *post in posts) {
                if (post == nil || post.text == nil || post.user == nil) {
                    [nilPosts addObject:post];
                }
            }
            [posts removeObjectsInArray:nilPosts.copy];
            
            
            NSMutableArray *mutableArrayNewItems = [[NSMutableArray alloc] init];
            if (array_posts.count > 0 && array_posts != nil) {
                for (ANKPost *post in posts) {
                    if (post.postID.integerValue > [[array_posts objectAtIndex:0] postID].integerValue) [mutableArrayNewItems addObject:post];
                }
            } else {
                mutableArrayNewItems = [posts mutableCopy];
            }
            
            
            //FILTER
            array_posts = [[RCFilterManager sharedFilterManager] applyFiltersToArray:array_posts].mutableCopy;
            mutableArrayNewItems = [[RCFilterManager sharedFilterManager] applyFiltersToArray:mutableArrayNewItems].mutableCopy;
            //--FILTER
            
            BOOL shouldScroll;
            if (array_posts.count > 0 && array_posts != nil) {
                //We already had items from the array. Let's just add the new ones!
                shouldScroll = TRUE;
                array_posts = [[mutableArrayNewItems arrayByAddingObjectsFromArray:array_posts] mutableCopy];
            }
            else {
                shouldScroll = FALSE;
                array_posts = [mutableArrayNewItems mutableCopy];
            }
            
            tableView_posts.showsInfiniteScrolling = NO;
            if (meta.moreDataAvailable) tableView_posts.showsInfiniteScrolling = YES;
            
            [tableView_posts.infiniteScrollingView stopAnimating];
            lastPostID = meta.minID;
            
            //Saving the offset. MAJIK TRYK!
            CGPoint offset = [tableView_posts contentOffset];
            //Reload the data, this way, the offset is reset.
            [tableView_posts reloadData];
            //Set a default value for the row size. Don't worry, will be fixed in the l00p.
            CGFloat newRowsSize = 0.f;
            //That's the l00p I was talking 'bout.
            int index = 0;
            for (index = 0; index < mutableArrayNewItems.count; index++) {
                //See the MAJIK?
                newRowsSize += [self tableView:tableView_posts heightForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            }
            //And the finale!
            offset.y += newRowsSize;
            if (offset.y > [tableView_posts contentSize].height) {
                offset.y = 0;
            }
            //Guess what? We only scroll if necessary!
            if (shouldScroll == TRUE) [tableView_posts setContentOffset:offset];
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
            [appWindow addSubview:HUD];
            HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
            HUD.mode = MBProgressHUDModeCustomView;
            HUD.labelText = @"Reloaded";
            [HUD show:YES];
            [HUD hide:YES afterDelay:1.5];
            [refreshControl endRefreshing];
        } else {
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
            [appWindow addSubview:HUD];
            HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
            HUD.mode = MBProgressHUDModeCustomView;
            HUD.labelText = @"Error loading posts";
            [HUD show:YES];
            [HUD hide:YES afterDelay:1.5];
            [refreshControl endRefreshing];
            [tableView_posts.infiniteScrollingView stopAnimating];
        }
    }];
}

- (void)reloadMorePosts {
    ANKPaginationSettings *paginationSettings = [ANKPaginationSettings settingsWithCount:100];
    [paginationSettings setBeforeID:lastPostID];
    
    //CONSTANT
    [paginationSettings setSinceID:@"0"];
    [[[ANKClient sharedClient] clientWithPagination:paginationSettings] fetchGlobalStreamWithCompletion:^(id responseObject, ANKAPIResponseMeta *meta, NSError *error) {
        if (error == nil) {
            NSMutableArray *posts = responseObject;
            
            
            NSMutableArray *nilPosts = @[].mutableCopy;
            for (ANKPost *post in posts) {
                if (post == nil || post.text == nil || post.user == nil) {
                    [nilPosts addObject:post];
                }
            }
            [posts removeObjectsInArray:nilPosts.copy];
            
            if (array_posts.count > 0) array_posts = [[array_posts arrayByAddingObjectsFromArray:posts] mutableCopy];
            else array_posts = [posts mutableCopy];
            [tableView_posts.infiniteScrollingView stopAnimating];
            lastPostID = meta.minID;
            
            //FILTER
            array_posts = [[RCFilterManager sharedFilterManager] applyFiltersToArray:array_posts].mutableCopy;
            //--FILTER
            
            [tableView_posts reloadData];
            [refreshControl endRefreshing];
        } else {
            MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
            [appWindow addSubview:HUD];
            HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
            HUD.mode = MBProgressHUDModeCustomView;
            HUD.labelText = @"Error loading posts";
            [HUD show:YES];
            [HUD hide:YES afterDelay:1.5];
            [refreshControl endRefreshing];
            [tableView_posts.infiniteScrollingView stopAnimating];
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidAppear:(BOOL)animated {
    [self.navigationController.navigationBar.topItem setTitle:self.title];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [refreshControl endRefreshing];
    
    
    
    for (RCComposeViewController *VC in [self childViewControllers]) {
        [VC.view removeFromSuperview];
        [VC removeFromParentViewController];
    }
    for (UIView *view in [self.view subviews]) {
        if (view.backgroundColor == [UIColor blackColor] && view.alpha == .7f) [view removeFromSuperview];
    }
    for (UIView *view in [self.navigationController.view subviews]) {
        if (view.backgroundColor == [UIColor blackColor] && view.alpha == .7f) [view removeFromSuperview];
    }
    
    if (array_posts.count > 0) {
        CGPoint offset = tableView_posts.contentOffset;
        NSMutableArray *array_itemsToRemove = @[].mutableCopy;
        for (ANKPost *post in array_posts) {
            if (post.isDeleted) [array_itemsToRemove addObject:post];
        }
        [array_posts removeObjectsInArray:array_itemsToRemove.copy];
        [tableView_posts reloadData];
        [tableView_posts setContentOffset:offset];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    int nowIndex = 0;
    for (nowIndex = 0; nowIndex <= array_posts.count; nowIndex = nowIndex + 1) {
        RCPostCell *currentCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:nowIndex inSection:0]];
        [currentCell hideMiniOptions];
    }
}

- (void)cell:(RCPostCell *)cell wantsTo:(RCPostCellAction)action {
    if (action == RCPostCellActionReply) {
        [self replyToPost:cell.post];
    } else if (action == RCPostCellActionRepost) {
        NSString *stringRepostButton = @"Repost";
        if ([cell.post isRepostedByCurrentUser]) stringRepostButton = @"Unrepost";
        //actionSheet_repost = [[RCActionSheet alloc] initWithTitle:@"Repost options:" post:cell.post delegate:self otherButtonTitles:@[stringRepostButton, @"Repost with comment", @"Cancel"]];
        actionSheet_repost = [[JLActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@[stringRepostButton, @"Repost with comment"]];
        [actionSheet_repost setStyle:JLSTYLE_SUPERCLEAN];
        [actionSheet_repost setClickedButtonBlock:^(JLActionSheet *actionSheet, NSInteger clickedButtonIndex) {
            if (clickedButtonIndex == 2) {
                //Repost
                [self repostPost:cell.post];
            } else if (clickedButtonIndex == 1) {
                [self repostWithQuote:cell.post];
            }
        }];
        [actionSheet_repost showInView:self.view];
    } else if (action == RCPostCellActionStar) {
        [self starPost:cell.post];
    } else if (action == RCPostCellActionThread) {
        [self threadForPost:cell.post];
    }
}

- (void)cell:(RCPostCell *)cell didMakeSwipeMotionWithDirection:(RCDirection)direction {
    if (direction == RCDirectionLeftToRight) {
        int nowIndex = 0;
        for (nowIndex = 0; nowIndex <= array_posts.count; nowIndex = nowIndex + 1) {
            RCPostCell *currentCell = (RCPostCell *)[tableView_posts cellForRowAtIndexPath:[NSIndexPath indexPathForRow:nowIndex inSection:0]];
            if (currentCell != cell) {
                if (currentCell.isShowingMiniOptions) [currentCell hideMiniOptions];
            }
        }
    }
    
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [array_posts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"Cell";
    RCPostCell *cell = (RCPostCell *)[tableView_posts dequeueReusableCellWithIdentifier:identifier];

    if (cell == nil) cell = [[RCPostCell alloc] init];
    cell.indexPath = indexPath;
    cell.delegate = self;
    
    ANKPost *currentPost = (ANKPost *)[array_posts objectAtIndex:indexPath.row];
    ANKPost *originalPost = currentPost;
    if ([currentPost repostedPost] != nil) currentPost = [currentPost repostedPost];
    
    cell.label_username.text = [NSString stringWithFormat:@"@%@", currentPost.user.username];
    
    
//    cell.label_post.text = currentPost.text;
//    if (currentPost == [originalPost repostedPost]) {
//        cell.label_post.text = [cell.label_post.text stringByAppendingString:[NSString stringWithFormat:@"\n\nReposted by @%@", originalPost.user.username]];
//    }
    
#pragma mark - Start of colored stuff
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:currentPost.text attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Light" size:15.f], NSForegroundColorAttributeName : [UIColor blackColor]}];
    
    
    
    for (id object in currentPost.entities.mentions) {
        [string addAttributes:@{NSForegroundColorAttributeName : defaultBlue} range:[currentPost.text rangeOfString:[NSString stringWithFormat:@"@%@", [object username]]]];
    }
    
    //Disable for links = improved reading...
    //    for (id object in currentPost.entities.links) {
    //        [string addAttributes:@{NSForegroundColorAttributeName : defaultBlue} range:[object range]];
    //    }
    
    [cell.label_post setAttributedText:string];
    
    
    
    
    
    if (currentPost == [originalPost repostedPost]) {
        NSMutableAttributedString *attr1 = cell.label_post.attributedText.mutableCopy;
        NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n\nReposted by @%@", originalPost.user.username]];
        [attr1 appendAttributedString:attr2];
        cell.label_post.attributedText = attr1;
    }
#pragma mark - End of colored stuff.
    
    
    cell.imageView_userImage.image = [UIImage imageNamed:@"placeholder"];
    
    NSDate *now = [NSDate date];
    
    NSTimeInterval timeInterval = [now timeIntervalSinceDate:currentPost.createdAt];
    NSUInteger minutes = timeInterval/60;
    NSUInteger seconds = timeInterval/1;
    NSUInteger hours = timeInterval/3600;
    NSString *finalTime = [NSString stringWithFormat:@"%i min(s) ", minutes];
    if (timeInterval <= 60) {
        finalTime = [NSString stringWithFormat:@"%i sec(s) ", seconds];
    }else if (timeInterval >= 3600) {
        finalTime = [NSString stringWithFormat:@"%i hour(s) ", hours];
    }
    
    cell.label_longAgo.text = finalTime;
    
    cell.post = currentPost;
    
    NSString *finalURLString = [currentPost.user.avatarImage.URL.absoluteString stringByAppendingString:@"?h=120&w=120"];
    NSURL *userURL = [NSURL URLWithString:finalURLString];
    
    [cell.imageView_userImage setImageWithURL:userURL placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    for (UITableViewCell *cell in tableView_posts.visibleCells) {
        if ([cell.class isSubclassOfClass:[UITableViewCell class]]) {
            [(RCPostCell *)cell hideMiniOptions];
        }
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    ANKPost *currentPost = (ANKPost *)[array_posts objectAtIndex:indexPath.row];
    ANKPost *orignialPost = currentPost;
    if ([currentPost repostedPost] != nil) currentPost = [currentPost repostedPost];
    NSString *cellText =  [currentPost text];
    if (orignialPost != currentPost) cellText = [cellText stringByAppendingString:[NSString stringWithFormat:@"\n\nReposted by @%@", orignialPost.user.username]];
    UIFont *cellFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.f];
    CGSize constraintSize = CGSizeMake(231.f, 5000.f);
    CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    
    return labelSize.height + 55.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PostDetailViewController *postDetailViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"PostDetailViewController"];
    
    [postDetailViewController setPost:[array_posts objectAtIndex:indexPath.row]];
    if ([[array_posts objectAtIndex:indexPath.row] repostedPost]) {
        [postDetailViewController setPost:[[array_posts objectAtIndex:indexPath.row] repostedPost]];
    }
    [postDetailViewController setIndexPath:indexPath];
    [[self navigationController] pushViewController:postDetailViewController animated:YES];

}


#pragma mark - Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        //Repost
        [self repostPost:[(RCActionSheet *)actionSheet post]];
    } else if (buttonIndex == 1) {
        [self repostWithQuote:[(RCActionSheet *)actionSheet post]];
    }
}

@end
