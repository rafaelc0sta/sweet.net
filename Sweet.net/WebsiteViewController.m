//
//  WebsiteViewController.m
//  Sweet.net
//
//  Created by Rafael Costa on 06/04/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "WebsiteViewController.h"
#import "ARChromeActivity.h"
#import "TUSafariActivity.h"
#import "ReadabilityActivity.h"

#define readJS @"(function(){window.baseUrl='https://www.readability.com';window.readabilityToken='';var s=document.createElement('script');s.setAttribute('type','text/javascript');s.setAttribute('charset','UTF-8');s.setAttribute('src',baseUrl+'/bookmarklet/read.js');document.documentElement.appendChild(s);})()"

@interface WebsiteViewController ()

@end

@implementation WebsiteViewController

- (IBAction)readerMode:(id)sender {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://rafaelc.com.br/api/reader/"]];
    NSDictionary *dict = @{@"url": [[[webView request] URL] absoluteString]};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    [request setHTTPBody:jsonData];
    [request setHTTPMethod:@"POST"];

    [webView loadRequest:request];
    //[webView stringByEvaluatingJavaScriptFromString:readJS];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [[webView scrollView] setContentInset:UIEdgeInsetsMake(64, 0, 44, 0)];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setTitle:self.url.absoluteString];
    [webView loadRequest:[NSURLRequest requestWithURL:self.url]];
    
    barButtonItem_forward.enabled = FALSE;
    barButtonItem_back.enabled = FALSE;
    barButtonItem_share = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"share_icon_mobile"] style:UIBarButtonItemStylePlain target:self action:@selector(openInThirdPartyApp)];
    [barButtonItem_share setEnabled:false];
    [self.navigationItem setRightBarButtonItem:barButtonItem_share];
    
	// Do any additional setup after loading the view.
}

-(void)viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        
    }
    [super viewWillDisappear:animated];
}

- (void)openInThirdPartyApp {
    
//    NSString *texttoshare = webView.request.URL.absoluteString; //this is your text string to share
    
    TUSafariActivity *safari = [[TUSafariActivity alloc] init];
    NSMutableArray *activityApps = @[safari].mutableCopy;
    
    
    
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"googlechrome://"]]) {
        ARChromeActivity *chrome = [[ARChromeActivity alloc] init];
        [activityApps addObject:chrome];
    }
    
    if ([ReadabilityActivity canPerformActivity]) {
        ReadabilityActivity *readability = [[ReadabilityActivity alloc] init];
        [activityApps addObject:readability];
    }
    
    
    NSURL *url = webView.request.URL;
    NSArray *activityItems = @[url];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:activityApps];
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint, UIActivityTypeAirDrop];
    [self presentViewController:activityVC animated:TRUE completion:nil];
    
    return;
    
    //UIActivityViewController *viewController
    
    NSString *link = webView.request.URL.absoluteString;
    if (link.length == 0) link = self.url.absoluteString;
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"googlechrome://"]]) {
        //actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Open \"%@\" somewhere else?", link] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Open in Safari", @"Open in Chrome", nil];
        actionSheet = [[JLActionSheet alloc] initWithTitle:@"Open link somewhere else?" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Open in Safari", @"Open in Chrome"]];
        [actionSheet setStyle:JLSTYLE_SUPERCLEAN];
        [actionSheet setDidDismissBlock:^(JLActionSheet *actionSheet, NSInteger buttonIndex) {
            if (buttonIndex == 2) {
                // Safari
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
            } else if (buttonIndex == 1) {
                // Chrome
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[link stringByReplacingOccurrencesOfString:@"http" withString:@"googlechrome"]]];
            }
        }];
    } else {
        actionSheet = [[JLActionSheet alloc] initWithTitle:@"Open link in Safari?" delegate:nil cancelButtonTitle:@"No" otherButtonTitles:@[@"Yes"]];
        [actionSheet setStyle:JLSTYLE_SUPERCLEAN];
        [actionSheet setDidDismissBlock:^(JLActionSheet *actionSheet, NSInteger buttonIndex) {
            if (buttonIndex == 0) {
                // No.
            } else if (buttonIndex == 1) {
                // Yes.
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
            }
        }];
    }
    [actionSheet showInView:self.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ActionSheet

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSURL *url = webView.request.URL;
    if (url.absoluteString.length == 0) url = self.url;
    NSLog(@"INPUT ORIGINAL: %i; AFTER: %i", webView.request.URL.absoluteString.length, url.absoluteString.length);
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"googlechrome://"]]) {

        /* Chrome URL setting */
        NSURL *inputURL = url;
        
        NSString *scheme = inputURL.scheme;
        
        // Replace the URL Scheme with the Chrome equivalent.
        NSString *chromeScheme = nil;
        if ([scheme isEqualToString:@"http"]) {
            chromeScheme = @"googlechrome";
        } else if ([scheme isEqualToString:@"https"]) {
            chromeScheme = @"googlechromes";
        }
        
        /* END OF: Chrome URL setting */
        
        // Button index 0 = Safari; 1 = Chrome; 2 = Cancel;
        switch (buttonIndex) {
            case 0:
                [[UIApplication sharedApplication] openURL:url];
                break;
            case 1:
                if (chromeScheme) {
                    NSString *absoluteString = [inputURL absoluteString];
                    NSRange rangeForScheme = [absoluteString rangeOfString:@":"];
                    NSString *urlNoScheme =
                    [absoluteString substringFromIndex:rangeForScheme.location];
                    NSString *chromeURLString =
                    [chromeScheme stringByAppendingString:urlNoScheme];
                    NSURL *chromeURL = [NSURL URLWithString:chromeURLString];
                    
                    // Open the URL with Chrome.
                    [[UIApplication sharedApplication] openURL:chromeURL];
                }
                break;
            case 2:
                // Do nothing
            default:
                break;
        }
    } else {
        // Button index 0 = Safari; 1 =  Cancel;
        switch (buttonIndex) {
            case 0:
                [[UIApplication sharedApplication] openURL:url];
                break;
            case 1:
                // Do nothing
                break;
                
            default:
                break;
        }
    }
}

#pragma mark WebViewDelegate 

- (void)webViewDidFinishLoad:(UIWebView *)webView1 {
    [self.navigationItem setTitle:webView1.request.URL.absoluteString];
    if (webView1.canGoBack) barButtonItem_back.enabled = TRUE;
    else barButtonItem_back.enabled = FALSE;
    if (webView1.canGoForward) barButtonItem_forward.enabled = TRUE;
    else barButtonItem_forward.enabled = FALSE;
    
    barButtonItem_share.enabled = true;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    barButtonItem_forward.enabled = FALSE;
    barButtonItem_back.enabled = FALSE;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    if (error.code != NSURLErrorCancelled) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
