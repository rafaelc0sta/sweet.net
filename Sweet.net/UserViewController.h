//
//  UserViewController.h
//  Sweet.net
//
//  Created by Rafael Costa on 16/03/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADNKit.h"
#import "AGMedallionView.h"
#import "RCKit.h"

@interface UserViewController : UIViewController <UINavigationControllerDelegate, UIScrollViewDelegate> {
    
    NSMutableArray *array_options;
    
    IBOutlet UILabel *label_name;
    IBOutlet UILabel *label_username;
    IBOutlet UITextView *textView_description;
    
    IBOutlet UIImageView *imageView_userImage;
    IBOutlet UIImageView *imageView_verified;
    IBOutlet UIImageView *imageView_userBG;
    IBOutlet UIImageView *imageView_userBGmask;
    
    IBOutlet UIButton *button_followers;
    IBOutlet UIButton *button_following;
    IBOutlet UIButton *button_stars;
    IBOutlet UIButton *button_posts;
    IBOutlet UIButton *button_logout;
    IBOutlet UIButton *button_settings;
    
    IBOutlet UIScrollView *scrollView;
    
    CGRect frame_imageViewUserBG;
    
    NSMutableArray *array_MWPhotos;
    
    //BOOL hasSet;
}
@property (nonatomic) ANKUser *user;
- (IBAction)logout:(id)sender;

- (IBAction)showFollowers:(id)sender;
- (IBAction)showFollowing:(id)sender;
- (IBAction)showPosts:(id)sender;
- (IBAction)showStars:(id)sender;

- (IBAction)showSettings:(id)sender;
@end
