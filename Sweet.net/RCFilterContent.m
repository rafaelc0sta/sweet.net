//
//  RCFilterContent.m
//  Sweet.net
//
//  Created by Rafael Costa on 1/17/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import "RCFilterContent.h"

@implementation RCFilterContent
- (BOOL)shouldRemovePost:(id)filteree {
    ANKPost *post = (ANKPost *)filteree;
    
    if (self.searchOption == RCFilterCompareOptionEquals) {
        if ([post.text.lowercaseString isEqualToString:self.query.lowercaseString]) return true;
        return false;
    } else if (self.searchOption == RCFilterCompareOptionContains) {
        if ([post.text.lowercaseString rangeOfString:self.query.lowercaseString].location != NSNotFound) return true;
        return false;
    }
}
@end
