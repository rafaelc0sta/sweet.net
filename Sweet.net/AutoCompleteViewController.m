//
//  AutoCompleteViewController.m
//  SweetTweet7
//
//  Created by Rafael Costa on 25/12/12.
//  Copyright (c) 2012 Rafael Costa. All rights reserved.
//

#import "AutoCompleteViewController.h"
#import "MessagesListViewController.h"
#import "RCComposeViewController.h"
#import "MBProgressHUD.h"

@interface AutoCompleteViewController ()
@end

@implementation AutoCompleteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.toolbar.delegate = self;
    [self.autocompleteTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.autocompleteTableView setSeparatorColor:[UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:0.75]];
    
    [self.textField becomeFirstResponder];
    _mutableArray_completions = [[NSMutableArray alloc]init];
    
//    _mutableArray_FollowingSavedCompletions = [[[[RCUserListStorage sharedInstance] followingUserList] allKeys] mutableCopy];
//    _mutableArray_ConversationSavedCompletions = [[[[RCUserListStorage sharedInstance] conversationsUserList] allKeys] mutableCopy];
//    
//    _usableSavedCompletions = [[_mutableArray_FollowingSavedCompletions arrayByAddingObjectsFromArray:_mutableArray_ConversationSavedCompletions] mutableCopy];
    
    _usableSavedCompletions = [[RCUserListStorage sharedInstance] userList];
    
//    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(repeatedMethod) userInfo:nil repeats:YES];
	// Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self textChanged:self];
    
    self.textField.text = @"@";
    [self searchAutocompleteEntriesWithSubstring:self.textField.text];
}

//- (void)repeatedMethod {
//    if (self.textField.text.length == 0) {
//        [_mutableArray_completions removeAllObjects];
//        [self.autocompleteTableView reloadData];
//    }
//}

- (IBAction)cancelar:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)textChanged:(id)sender {
    _autocompleteTableView.hidden = NO;
    [_mutableArray_completions removeAllObjects];
    NSString *substring = [NSString stringWithString:_textField.text];
//    substring = [substring
//                 stringByReplacingCharactersInRange:range withString:string];
//    if ([substring rangeOfString:@" "].location != NSNotFound) {
//        substring = [substring substringFromIndex:[substring rangeOfString:@" " options:NSBackwardsSearch].location];
//    }
    substring = [substring stringByReplacingOccurrencesOfString:@"@" withString:@""];
    [self searchAutocompleteEntriesWithSubstring:substring];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    //NSLog(@"String to be searched: %@", substring);
    if (textField.text.length == 1 && string.length == 0) return NO;
    return YES;

}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    // Put anything that starts with this substring into the autocompleteUrls array
    // The items in this array is what will show up in the table view
    [_mutableArray_completions removeAllObjects];
    _stringTyped = substring;
    substring = [substring lowercaseString];
    for(NSString *string in _usableSavedCompletions) {
        NSRange substringRange = [[string lowercaseString] rangeOfString:[substring stringByReplacingOccurrencesOfString:@" " withString:@""]];
        if (substringRange.location == 1 || substringRange.location == 0) {
            [_mutableArray_completions addObject:string];
        }
    }
    NSLog(@"CountAFTER: %i", [_mutableArray_completions count]);
    [_mutableArray_completions sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    if (_mutableArray_completions.count == 0 && self.textField.text.length > 0) {
        [_mutableArray_completions addObject:substring];
    }
    [_autocompleteTableView reloadData];
}

#pragma mark UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section {
    if (_mutableArray_completions.count > 25) {
        return 25;
    } else {
        return _mutableArray_completions.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    if (cell == nil) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    cell.userInteractionEnabled = true;
    cell.textLabel.text = [@"@" stringByAppendingString:[_mutableArray_completions objectAtIndex:indexPath.row]];
    
    if ([cell.textLabel.text isEqualToString:@"@@"] || [cell.textLabel.text isEqualToString:@"@"]) cell.textLabel.text = @"No results!";

    return cell;
}


#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
//    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
//    [self.autocompleteTableView deselectRowAtIndexPath:indexPath animated:YES];
//    NSString *cellText = selectedCell.textLabel.text;
//    NSString *firstChar = [NSString stringWithFormat:@"%c", [cellText characterAtIndex:0]];
//    if ([firstChar isEqualToString:@"@"]) {
//        
//        if ([self.delegate isKindOfClass:[RCComposeViewController class]]) {
//            [self.delegate autoComplete:self didReturnWithUsername:cellText andUserID:@"notNeeded"];
//            [self dismissViewControllerAnimated:YES completion:nil];
//        } else {
//            if ([[[RCUserListStorage sharedInstance] followingUserList] objectForKey:[cellText stringByReplacingOccurrencesOfString:@"@" withString:@""]] != nil) {
//                [self.delegate autoComplete:self didReturnWithUsername:cellText andUserID:[[[RCUserListStorage sharedInstance] followingUserList] objectForKey:[cellText stringByReplacingOccurrencesOfString:@"@" withString:@""]]];
//                [self dismissViewControllerAnimated:YES completion:nil];
//            } else {
//                MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:appWindow];
//                [appWindow addSubview:HUD];
//                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
//                HUD.mode = MBProgressHUDModeCustomView;
//                HUD.labelText = @"This user DOES NOT follow you.";
//                [HUD show:YES];
//                [HUD hide:YES afterDelay:1.5];
//            }
//        }
//        
//        
//    }
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

@end
