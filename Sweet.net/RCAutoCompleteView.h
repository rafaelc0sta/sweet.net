//
//  RCAutoCompleteView.h
//  Sweet.net
//
//  Created by Rafael Costa on 2/26/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RCAutoCompleteViewDelegate;

@interface RCAutoCompleteView : UIScrollView {
    NSMutableArray *array_autoCompleteItems;
    NSMutableArray *array_savedAutoCompleteItems;
    
    NSRange typedRange;
}

@property (nonatomic, setter = setTypedString:) NSString *typedString;
@property (nonatomic, strong) NSObject<RCAutoCompleteViewDelegate, UIScrollViewDelegate> *delegate;
@property (nonatomic, setter = setIsTypingAutoComplete:) BOOL isTypingAutocomplete;


- (void)setIsTypingAutocomplete:(BOOL)isTypingAutocomplete;
- (void)setTypedString:(NSString *)typedString withRange:(NSRange)range;

//- (void)reload;
@end

@protocol RCAutoCompleteViewDelegate <NSObject>

- (void)returnedCompletion:(NSString *)completion withRange:(NSRange)range;

@end