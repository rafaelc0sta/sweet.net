//
//  RCMaskedImageView.m
//  Sweet.net
//
//  Created by Rafael Costa on 13/06/13.
//  Copyright (c) 2013 Rafael Costa. All rights reserved.
//

#import "RCMaskedImageView.h"

@implementation RCMaskedImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    [self setUpWithMaskImage:[UIImage imageNamed:@"profileMask"]];
}

- (id)initWithImage:(UIImage *)image andMaskImage:(UIImage *)maskImage {
    self = [super initWithImage:image];
    if (self) {
        // Initialization code
        [self setUpWithMaskImage:maskImage];
    }
    return self;
}

- (id)initWithDefaultMaskAndImage:(UIImage *)image {
    self = [super initWithImage:image];
    if (self) {
        // Initialization code
        [self setUpWithMaskImage:[UIImage imageNamed:@"profileMask"]];
    }
    return self;
}

- (void)setUpWithMaskImage:(UIImage *)maskImage {
    CGPoint saveCenter = self.center;
    self.layer.cornerRadius = self.frame.size.width / 2.0;
    self.center = saveCenter;
    self.clipsToBounds = YES;
//    CALayer *mask = [CALayer layer];
//    mask.contents = (id)[maskImage CGImage];
//    mask.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
//    self.layer.mask = mask;
//    self.layer.masksToBounds = YES;
}

@end
