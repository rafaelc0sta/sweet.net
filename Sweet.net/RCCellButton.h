//
//  RCCellButton.h
//  Sweet.net
//
//  Created by Rafael Costa on 3/15/14.
//  Copyright (c) 2014 Rafael Costa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCCellButton : UIButton {
    IBOutlet UILabel *label_title;
    IBOutlet UILabel *label_subtitle;
}
- (void)setTitle:(NSString *)title withSubtitle:(NSString *)subtitle;
@end
